package com.softlogic.services;
import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.softlogic.daos.ICustomerDetails;
import com.softlogic.daos.IPDFGenerator;
import com.softlogic.models.AC_Customer;
import com.softlogic.models.Customer;
import com.softlogic.models.PrintModel;
import com.softlogic.models.VM_AC_Customer;
import com.softlogic.daos.FileReadDao;

@Service
public class DataManager 
{
	@Autowired
	private ICustomerDetails customerDetails;
	@Autowired
	private IPDFGenerator PDFGenerator;
/*	public ArrayList<Customer> excelDataManager()
	{
		System.out.println("inside the excelDataManager");
		customerDetails.excelToMysql();
		ArrayList<Customer> customers =customerDetails.getCustomerData();
		return customers;
	}*/
	public ArrayList<AC_Customer> takeAllAC_CustomerDetails()
	{
		ArrayList<AC_Customer> acCustomerList = customerDetails.getAC_CustomerData();
		/*ArrayList<VM_AC_Customer> vmacCustomerList = new ArrayList<VM_AC_Customer>();
		
		for (int i = 0; i < acCustomerList.size(); i++) 
		{
			vmacCustomerList.add(this.createVMAC_CustomerToTable(acCustomerList.get(i)));
		}*/
		
		return acCustomerList; 
	}
	public void createExcelFile()
	{
		customerDetails.createExcelFile();
	}
	public void manageInsertingData(String stringMonth,String installationDate,String jobNo,String customerName,String customerAdress,String city,String contact,String contactNo,String type,String unit,String capacity,String serialNumberIndoor,String serialNumberOutdoor,String no_name,String instNo,String intalledBy,String serviceTeam,String frequency)
	{
		AC_Customer acCustomertoInsert= new AC_Customer();
	
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			Date tempDate = format.parse(stringMonth);
			format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(tempDate);
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		finally
		{
			acCustomertoInsert.setMonth(stringMonth);
		}
		
		acCustomertoInsert.setInstallationDate(installationDate);
		acCustomertoInsert.setJobNo(jobNo);
		acCustomertoInsert.setCustomerName(customerName);
		acCustomertoInsert.setCustomerAdress(customerAdress);
		acCustomertoInsert.setCity(city);
		acCustomertoInsert.setContact(contact);
		acCustomertoInsert.setContactNo(contactNo);
		acCustomertoInsert.setType(type);
		acCustomertoInsert.setUnit(unit);
		acCustomertoInsert.setCapacity(capacity);
		acCustomertoInsert.setSerialNumberIndoor(serialNumberIndoor);
		acCustomertoInsert.setSerialNumberOutdoor(serialNumberOutdoor);
		acCustomertoInsert.setNo_name(no_name);
		acCustomertoInsert.setInstNo(instNo);
		acCustomertoInsert.setIntalledBy(intalledBy);
		acCustomertoInsert.setFrequency(frequency);
		acCustomertoInsert.setServiceTeam(serviceTeam);
		
		
		try
		{
			int intFrequency = Integer.parseInt(acCustomertoInsert.getFrequency());
			int intervalDays = this.calculateIntervalDays(intFrequency);
			if(1 <= intFrequency)
			{
				String firstServiceDate = this.calculateAnyServiceDate(installationDate,intervalDays,1);
				acCustomertoInsert.setFirstFree(firstServiceDate);
			}
			if(2 <= intFrequency)
			{
				String secondServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,2);
				acCustomertoInsert.setSecondFree(secondServiceDate);
			}
			if(3 <= intFrequency)
			{
				String thiredServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,3);
				acCustomertoInsert.setThirdFree(thiredServiceDate);
			}
			if(4 <= intFrequency)
			{
				String forthServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,4);
				acCustomertoInsert.setForth_Service(forthServiceDate);
			}
			if(5 <= intFrequency)
			{
				String fifthServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,5);
				acCustomertoInsert.setFifth_Service(fifthServiceDate);
			}
			if(6 <= intFrequency)
			{
				String SixthServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,6);
				acCustomertoInsert.setSixth_Service(SixthServiceDate);
			}
			
			/*if(firstFreeStatus.length==2)
			{
				acCustomertoInsert.setFirstFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setFirstFreeStatus(0);
			}
			if(secondFreeStatus.length==2)
			{
				acCustomertoInsert.setSecondFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setSecondFreeStatus(0);
			}
			if(thirdFreeStatus.length==2)
			{
				acCustomertoInsert.setThirdFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setThirdFreeStatus(0);
			}*/
			
			
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
			
		}
		try
		{
			acCustomertoInsert.setDateInstDate(instdateStringToMySqlDateString(acCustomertoInsert.getInstallationDate()));
		}
		catch(Exception ex)
		{
			acCustomertoInsert.setReadError(1);
		}
		acCustomertoInsert.setAmc(1);
		customerDetails.insertMysql(acCustomertoInsert);
		//customerDetails.createExcelFile();
	}
	public void manageInsertingFreeServiceData(String stringMonth,String installationDate,String jobNo,String customerName,String customerAdress,String city,String contact,String contactNo,String type,String unit,String capacity,String serialNumberIndoor,String serialNumberOutdoor,String no_name,String instNo,String intalledBy,String serviceTeam)
	{
		AC_Customer acCustomertoInsert= new AC_Customer();
	
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			Date tempDate = format.parse(stringMonth);
			format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(tempDate);
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		finally
		{
			acCustomertoInsert.setMonth(stringMonth);
		}
		try
		{
		//acCustomertoInsert.setMonth(installationDate);
		acCustomertoInsert.setInstallationDate(installationDate);
		acCustomertoInsert.setJobNo(jobNo);
		acCustomertoInsert.setCustomerName(customerName);
		acCustomertoInsert.setCustomerAdress(customerAdress);
		acCustomertoInsert.setCity(city);
		acCustomertoInsert.setContact(contact);
		acCustomertoInsert.setContactNo(contactNo);
		acCustomertoInsert.setType(type);
		acCustomertoInsert.setUnit(unit);
		acCustomertoInsert.setCapacity(capacity);
		acCustomertoInsert.setSerialNumberIndoor(serialNumberIndoor);
		acCustomertoInsert.setSerialNumberOutdoor(serialNumberOutdoor);
		acCustomertoInsert.setNo_name(no_name);
		acCustomertoInsert.setInstNo(instNo);
		acCustomertoInsert.setIntalledBy(intalledBy);
		//acCustomertoInsert.setEmpty(frequency);
		acCustomertoInsert.setServiceTeam(serviceTeam);
		
		String firstServiceDate = this.createFirstServiceDate(acCustomertoInsert);
		acCustomertoInsert.setFirstFree(firstServiceDate);

		String secondServiceDate=this.createSecondServiceDate(acCustomertoInsert);
		acCustomertoInsert.setSecondFree(secondServiceDate);

		String thiredServiceDate=this.createThiredServiceDate(acCustomertoInsert);
		acCustomertoInsert.setThirdFree(thiredServiceDate);

			
			/*if(firstFreeStatus.length==2)
			{
				acCustomertoInsert.setFirstFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setFirstFreeStatus(0);
			}
			if(secondFreeStatus.length==2)
			{
				acCustomertoInsert.setSecondFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setSecondFreeStatus(0);
			}
			if(thirdFreeStatus.length==2)
			{
				acCustomertoInsert.setThirdFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setThirdFreeStatus(0);
			}*/
			
			
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		try
		{
			acCustomertoInsert.setDateInstDate(instdateStringToMySqlDateString(acCustomertoInsert.getInstallationDate()));
		}
		catch(Exception ex)
		{
			acCustomertoInsert.setReadError(1);
		}
		customerDetails.insertMysql(acCustomertoInsert);
		//customerDetails.createExcelFile();
	}
	public AC_Customer customerDetailsFromAC_Customer_pk(String acCustomer_pk)
	{
		AC_Customer acCustomer = customerDetails.selectDetailsFromAC_customer_pk(acCustomer_pk);
		return acCustomer;
	}
	public void editCustomerData(String pk,String stringMonth,String installationDate,String jobNo,String customerName,String customerAdress,String city,String contact,String contactNo,String type,String unit,String capacity,String serialNumberIndoor,String serialNumberOutdoor,String no_name,String instNo,String intalledBy,String serviceTeam,String firstFree,String secondFree,String thirdFree,String forthService,String fifthService,String sixthService,String[] firstFreeStatus,String[] secondFreeStatus,String[] thirdFreeStatus,String[] forthServiceStatus,String[] fifthServicetatus,String[] sixthServicetatus,String serviceType,String comments)
	{
		AC_Customer acCustomertoEdit= customerDetails.selectDetailsFromAC_customer_pk(pk);
		System.out.println("pk"+pk);
		acCustomertoEdit.setCustomer_pk(pk);
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			Date tempDate = format.parse(stringMonth);
			format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(tempDate);
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		finally
		{
			acCustomertoEdit.setMonth(stringMonth);
		}
		
		acCustomertoEdit.setInstallationDate(installationDate);
		acCustomertoEdit.setJobNo(jobNo);
		acCustomertoEdit.setCustomerName(customerName);
		acCustomertoEdit.setCustomerAdress(customerAdress);
		acCustomertoEdit.setCity(city);
		acCustomertoEdit.setContact(contact);
		acCustomertoEdit.setContactNo(contactNo);
		acCustomertoEdit.setType(type);
		acCustomertoEdit.setUnit(unit);
		acCustomertoEdit.setCapacity(capacity);
		acCustomertoEdit.setSerialNumberIndoor(serialNumberIndoor);
		acCustomertoEdit.setSerialNumberOutdoor(serialNumberOutdoor);
		acCustomertoEdit.setNo_name(no_name);
		acCustomertoEdit.setInstNo(instNo);
		acCustomertoEdit.setIntalledBy(intalledBy);

		acCustomertoEdit.setServiceTeam(serviceTeam);

		if(!firstFree.equals(""))
		{
			acCustomertoEdit.setFirstFree(firstFree);
		}
		if(!secondFree.equals(""))
		{
			acCustomertoEdit.setSecondFree(secondFree);
		}
		if(!thirdFree.equals(""))
		{
			acCustomertoEdit.setThirdFree(thirdFree);
		}
		if(!forthService.equals(""))
		{
			acCustomertoEdit.setForth_Service(forthService);
		}
		if(!fifthService.equals(""))
		{
			acCustomertoEdit.setFifth_Service(fifthService);
		}
		if(!sixthService.equals(""))
		{
			acCustomertoEdit.setSixth_Service(sixthService);
		}
		
		if(firstFreeStatus.length==2)
		{
			acCustomertoEdit.setFirstFreeStatus(1);
		}
		else
		{
			acCustomertoEdit.setFirstFreeStatus(0);
		}
		if(secondFreeStatus.length==2)
		{
			acCustomertoEdit.setSecondFreeStatus(1);
		}
		else
		{
			acCustomertoEdit.setSecondFreeStatus(0);
		}
		if(thirdFreeStatus.length==2)
		{
			acCustomertoEdit.setThirdFreeStatus(1);
		}
		else
		{
			acCustomertoEdit.setThirdFreeStatus(0);
		}
		if(forthServiceStatus.length==2)
		{
			acCustomertoEdit.setForthServiceStatus(1);
		}
		else
		{
			acCustomertoEdit.setForthServiceStatus(0);
		}
		if(fifthServicetatus.length==2)
		{
			acCustomertoEdit.setFifthServiceStatus(1);
		}
		else
		{
			acCustomertoEdit.setFifthServiceStatus(0);
		}
		if(sixthServicetatus.length==2)
		{
			acCustomertoEdit.setSixthServiceStatus(1);
		}
		else
		{
			acCustomertoEdit.setSixthServiceStatus(0);
		}
		System.out.println("serviceType :" + serviceType);
		if(serviceType.equals("amc"))
		{System.out.println("came amc in");
			acCustomertoEdit.setAmc(1);
		}
		else if(serviceType.equals("free"))
		{System.out.println("came free in");
			acCustomertoEdit.setAmc(0);
		}
		try
		{
			acCustomertoEdit.setDateInstDate(instdateStringToMySqlDateString(acCustomertoEdit.getInstallationDate()));
		}
		catch(Exception ex)
		{
			acCustomertoEdit.setReadError(1);
		}
		acCustomertoEdit.setComments(comments);
		customerDetails.updateAC_Customer(acCustomertoEdit);
	}
	public void  removeCustomer(String customerPK)
	{
		AC_Customer acCustomer= new AC_Customer();
		acCustomer.setCustomer_pk(customerPK);
		customerDetails.DeleteCustomerRowByPK(acCustomer);
	}
	public String[] readExcel(String filepath,String fileName,String serviceType)
	{
		//customerDetails.deleteAllAC_Customers();
		FileReadDao frd = new FileReadDao();
		ArrayList<AC_Customer> AC_customerList = null;
		
		String extention = FilenameUtils.getExtension(fileName);
		//String[] spliteFileName = fileName.split("\\.");//"." in a regular expression means "any character".
		//String extention = spliteFileName[1];
		//System.out.println("File Extention :" + extention);
		
		if(extention.equals("xls"))
		{
			AC_customerList = frd.readAllAC_CustomerDetailsXls(filepath);

		}
		if(extention.equals("xlsx"))
		{
			AC_customerList = frd.readAllAC_CustomerDetailsXlsx(filepath);
			System.out.println("AC_customerList.size :" + AC_customerList.size());
		}
		if (serviceType.equals("free"))
		{		

			for(int i=0;i<AC_customerList.size();i++)
			{
				try
				{
					AC_customerList.get(i).setDateInstDate(instdateStringToMySqlDateString(AC_customerList.get(i).getInstallationDate()));
				}
				catch(Exception ex)
				{
					AC_customerList.get(i).setReadError(1);
				}
				try
				{
					String firstFreeDate=this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),30,1);
					AC_customerList.get(i).setFirstFree(firstFreeDate);
	
					String secondFreeDate=this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),30,2);
					AC_customerList.get(i).setSecondFree(secondFreeDate);
	
					String thiredFreeDate=this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),30,3);
					AC_customerList.get(i).setThirdFree(thiredFreeDate);
					
				}
				catch(Exception	ex)
				{
					AC_customerList.get(i).setReadError(1);
				}
			}
		}
		if(serviceType.equals("amc"))
		{
			int intFrequency;
			int intevalDays;
			for(int i=0;i<AC_customerList.size();i++)
			{
				AC_customerList.get(i).setAmc(1);
				try
				{
					AC_customerList.get(i).setDateInstDate(instdateStringToMySqlDateString(AC_customerList.get(i).getInstallationDate()));
				}
				catch(Exception ex)
				{
					AC_customerList.get(i).setReadError(1);
				}
				if((AC_customerList.get(i).getFrequency()!=null)&&(AC_customerList.get(i).getInstallationDate()!=null))
				{
					try
					{
						intFrequency = Integer.parseInt(AC_customerList.get(i).getFrequency());
						intevalDays = this.calculateIntervalDays(intFrequency);	
						if(1 <= intFrequency)
						{
							String serviceDate = this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),intevalDays,1);
							AC_customerList.get(i).setFirstFree(serviceDate);
						}
						if(2 <= intFrequency)
						{
							String serviceDate = this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),intevalDays,2);
							AC_customerList.get(i).setSecondFree(serviceDate);
						}
						if(3 <= intFrequency)
						{
							String serviceDate = this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),intevalDays,3);
							AC_customerList.get(i).setThirdFree(serviceDate);
						}
						if(4 <= intFrequency)
						{
							String serviceDate = this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),intevalDays,4);
							AC_customerList.get(i).setForth_Service(serviceDate);
						}
						if(5 <= intFrequency)
						{
							String serviceDate = this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),intevalDays,5);
							AC_customerList.get(i).setFifth_Service(serviceDate);
						}
						if(6 <= intFrequency)
						{
							String serviceDate = this.calculateAnyServiceDate(AC_customerList.get(i).getInstallationDate(),intevalDays,6);
							AC_customerList.get(i).setSixth_Service(serviceDate);
						}
						
					}
					catch(NumberFormatException NFEex)
					{
						AC_customerList.get(i).setReadError(1);
						System.out.println("----------------------------------ParseException---------------------------------");
						System.out.println("DataManager/readExcel/if servicetype==amc/exception :" + NFEex);
						System.out.println("---------------------------------------------------------------------------------");
					}
					catch(ParseException pex)
					{
						AC_customerList.get(i).setReadError(1);
						System.out.println("----------------------------------ParseException---------------------------------");
						System.out.println("DataManager/readExcel/if servicetype==amc/exception :" + pex);
						System.out.println("---------------------------------------------------------------------------------");
					}
					catch(Exception ex)
					{
						System.out.println("----------------------------------Exception---------------------------------");
						System.out.println("DataManager/readExcel/if servicetype==amc/exception :" + ex);
						System.out.println("----------------------------------Exception---------------------------------");
					}
					//AC_customerList.get(i).setMonth(AC_customerList.get(i).getInstallationDate());
					//AC_customerList.get(i).setInstallationDate("");
				}
				else
				{
					AC_customerList.get(i).setReadError(1);
				}
			}

		}
		
		ArrayList<AC_Customer> ac_CustomerArrayList = new ArrayList<AC_Customer>();
		ac_CustomerArrayList=customerDetails.insertAC_CustomerList(AC_customerList);
		ArrayList<AC_Customer> ac_CustomerArrayListToReturn = new ArrayList<AC_Customer>();
		String[] ExcelViewIdArray = new String[ac_CustomerArrayList.size()];
		int j = 0;
		for(int i=(ac_CustomerArrayList.size()-1); (-1) < i ; i--)
		{
			ac_CustomerArrayListToReturn.add(ac_CustomerArrayList.get(i));
			ExcelViewIdArray[j]=ac_CustomerArrayList.get(i).getCustomer_pk();
			j++;	
		}
		return ExcelViewIdArray; 
		
	}
	private int calculateIntervalDays(int frequency)throws Exception
	{
		int intervalDays;
		
		intervalDays = 30*(12/frequency);
		
		return intervalDays;
	}
	private String calculateAnyServiceDate(String installedDate,int intervalDays,int serviceNumber)throws Exception
	{
		String stringCellValue = null;
		String serviceDate = null;
		Date dateCellValue = null;
		/*try
		{*/
			if(installedDate != null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(installedDate));
				int daysToAdd = intervalDays * serviceNumber;
				c.add(Calendar.DATE, daysToAdd);  // number of days to add
				sdf = new SimpleDateFormat("yyyy-MM-dd");
				serviceDate = sdf.format(c.getTime());
			}
		/*}
		catch(Exception ex)
		{
			System.out.println("DataManager.createFirstServiceDate"+ex);
		}*/
		
		return serviceDate;
	}
	public int testingTest(int number)
	{
		return customerDetails.testingTest(number);
	}
/*	private VM_AC_Customer createVMAC_CustomerToEdit(AC_Customer acCustomer)
	{
		String stringMonth=null;
		VM_AC_Customer vmacCustomer = new VM_AC_Customer();
		vmacCustomer.setCustomer_pk(acCustomer.getCustomer_pk());
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(acCustomer.getMonth());
		}
		catch(Exception ex)
		{
			System.out.println("Class:AC_Customer(model),fuction:setMonth,Exception:" + ex); 
		}
		vmacCustomer.setMonth(stringMonth);
		
		String stringInstallationDate = null;
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			stringInstallationDate  = format.format(acCustomer.getInstallationDate());
		}
		catch(Exception ex)
		{
			System.out.println("Class:AC_Customer(model),fuction:setMonth,Exception:" + ex); 
		}
		vmacCustomer.setInstallationDate(stringInstallationDate);
		vmacCustomer.setJobNo(acCustomer.getJobNo());
		vmacCustomer.setCustomerName(acCustomer.getCustomerName());
		vmacCustomer.setCustomerAdress(acCustomer.getCustomerAdress());
		vmacCustomer.setCity(acCustomer.getCity());
		vmacCustomer.setContact(acCustomer.getCapacity());
		vmacCustomer.setContactNo(acCustomer.getContactNo());
		vmacCustomer.setType(acCustomer.getType());
		vmacCustomer.setUnit(acCustomer.getUnit());
		vmacCustomer.setCapacity(acCustomer.getCapacity());
		vmacCustomer.setSerialNumberIndoor(acCustomer.getSerialNumberIndoor());
		vmacCustomer.setSerialNumberOutdoor(acCustomer.getSerialNumberOutdoor());
		vmacCustomer.setNo_name(acCustomer.getNo_name());
		vmacCustomer.setInstNo(acCustomer.getInstNo());
		vmacCustomer.setIntalledBy(acCustomer.getIntalledBy());
		vmacCustomer.setServiceTeam(acCustomer.getServiceTeam());
		vmacCustomer.setFirstFree(acCustomer.getFirstFree());
		vmacCustomer.setSecondFree(acCustomer.getSecondFree());
		vmacCustomer.setThirdFree(acCustomer.getThirdFree());
		return vmacCustomer;
	}
	private VM_AC_Customer createVMAC_CustomerToTable(AC_Customer acCustomer)
	{
		VM_AC_Customer vmacCustomer = new VM_AC_Customer();
		
		String stringMonth=null;
		vmacCustomer.setCustomer_pk(acCustomer.getCustomer_pk());
		try
		{
			DateFormat format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(acCustomer.getMonth());
		}
		catch(Exception ex)
		{
			//System.out.println("Class:DataManager(Service),fuction:createVMAC_CustomerToTable,Exception:" + ex); 
		}
		vmacCustomer.setMonth(stringMonth);
		
		String stringInstallationDate=null;
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			stringInstallationDate = format.format(acCustomer.getInstallationDate());
		}
		catch(Exception ex)
		{
			//System.out.println("Class:DataManager(Service),fuction:createVMAC_CustomerToTable,Exception:" + ex); 
		}
		vmacCustomer.setInstallationDate(stringInstallationDate);
		
		vmacCustomer.setJobNo(acCustomer.getJobNo());
		vmacCustomer.setCustomerName(acCustomer.getCustomerName());
		vmacCustomer.setCustomerAdress(acCustomer.getCustomerAdress());
		vmacCustomer.setCity(acCustomer.getCity());
		vmacCustomer.setContact(acCustomer.getCapacity());
		vmacCustomer.setContactNo(acCustomer.getContactNo());
		vmacCustomer.setType(acCustomer.getType());
		vmacCustomer.setUnit(acCustomer.getUnit());
		vmacCustomer.setCapacity(acCustomer.getCapacity());
		vmacCustomer.setSerialNumberIndoor(acCustomer.getSerialNumberIndoor());
		vmacCustomer.setSerialNumberOutdoor(acCustomer.getSerialNumberOutdoor());
		vmacCustomer.setNo_name(acCustomer.getNo_name());
		vmacCustomer.setInstNo(acCustomer.getInstNo());
		vmacCustomer.setIntalledBy(acCustomer.getIntalledBy());
		vmacCustomer.setServiceTeam(acCustomer.getServiceTeam());
		vmacCustomer.setFirstFree(acCustomer.getFirstFree());
		vmacCustomer.setSecondFree(acCustomer.getSecondFree());
		vmacCustomer.setThirdFree(acCustomer.getThirdFree());
		return vmacCustomer;
	}*/
	public void pdfGenarateService(String[] checkedArray,String[] filterInfo)
	{	
		ArrayList<AC_Customer> acCustomerList = customerDetails.selectCustomerDetailsByArrayOfPKs(checkedArray);
		//System.out.println(acCustomerList.size());
		//System.out.println(acCustomerList.get(0).getCustomer_pk());
		
		/*

		ArrayList<AC_Customer> acCustomerSecondServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerThiredServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerfirstAMCServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerSecondAMCServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerThiredAMCServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerforthServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerfifthServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomersixthServiceList = new ArrayList<AC_Customer>();
		*/
		ArrayList<PrintModel> printModelList = new ArrayList<PrintModel>();
		
		int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		Date forthServiceDate=null;
		Date fifthServiceDate=null;
		Date sixthServiceDate=null;
		
		//System.out.println("List.size:"+ acCustomerList.size());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format.parse(filterInfo[0]);
			endDate= format.parse(filterInfo[1]);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		PrintModel printModel;
		for(int i=0;i<acCustomerList.size();i++)
		{
			convertionFlag=0;
			listAddedFlag=0;

			firstServiceDate=null;
			secondServiceDate=null;
			thiredServiceDate=null;
			forthServiceDate=null;
			fifthServiceDate=null;
			sixthServiceDate=null;
			
			try
			{
				System.out.println(acCustomerList.get(i).getCustomer_pk()+":"+acCustomerList.get(i).getFirstFree());

			    	firstServiceDate = format.parse(acCustomerList.get(i).getFirstFree());
			    	//System.out.println(acCustomerList.get(i).getFirstFree());
			    	//System.out.println(format.format(firstServiceDate));
					if((!firstServiceDate.after(endDate))&&(!firstServiceDate.before(startDate)))
					{
						printModel = new PrintModel();
						printModel.setServiceNumber(1);
						printModel.setAc_Customer(acCustomerList.get(i));
						printModelList.add(printModel);
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    secondServiceDate = format.parse(acCustomerList.get(i).getSecondFree());
				if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
				{
					printModel = new PrintModel();
					printModel.setServiceNumber(2);
					printModel.setAc_Customer(acCustomerList.get(i));
					printModelList.add(printModel);
				}
			}
			catch(Exception ex)
			{
				
			}
			    
			try
			{
			    thiredServiceDate = format.parse(acCustomerList.get(i).getThirdFree());
			    if((!thiredServiceDate.after(endDate))&&(!thiredServiceDate.before(startDate)))
				{
					printModel = new PrintModel();
					printModel.setServiceNumber(3);
					printModel.setAc_Customer(acCustomerList.get(i));
					printModelList.add(printModel);
			    }
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	forthServiceDate = format.parse(acCustomerList.get(i).getForth_Service());
			    	if((!forthServiceDate.after(endDate))&&(!forthServiceDate.before(startDate)))
					{
						printModel = new PrintModel();
						printModel.setServiceNumber(4);
						printModel.setAc_Customer(acCustomerList.get(i));
						printModelList.add(printModel);
					}
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	fifthServiceDate = format.parse(acCustomerList.get(i).getFifth_Service());
			    	if((!fifthServiceDate.after(endDate))&&(!fifthServiceDate.before(startDate)))
					{
						printModel = new PrintModel();
						printModel.setServiceNumber(5);
						printModel.setAc_Customer(acCustomerList.get(i));
						printModelList.add(printModel);
					}
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	sixthServiceDate = format.parse(acCustomerList.get(i).getSixth_Service());
			    	if((!sixthServiceDate.after(endDate))&&(!sixthServiceDate.before(startDate)))
					{
						printModel = new PrintModel();
						printModel.setServiceNumber(6);
						printModel.setAc_Customer(acCustomerList.get(i));
						printModelList.add(printModel);
					}
			}
			catch(Exception ex)
			{
			
			}
			
		}
		//System.out.println("acCustomerforthServiceList.size"+acCustomerforthServiceList.size());
		//System.out.println(acCustomerforthServiceList.get(0).getCustomer_pk());		
		if(PDFGenerator.AllServicePDFGeneratorDBOder(printModelList))
		{
			for(int i=0;i<acCustomerList.size();i++)
			{
				acCustomerList.get(i).setFirstFreePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerList.get(i));
			}
		}
		return;
		/*switch(filterInfoArray[0])
		{
			case 0:
				if(PDFGenerator.firstServicePDFGenerator(acCustomerList))
				{
					for(int i=0;i<acCustomerList.size();i++)
					{
						acCustomerList.get(i).setFirstFreeStatus(1);
						customerDetails.updateAC_Customer(acCustomerList.get(i));
					}
				}
				break;
			case 1:
				if(PDFGenerator.secondServicePDFGenerator(acCustomerList))
				{
					for(int i=0;i<acCustomerList.size();i++)
					{
						acCustomerList.get(i).setSecondFreeStatus(1);
						customerDetails.updateAC_Customer(acCustomerList.get(i));
					}
				}
				break;
			case 2:
				if(PDFGenerator.thiredServicePDFGenerator(acCustomerList))
				{
					for(int i=0;i<acCustomerList.size();i++)
					{
						acCustomerList.get(i).setThirdFreeStatus(1);
						customerDetails.updateAC_Customer(acCustomerList.get(i));
					}
				}
				break;
		}*/
	}
	
	public ArrayList<AC_Customer> filterNotDoneServices(String fromDate,String toDate)
	{
		ArrayList<AC_Customer> acCustomerListtosend = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		acCustomerList =customerDetails.selectCustomerDetailsBySixOrCriterias("firstFreeStatus",0,"secondFreeStatus",0,"thirdFreeStatus",0,"forthServiceStatus",0,"fifthServiceStatus",0,"sixthServiceStatus",0);
		
		int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		
		System.out.println("List.size:"+ acCustomerList.size());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format.parse(fromDate);
			endDate= format.parse(toDate);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return acCustomerListtosend;
		}
		

		for(int i=0;i<acCustomerList.size();i++)
		{
			convertionFlag=0;
			listAddedFlag=0;

			
			firstServiceDate=null;
			secondServiceDate=null;
			thiredServiceDate=null;
			try
			{
				System.out.println(acCustomerList.get(i).getCustomer_pk()+":"+acCustomerList.get(i).getFirstFree());

			    	firstServiceDate = format.parse(acCustomerList.get(i).getFirstFree());
					if((!firstServiceDate.after(endDate))&&(!firstServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFirstFreeStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSecondFree());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSecondFreeStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			    
			try
			{
			    	thiredServiceDate = format.parse(acCustomerList.get(i).getThirdFree());
			    	if((!thiredServiceDate.after(endDate))&&(!thiredServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getThirdFreeStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getForth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getForthServiceStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getFifth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFifthServiceStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSixth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSixthServiceStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			if(listAddedFlag == 1)
			{
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}
		return acCustomerListtosend;
		/*
		Date toDay = new Date();
		
		Date startDate = new Date(toDay.getTime());
		
		Date endDate = new Date();
		switch(timeSelected)
		{
			case 0:
				endDate = new Date(toDay.getTime() + (1000 * 60 * 60 * 24 * 7));
				System.out.println("case 0");
				break;
			case 1:
				endDate = new Date(toDay.getTime() + (1000 * 60 * 60 * 24 * 14));
				System.out.println("end date"+endDate);
				break;
			case 2:
				endDate = new Date(toDay.getTime() + (1000 * 60 * 60 * 24 * 21));
				System.out.println("case 2");
				break;
			case 3:
				endDate = new Date(toDay.getTime() + (1000 * 60 * 60 * 24 * 28));
				System.out.println("case 3");
				break;
		}
		
		switch (serviceSelected)
		{
			case 0:
				acCustomerList =customerDetails.selectCustomerDetailsByOneCriteria("firstFreeStatus",0);
				break;
			case 1:
				acCustomerList =customerDetails.selectCustomerDetailsByOneCriteria("secondFreeStatus",0);
				break;
			case 2:
			
				acCustomerList =customerDetails.selectCustomerDetailsByOneCriteria("thirdFreeStatus",0);
				break;
		}
		int convertionFlag=0;
		System.out.println("List.size:"+acCustomerList.size());
		for(int i=0;i<acCustomerList.size();i++)
		{
			convertionFlag=0;
			DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			Date firstServiceDate=null;
			try
			{
				System.out.println(acCustomerList.get(i).getCustomer_pk()+":"+acCustomerList.get(i).getFirstFree());
				firstServiceDate = format.parse(acCustomerList.get(i).getFirstFree());
				System.out.println(firstServiceDate);
			}
			catch(Exception ex)
			{
				System.out.println("Method:filterService"+ex);
				convertionFlag=1;
			}
			if(convertionFlag == 1)
			{
				//acCustomerList.add(acCustomerList.get(i));
			}
			else if((!firstServiceDate.after(endDate))&&(!firstServiceDate.before(startDate)))
			{
				System.out.println("came in");
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}*/
	}
	public ArrayList<AC_Customer> filterNotDoneNotAllServices(String fromDate,String toDate,String amcOrFree) 
	{
		ArrayList<AC_Customer> acCustomerListtosend = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		acCustomerList =customerDetails.selectCustomerDetailsBySixOrCriterias("firstFreeStatus",0,"secondFreeStatus",0,"thirdFreeStatus",0,"forthServiceStatus",0,"fifthServiceStatus",0,"sixthServiceStatus",0);
		
		int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		
		System.out.println("List.size:"+ acCustomerList.size());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format.parse(fromDate);
			endDate= format.parse(toDate);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return acCustomerListtosend;
		}
		int amc=0;
		if(Integer.parseInt(amcOrFree) == 1)
		{
			amc=1;
		}
		else if(Integer.parseInt(amcOrFree) == 2)
		{
			amc=0;
		}

		for(int i=0;i<acCustomerList.size();i++)
		{
			if(acCustomerList.get(i).getAmc() != amc)continue;
			convertionFlag=0;
			listAddedFlag=0;

			
			firstServiceDate=null;
			secondServiceDate=null;
			thiredServiceDate=null;
			try
			{
				System.out.println(acCustomerList.get(i).getCustomer_pk()+":"+acCustomerList.get(i).getFirstFree());

			    	firstServiceDate = format.parse(acCustomerList.get(i).getFirstFree());
					if((!firstServiceDate.after(endDate))&&(!firstServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFirstFreeStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSecondFree());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSecondFreeStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			    
			try
			{
			    	thiredServiceDate = format.parse(acCustomerList.get(i).getThirdFree());
			    	if((!thiredServiceDate.after(endDate))&&(!thiredServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getThirdFreeStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getForth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getForthServiceStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getFifth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFifthServiceStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSixth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSixthServiceStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			if(listAddedFlag == 1)
			{
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}
		return acCustomerListtosend;
	}
	public ArrayList<AC_Customer> filterAllService(String fromDate,String toDate)
	{
		ArrayList<AC_Customer> acCustomerListtosend = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();

		
		int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		
		System.out.println("List.size:"+ acCustomerList.size());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format.parse(fromDate);
			endDate= format.parse(toDate);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return acCustomerListtosend;
		}
		acCustomerList = customerDetails.selectAC_CustomersBetweenTwoDays(fromDate,toDate);

		for(int i=0;i<acCustomerList.size();i++)
		{
			convertionFlag=0;
			listAddedFlag=0;

			
			firstServiceDate=null;
			secondServiceDate=null;
			thiredServiceDate=null;
			try
			{
				System.out.println(acCustomerList.get(i).getCustomer_pk()+":"+acCustomerList.get(i).getFirstFree());

			    	firstServiceDate = format.parse(acCustomerList.get(i).getFirstFree());
					if((!firstServiceDate.after(endDate))&&(!firstServiceDate.before(startDate)))
					{
						listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSecondFree());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			    
			try
			{
			    	thiredServiceDate = format.parse(acCustomerList.get(i).getThirdFree());
			    	if((!thiredServiceDate.after(endDate))&&(!thiredServiceDate.before(startDate)))
					{
						listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getForth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getFifth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSixth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			if(listAddedFlag == 1)
			{
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}
		return acCustomerListtosend;
	}
	public ArrayList<AC_Customer> filterAllNotAllService(String fromDate,String toDate,String amcOrFree)
	{
		ArrayList<AC_Customer> acCustomerListtosend = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();

		
		int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		
		System.out.println("List.size:"+ acCustomerList.size());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format.parse(fromDate);
			endDate= format.parse(toDate);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return acCustomerListtosend;
		}
		acCustomerList = customerDetails.selectAC_CustomersBetweenTwoDays(fromDate,toDate);
		int amc=0;
		if(Integer.parseInt(amcOrFree) == 1)
		{
			amc=1;
		}
		else if(Integer.parseInt(amcOrFree) == 2)
		{
			amc=0;
		}
		for(int i=0;i<acCustomerList.size();i++)
		{
			convertionFlag=0;
			listAddedFlag=0;

			
			firstServiceDate=null;
			secondServiceDate=null;
			thiredServiceDate=null;

			if(acCustomerList.get(i).getAmc() == amc)listAddedFlag = 1;
			if(listAddedFlag == 1)
			{
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}
		return acCustomerListtosend;
	}
	public ArrayList<AC_Customer> filterNotPrintedService(String fromDate,String toDate)
	{
		ArrayList<AC_Customer> acCustomerListtosend = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		acCustomerList =customerDetails.selectCustomerDetailsBySixOrCriterias("firstFreePrintStatus",0,"secondFreePrintStatus",0,"thirdFreePrintStatus",0,"forthServicePrintStatus",0,"fifthServicePrintStatus",0,"sixthServicePrintStatus",0);
		
		int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		
		System.out.println("List.size:"+ acCustomerList.size());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format.parse(fromDate);
			endDate= format.parse(toDate);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return acCustomerListtosend;
		}
		

		for(int i=0;i<acCustomerList.size();i++)
		{
			convertionFlag=0;
			listAddedFlag=0;

			
			firstServiceDate=null;
			secondServiceDate=null;
			thiredServiceDate=null;
			try
			{
				System.out.println(acCustomerList.get(i).getCustomer_pk()+":"+acCustomerList.get(i).getFirstFree());

			    	firstServiceDate = format.parse(acCustomerList.get(i).getFirstFree());
					if((!firstServiceDate.after(endDate))&&(!firstServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFirstFreePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSecondFree());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSecondFreePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			    
			try
			{
			    	thiredServiceDate = format.parse(acCustomerList.get(i).getThirdFree());
			    	if((!thiredServiceDate.after(endDate))&&(!thiredServiceDate.before(startDate)))
					{
			    		if(acCustomerList.get(i).getThirdFreePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getForth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getForthServicePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getFifth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFifthServicePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSixth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSixthServicePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			if(listAddedFlag == 1)
			{
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}
		return acCustomerListtosend;
	}
	public ArrayList<AC_Customer> filterNotPrintedNotAllService(String fromDate, String toDate,String amcOrFree) 
	{
		ArrayList<AC_Customer> acCustomerListtosend = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		acCustomerList =customerDetails.selectCustomerDetailsBySixOrCriterias("firstFreePrintStatus",0,"secondFreePrintStatus",0,"thirdFreePrintStatus",0,"forthServicePrintStatus",0,"fifthServicePrintStatus",0,"sixthServicePrintStatus",0);
		
		int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		
		System.out.println("List.size:"+ acCustomerList.size());
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format.parse(fromDate);
			endDate= format.parse(toDate);
		} 
		catch (ParseException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return acCustomerListtosend;
		}
		
		int amc=0;
		if(Integer.parseInt(amcOrFree) == 1)
		{
			amc=1;
		}
		else if(Integer.parseInt(amcOrFree) == 2)
		{
			amc=0;
		}
		for(int i=0;i<acCustomerList.size();i++)
		{
			if(acCustomerList.get(i).getAmc() != amc)continue;
			convertionFlag=0;
			listAddedFlag=0;

			
			firstServiceDate=null;
			secondServiceDate=null;
			thiredServiceDate=null;
			try
			{
				System.out.println(acCustomerList.get(i).getCustomer_pk()+":"+acCustomerList.get(i).getFirstFree());

			    	firstServiceDate = format.parse(acCustomerList.get(i).getFirstFree());
					if((!firstServiceDate.after(endDate))&&(!firstServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFirstFreePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSecondFree());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSecondFreePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			    
			try
			{
			    	thiredServiceDate = format.parse(acCustomerList.get(i).getThirdFree());
			    	if((!thiredServiceDate.after(endDate))&&(!thiredServiceDate.before(startDate)))
					{
			    		if(acCustomerList.get(i).getThirdFreePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
			
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getForth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getForthServicePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getFifth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getFifthServicePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			try
			{
			    	secondServiceDate = format.parse(acCustomerList.get(i).getSixth_Service());
					if((!secondServiceDate.after(endDate))&&(!secondServiceDate.before(startDate)))
					{
						if(acCustomerList.get(i).getSixthServicePrintStatus() == 0)listAddedFlag = 1;
					}
			}
			catch(Exception ex)
			{
				
			}
			if(listAddedFlag == 1)
			{
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}

		return acCustomerListtosend;
	}
	private String createFirstServiceDate(AC_Customer ac_Customer)
	{
		String stringCellValue = null;
		String serviceDate = null;
		Date dateCellValue = null;
		try
		{
			if(ac_Customer.getInstallationDate() != null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(ac_Customer.getInstallationDate()));
				c.add(Calendar.DATE, 90);  // number of days to add
				sdf = new SimpleDateFormat("yyyy-MM-dd");
				serviceDate = sdf.format(c.getTime());
			}
		}
		catch(Exception ex)
		{
			System.out.println("DataManager.createFirstServiceDate"+ex);
		}
		return serviceDate;

	}
	private String createSecondServiceDate(AC_Customer ac_Customer)
	{
		String stringCellValue = null;
		String serviceDate = null;
		Date dateCellValue = null;
		try
		{
			if(ac_Customer.getInstallationDate() != null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(ac_Customer.getInstallationDate()));
				c.add(Calendar.DATE,180); // number of days to add
				sdf = new SimpleDateFormat("yyyy-MM-dd");
				serviceDate = sdf.format(c.getTime());
			}
		}
		catch(Exception ex)
		{
			System.out.println("DataManager.createFirstServiceDate"+ex);
		}
		return serviceDate;
	}
	private String createThiredServiceDate(AC_Customer ac_Customer)
	{
		String stringCellValue = null;
		String serviceDate = null;
		Date dateCellValue = null;
		try
		{
			if(ac_Customer.getInstallationDate() != null)
			{
				SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
				Calendar c = Calendar.getInstance();
				c.setTime(sdf.parse(ac_Customer.getInstallationDate()));
				c.add(Calendar.DATE, 270);  // number of days to add
				sdf = new SimpleDateFormat("yyyy-MM-dd");
				serviceDate = sdf.format(c.getTime());
			}
		}
		catch(Exception ex)
		{
			System.out.println("DataManager.createFirstServiceDate"+ex);
		}
		return serviceDate;
	}
	public ArrayList<AC_Customer> search(String searchColumn,String searchKeyWords)
	{
		int intigerSearchColumn = Integer.parseInt(searchColumn);
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		switch (intigerSearchColumn)
		{
		case 0:
			acCustomerList = customerDetails.searchByColumn("installationDate",searchKeyWords);
			break;
		case 1:
			acCustomerList = customerDetails.searchByColumnMiddle("jobNo",searchKeyWords);
			break;
		case 2:
			acCustomerList = customerDetails.searchByColumn("customerName",searchKeyWords);
			break;
		case 3:
			acCustomerList = customerDetails.searchByColumnMiddle("customerAdress",searchKeyWords);
			break;
		case 4:
			acCustomerList = customerDetails.searchByColumn("city",searchKeyWords);
			break;
		case 5:
			acCustomerList = customerDetails.searchByColumn("contact",searchKeyWords);
			break;
		case 6:
			acCustomerList = customerDetails.searchByColumn("contactNo",searchKeyWords);
			break;
			
		case 7:
			acCustomerList = customerDetails.searchByColumn("type",searchKeyWords);
			break;
		case 8:
			acCustomerList = customerDetails.searchByColumn("unit",searchKeyWords);
			break;
		case 9:
			acCustomerList = customerDetails.searchByColumn("capacity",searchKeyWords);
			break;
		case 10:
			acCustomerList = customerDetails.searchByColumn("serialNumberIndoor",searchKeyWords);
			break;
		case 11:
			acCustomerList = customerDetails.searchByColumn("serialNumberOutdoor",searchKeyWords);
			break;
		case 12:
			acCustomerList = customerDetails.searchByColumn("no_name",searchKeyWords);
			break;
		case 13:
			acCustomerList = customerDetails.searchByColumn("instNo",searchKeyWords);
			break;
		case 14:
			acCustomerList = customerDetails.searchByColumn("intalledBy",searchKeyWords);
			break;
		case 15:
			acCustomerList = customerDetails.searchByColumn("empty",searchKeyWords);
			break;
		case 16:
			acCustomerList = customerDetails.searchByColumn("serviceTeam",searchKeyWords);
			break;
		case 17:
			acCustomerList = customerDetails.searchByColumn("firstFree",searchKeyWords);
			break;
		case 18:
			acCustomerList = customerDetails.searchByColumn("secondFree",searchKeyWords);
			break;
		case 19:
			acCustomerList = customerDetails.searchByColumn("thirdFree",searchKeyWords);
			break;
		case 20:
			acCustomerList = customerDetails.searchByColumn("forth_Service",searchKeyWords);
			break;
		case 21:
			acCustomerList = customerDetails.searchByColumn("fifth_Service",searchKeyWords);
			break;
		case 22:
			acCustomerList = customerDetails.searchByColumn("sixth_Service",searchKeyWords);
			break;
			
		}
		System.out.println("before return customer list");
		return acCustomerList;
	}
	public ArrayList<AC_Customer> newlyEnteredCustomers()
	{
		
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		acCustomerList = customerDetails.takeLastThousandCustomers(10);
		return acCustomerList;
	}
	public void batchDeleteService(String[] checkedArray)
	{
		//ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		AC_Customer acCustomer = new AC_Customer();
		for(int i=0;i<checkedArray.length;i++)
		{
			acCustomer.setCustomer_pk(checkedArray[i]);
			customerDetails.DeleteCustomerRowByPK(acCustomer);
		}
		return;
	}
	public int testingTest()
	{
		return 1;
	}
	
	public ArrayList<AC_Customer> getCustomerListByArrayOfPKs(String[] idList)
	{
		ArrayList<AC_Customer> acCustomerList = customerDetails.selectCustomerDetailsByArrayOfPKs(idList);
		return acCustomerList;
	}
	public void pdfGenarateByService(String[] checkedArray,String[] serviceNo)
	{
		ArrayList<AC_Customer> acCustomerList = customerDetails.selectCustomerDetailsByArrayOfPKs(checkedArray);
		//int intSeviceNo=Integer.parses(serviceNo[0]);
		ArrayList<AC_Customer> acCustomerfirstServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerSecondServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerThiredServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerfirstAMCServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerSecondAMCServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerThiredAMCServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerforthServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerfifthServiceList = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomersixthServiceList = new ArrayList<AC_Customer>();
		
		System.out.println("serviceNo[0]:"+serviceNo[0]);
		if(serviceNo[0].equals("0"))
		{
			for(int i=0;i<acCustomerList.size();i++)
			{
				if(acCustomerList.get(i).getFirstFree() != null)
				{
					if(acCustomerList.get(i).getAmc()==1)
					{
						acCustomerfirstAMCServiceList.add(acCustomerList.get(i));
					}
					else
					{
						acCustomerfirstServiceList.add(acCustomerList.get(i));
					}
				}
			}
		}
		else if(serviceNo[0].equals("1"))
		{
			for(int i=0;i<acCustomerList.size();i++)
			{
				if(acCustomerList.get(i).getFirstFree() != null)
				{
					if(acCustomerList.get(i).getAmc()==1)
					{
						acCustomerSecondAMCServiceList.add(acCustomerList.get(i));
					}
					else
					{
						acCustomerSecondServiceList.add(acCustomerList.get(i));
					}
				}
			}
		}
		else if(serviceNo[0].equals("2"))
		{
			for(int i=0;i<acCustomerList.size();i++)
			{
				if(acCustomerList.get(i).getThirdFree() != null)
				{
					if(acCustomerList.get(i).getAmc()==1)
					{
						acCustomerThiredAMCServiceList.add(acCustomerList.get(i));
					}
					else
					{
						acCustomerThiredServiceList.add(acCustomerList.get(i));
					}
				}
			}
		}
		else if(serviceNo[0].equals("3"))
		{
			for(int i=0;i<acCustomerList.size();i++)
			{
				if(acCustomerList.get(i).getForth_Service() != null)
				{
					acCustomerforthServiceList.add(acCustomerList.get(i));
				}
			}
		}
		else if(serviceNo[0].equals("4"))
		{
			for(int i=0;i<acCustomerList.size();i++)
			{
				//if(acCustomerList.get(i).getFifth_Service()== "")System.out.println("empty");
				//if(acCustomerList.get(i).getFifth_Service()== null)System.out.println("null");
				
				if(acCustomerList.get(i).getFifth_Service()!= null)
				{
					acCustomerfifthServiceList.add(acCustomerList.get(i));
				}
			}
		}
		else if(serviceNo[0].equals("5"))
		{
			for(int i=0;i<acCustomerList.size();i++)
			{
				if(acCustomerList.get(i).getSixth_Service() != null)
				{
					acCustomersixthServiceList.add(acCustomerList.get(i));
				}
			}
		}
		if(PDFGenerator.AllServicePDFGenerator(acCustomerfirstServiceList,acCustomerSecondServiceList,acCustomerThiredServiceList,acCustomerfirstAMCServiceList,acCustomerSecondAMCServiceList,acCustomerThiredAMCServiceList,acCustomerforthServiceList,acCustomerfifthServiceList,acCustomersixthServiceList))
		{
			for(int i=0;i<acCustomerfirstServiceList.size();i++)
			{
				acCustomerfirstServiceList.get(i).setFirstFreePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerfirstServiceList.get(i));
			}
			for(int i=0;i<acCustomerSecondServiceList.size();i++)
			{
				acCustomerSecondServiceList.get(i).setSecondFreePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerSecondServiceList.get(i));
			}
			for(int i=0;i<acCustomerThiredServiceList.size();i++)
			{
				acCustomerThiredServiceList.get(i).setThirdFreePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerThiredServiceList.get(i));
			}
			//------------------------------------AMC-------------------------------------
			
			for(int i=0;i<acCustomerfirstAMCServiceList.size();i++)
			{
				acCustomerfirstAMCServiceList.get(i).setFirstFreePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerfirstAMCServiceList.get(i));
			}
			for(int i=0;i<acCustomerSecondAMCServiceList.size();i++)
			{
				acCustomerSecondAMCServiceList.get(i).setSecondFreePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerSecondAMCServiceList.get(i));
			}
			for(int i=0;i<acCustomerThiredAMCServiceList.size();i++)
			{
				acCustomerThiredAMCServiceList.get(i).setThirdFreePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerThiredAMCServiceList.get(i));
			}
			for(int i=0;i<acCustomerforthServiceList.size();i++)
			{
				acCustomerforthServiceList.get(i).setForthServicePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerforthServiceList.get(i));
			}
			for(int i=0;i<acCustomerfifthServiceList.size();i++)
			{
				acCustomerfifthServiceList.get(i).setFifthServicePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomerfifthServiceList.get(i));
			}
			for(int i=0;i<acCustomersixthServiceList.size();i++)
			{
				acCustomersixthServiceList.get(i).setSixthServicePrintStatus(1);
				customerDetails.updateAC_Customer(acCustomersixthServiceList.get(i));
			}
		}
		return;
	}
	private String instdateStringToMySqlDateString(String instDateString) throws Exception
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Date dateDate=sdf.parse(instDateString);
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		String mysqlDateString = sdf.format(dateDate);
	
		return mysqlDateString;
	}
	public long calculateNumberOfPages()
	{
		long numberOfRecords = customerDetails.getTotalNumberofCustomerRecoreds();
		long numberofPages = numberOfRecords/10;
		long r = numberOfRecords % 10;
		if (0 < r)
		{
			numberofPages=numberofPages+1;
		}
		return numberofPages;
	}
	public ArrayList<AC_Customer> getPageData(String pageNumberString)
	{
		int pageNumber = Integer.parseInt(pageNumberString);
		pageNumber=pageNumber-1;
		int firstRow = pageNumber * 10;
		int numOfRows = 10;
		return customerDetails.selectSetOfRows(firstRow,numOfRows);
	}
    /*public static Date addDays(Date date, int days) {
    GregorianCalendar cal = new GregorianCalendar();
    cal.setTime(date);
    cal.add(Calendar.DATE, days);         
    return cal.getTime();
	}*/
	public ArrayList<AC_Customer> filterByInstDate(String fromDate,String toDate,String amcOrFree) 
	{
		ArrayList<AC_Customer> acCustomerListtosend = new ArrayList<AC_Customer>();
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();

		/*int convertionFlag = 0;
		int listAddedFlag = 0;

		Date firstServiceDate=null;
		Date secondServiceDate=null;
		Date thiredServiceDate=null;
		
		System.out.println("List.size:"+ acCustomerList.size());*/
		
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date startDate;
		Date endDate;
		try 
		{
			startDate = format1.parse(fromDate);
			endDate= format1.parse(toDate);
			//System.out.println("came to hear");
			fromDate = format2.format(startDate);
			toDate = format2.format(endDate);
			//System.out.println("came to hear");
		} 
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("exception:"+ e);
			return acCustomerListtosend;
		}
		acCustomerList = customerDetails.selectAC_CustomersBetweenTwoInstallatoinDates(fromDate,toDate);
		int amc=0;
		if(Integer.parseInt(amcOrFree) == 1)
		{
			amc=1;
		}
		else if(Integer.parseInt(amcOrFree) == 2)
		{
			amc=0;
		}
		else
		{
			return acCustomerList;
		}
		int listAddedFlag=0;
		for(int i=0;i<acCustomerList.size();i++)
		{   
			listAddedFlag=0;
			if(acCustomerList.get(i).getAmc() == amc)listAddedFlag = 1;
			if(listAddedFlag == 1)
			{
				acCustomerListtosend.add(acCustomerList.get(i));
			}
		}
		return acCustomerListtosend;
	}
	
	public void editCustomerDataByUserLevel(String pk, String stringMonth,String installationDate, 
			String jobNo, String customerName,
			String customerAdress, String city, String contact,
			String contactNo, String type, String unit, String capacity,
			String serialNumberIndoor, String serialNumberOutdoor,
			String no_name, String instNo, String intalledBy,
			String serviceTeam, String firstFree, String secondFree,
			String thirdFree, String forthService, String fifthService,
			String sixthService,String serviceType,String comments) 
	{
		AC_Customer acCustomertoEdit= customerDetails.selectDetailsFromAC_customer_pk(pk);
		System.out.println("pk"+pk);
		acCustomertoEdit.setCustomer_pk(pk);
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			Date tempDate = format.parse(stringMonth);
			format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(tempDate);
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		finally
		{
			acCustomertoEdit.setMonth(stringMonth);
		}
		
		acCustomertoEdit.setInstallationDate(installationDate);
		acCustomertoEdit.setJobNo(jobNo);
		acCustomertoEdit.setCustomerName(customerName);
		acCustomertoEdit.setCustomerAdress(customerAdress);
		acCustomertoEdit.setCity(city);
		acCustomertoEdit.setContact(contact);
		acCustomertoEdit.setContactNo(contactNo);
		acCustomertoEdit.setType(type);
		acCustomertoEdit.setUnit(unit);
		acCustomertoEdit.setCapacity(capacity);
		acCustomertoEdit.setSerialNumberIndoor(serialNumberIndoor);
		acCustomertoEdit.setSerialNumberOutdoor(serialNumberOutdoor);
		acCustomertoEdit.setNo_name(no_name);
		acCustomertoEdit.setInstNo(instNo);
		acCustomertoEdit.setIntalledBy(intalledBy);

		acCustomertoEdit.setServiceTeam(serviceTeam);

		if(!firstFree.equals(""))
		{
			acCustomertoEdit.setFirstFree(firstFree);
		}
		if(!secondFree.equals(""))
		{
			acCustomertoEdit.setSecondFree(secondFree);
		}
		if(!thirdFree.equals(""))
		{
			acCustomertoEdit.setThirdFree(thirdFree);
		}
		if(!forthService.equals(""))
		{
			acCustomertoEdit.setForth_Service(forthService);
		}
		if(!fifthService.equals(""))
		{
			acCustomertoEdit.setFifth_Service(fifthService);
		}
		if(!sixthService.equals(""))
		{
			acCustomertoEdit.setSixth_Service(sixthService);
		}
		System.out.println("serviceType :" + serviceType);
		if(serviceType.equals("amc"))
		{System.out.println("came amc in");
			acCustomertoEdit.setAmc(1);
		}
		else if(serviceType.equals("free"))
		{System.out.println("came free in");
			acCustomertoEdit.setAmc(0);
		}
		try
		{
			acCustomertoEdit.setDateInstDate(instdateStringToMySqlDateString(acCustomertoEdit.getInstallationDate()));
		}
		catch(Exception ex)
		{
			acCustomertoEdit.setReadError(1);
		}
		acCustomertoEdit.setComments(comments);
		customerDetails.updateAC_Customer(acCustomertoEdit);
		
	}
	public void manageInsertingDataByApprover(String stringMonth,String installationDate,String jobNo,String customerName,String customerAdress,String city,String contact,String contactNo,String type,String unit,String capacity,String serialNumberIndoor,String serialNumberOutdoor,String no_name,String instNo,String intalledBy,String serviceTeam,String frequency,String comment) 
	{
		AC_Customer acCustomertoInsert= new AC_Customer();
		
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			Date tempDate = format.parse(stringMonth);
			format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(tempDate);
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		finally
		{
			acCustomertoInsert.setMonth(stringMonth);
		}
		
		acCustomertoInsert.setInstallationDate(installationDate);
		acCustomertoInsert.setJobNo(jobNo);
		acCustomertoInsert.setCustomerName(customerName);
		acCustomertoInsert.setCustomerAdress(customerAdress);
		acCustomertoInsert.setCity(city);
		acCustomertoInsert.setContact(contact);
		acCustomertoInsert.setContactNo(contactNo);
		acCustomertoInsert.setType(type);
		acCustomertoInsert.setUnit(unit);
		acCustomertoInsert.setCapacity(capacity);
		acCustomertoInsert.setSerialNumberIndoor(serialNumberIndoor);
		acCustomertoInsert.setSerialNumberOutdoor(serialNumberOutdoor);
		acCustomertoInsert.setNo_name(no_name);
		acCustomertoInsert.setInstNo(instNo);
		acCustomertoInsert.setIntalledBy(intalledBy);
		acCustomertoInsert.setFrequency(frequency);
		acCustomertoInsert.setServiceTeam(serviceTeam);
		
		
		try
		{
			int intFrequency = Integer.parseInt(acCustomertoInsert.getFrequency());
			int intervalDays = this.calculateIntervalDays(intFrequency);
			if(1 <= intFrequency)
			{
				String firstServiceDate = this.calculateAnyServiceDate(installationDate,intervalDays,1);
				acCustomertoInsert.setFirstFree(firstServiceDate);
			}
			if(2 <= intFrequency)
			{
				String secondServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,2);
				acCustomertoInsert.setSecondFree(secondServiceDate);
			}
			if(3 <= intFrequency)
			{
				String thiredServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,3);
				acCustomertoInsert.setThirdFree(thiredServiceDate);
			}
			if(4 <= intFrequency)
			{
				String forthServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,4);
				acCustomertoInsert.setForth_Service(forthServiceDate);
			}
			if(5 <= intFrequency)
			{
				String fifthServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,5);
				acCustomertoInsert.setFifth_Service(fifthServiceDate);
			}
			if(6 <= intFrequency)
			{
				String SixthServiceDate=this.calculateAnyServiceDate(installationDate,intervalDays,6);
				acCustomertoInsert.setSixth_Service(SixthServiceDate);
			}
			
			/*if(firstFreeStatus.length==2)
			{
				acCustomertoInsert.setFirstFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setFirstFreeStatus(0);
			}
			if(secondFreeStatus.length==2)
			{
				acCustomertoInsert.setSecondFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setSecondFreeStatus(0);
			}
			if(thirdFreeStatus.length==2)
			{
				acCustomertoInsert.setThirdFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setThirdFreeStatus(0);
			}*/
			
			
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
			
		}
		try
		{
			acCustomertoInsert.setDateInstDate(instdateStringToMySqlDateString(acCustomertoInsert.getInstallationDate()));
		}
		catch(Exception ex)
		{
			acCustomertoInsert.setReadError(1);
		}
		acCustomertoInsert.setAmc(1);
		acCustomertoInsert.setComments(comment);
		customerDetails.insertMysql(acCustomertoInsert);
		//customerDetails.createExcelFile();
		
	}
	public void manageInsertingFreeServiceDataByApprover(String stringMonth,String installationDate,String jobNo,String customerName,String customerAdress,String city,String contact,String contactNo,String type,String unit,String capacity,String serialNumberIndoor,String serialNumberOutdoor,String no_name,String instNo,String intalledBy,String serviceTeam,String comment) 
	{
		AC_Customer acCustomertoInsert= new AC_Customer();
		
		try
		{
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
			Date tempDate = format.parse(stringMonth);
			format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
			stringMonth = format.format(tempDate);
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		finally
		{
			acCustomertoInsert.setMonth(stringMonth);
		}
		try
		{
		//acCustomertoInsert.setMonth(installationDate);
		acCustomertoInsert.setInstallationDate(installationDate);
		acCustomertoInsert.setJobNo(jobNo);
		acCustomertoInsert.setCustomerName(customerName);
		acCustomertoInsert.setCustomerAdress(customerAdress);
		acCustomertoInsert.setCity(city);
		acCustomertoInsert.setContact(contact);
		acCustomertoInsert.setContactNo(contactNo);
		acCustomertoInsert.setType(type);
		acCustomertoInsert.setUnit(unit);
		acCustomertoInsert.setCapacity(capacity);
		acCustomertoInsert.setSerialNumberIndoor(serialNumberIndoor);
		acCustomertoInsert.setSerialNumberOutdoor(serialNumberOutdoor);
		acCustomertoInsert.setNo_name(no_name);
		acCustomertoInsert.setInstNo(instNo);
		acCustomertoInsert.setIntalledBy(intalledBy);
		//acCustomertoInsert.setEmpty(frequency);
		acCustomertoInsert.setServiceTeam(serviceTeam);
		
		String firstServiceDate = this.createFirstServiceDate(acCustomertoInsert);
		acCustomertoInsert.setFirstFree(firstServiceDate);

		String secondServiceDate=this.createSecondServiceDate(acCustomertoInsert);
		acCustomertoInsert.setSecondFree(secondServiceDate);

		String thiredServiceDate=this.createThiredServiceDate(acCustomertoInsert);
		acCustomertoInsert.setThirdFree(thiredServiceDate);

			
			/*if(firstFreeStatus.length==2)
			{
				acCustomertoInsert.setFirstFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setFirstFreeStatus(0);
			}
			if(secondFreeStatus.length==2)
			{
				acCustomertoInsert.setSecondFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setSecondFreeStatus(0);
			}
			if(thirdFreeStatus.length==2)
			{
				acCustomertoInsert.setThirdFreeStatus(1);
			}
			else
			{
				acCustomertoInsert.setThirdFreeStatus(0);
			}*/
			
			
		}
		catch(Exception ex)
		{
			System.out.println("Exception:"+ex);
		}
		try
		{
			acCustomertoInsert.setDateInstDate(instdateStringToMySqlDateString(acCustomertoInsert.getInstallationDate()));
		}
		catch(Exception ex)
		{
			acCustomertoInsert.setReadError(1);
		}
		acCustomertoInsert.setComments(comment);
		customerDetails.insertMysql(acCustomertoInsert);
		
	}



}

