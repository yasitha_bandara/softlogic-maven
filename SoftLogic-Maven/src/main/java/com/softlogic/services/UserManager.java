package com.softlogic.services;

//import java.io.FileInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import org.hibernate.Transaction;
/*
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
*/
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.softlogic.daos.IUserManagerDao;
import com.softlogic.models.AC_Customer;
import com.softlogic.models.User;
import com.softlogic.models.UserStatus;
@Service
public class UserManager 
{
	//@Autowired
	//private SessionFactory sessionFactory;
	
	@Autowired
	private IUserManagerDao userManagerDao;
	
	public boolean createUserService(String userName,String passWord,String userType)
	{
		boolean exsist = userManagerDao.checkUserName(userName);
		if(exsist)
		{
			return false;
		}
		User user=new User();
		user.setUserName(userName);
		try
		{
			//The MessageDigest classes are NOT thread safe. If they're going to be used by different threads, just create a new one, instead of trying to reuse them. 
			//MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			//messageDigest.digest(passWord.getBytes());
			//System.out.println("md5:"+messageDigest.toString());;
			//System.out.println("md5:"+ hex);
			//md.update(password.getBytes())
			//String passwordHash = (new HexBinaryAdapter()).marshal(messageDigest.digest(passWord.getBytes()));
			//System.out.println("md5:"+ hex);
			String passwordHash = md5String(passWord);
			//System.out.println("md5:"+ passwordHash );
			user.setPassword(passwordHash);
		}
		catch(Exception ex)
		{
			System.out.println("Class:UserManager,Method:createUserService,password:passWord,"+ex);
			return false;
		}
		int usertype = Integer.parseInt(userType);
		switch(usertype)
		{
			case 0:
				user.setRole("User");
				break;
			case 1:
				user.setRole("Approver");
				break;
			case 2:
				user.setRole("Admin");
				break;
		}
		user.setStatus(UserStatus.ACTIVE);
		boolean trState = userManagerDao.insertUser(user); 
		return trState;
	}
    private String md5String(String passWord)throws Exception
    {
        MessageDigest md = MessageDigest.getInstance("MD5");

         md.update(passWord.getBytes());
 
        byte[] mdbytes = md.digest();
     
        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < mdbytes.length; i++) {
          sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        //System.out.println("Digest(in hex format):: " + sb.toString());
        
        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
    	for (int i=0;i<mdbytes.length;i++) {
    		String hex=Integer.toHexString(0xff & mdbytes[i]);
   	     	if(hex.length()==1) hexString.append('0');
   	     	hexString.append(hex);
    	}
    	return hexString.toString();
    }

    public ArrayList<User> getAllCustomers()
    {
    	ArrayList<User> userList = new ArrayList<User>();
    	userList = userManagerDao.selectAllUsers();
    	return userList;
    }
    public void  removeUser(String userId)
    {
		User user = new User();
		int intUserId = Integer.parseInt(userId);
		user.setId(intUserId);
		userManagerDao.DeleteUserRowByPK(user);
    }
	public User userDetailsFromUser_id(String user_id)
	{
		int intUserId = Integer.parseInt(user_id);
		User user = userManagerDao.selectDetailsFromUser_id(intUserId);
		return user;
	}
	public String[] createUserTypetoSelectedArray1(String userType)
	{
		String[] UserTypetoSelectedArray =  new String[3];
		if(userType.equals("0"))
		{
			UserTypetoSelectedArray[0] = "selected";
		}
		else if(userType.equals("1"))
		{
			UserTypetoSelectedArray[1] = "selected";
		}
		else if(userType.equals("2"))
		{
			UserTypetoSelectedArray[2] = "selected";	
		}
		return UserTypetoSelectedArray;
	}
	public String[] createUserTypetoSelectedArray2(String user_id)
	{
		String[] UserTypetoSelectedArray =  new String[3];
		int intUserId = Integer.parseInt(user_id);
		User user = userManagerDao.selectDetailsFromUser_id(intUserId);
		if(user.getRole().equals("User"))
		{
			UserTypetoSelectedArray[0] = "selected";
		}
		else if(user.getRole().equals("Approver"))
		{
			UserTypetoSelectedArray[1] = "selected";
		}
		else if(user.getRole().equals("Admin"))
		{
			UserTypetoSelectedArray[2] = "selected";	
		}
		return UserTypetoSelectedArray;
	}
	public void changeUser(String userId,String username,String userType)
	{
		User user = this.userDetailsFromUser_id(userId);
		user.setUserName(username);
		
		int usertype = Integer.parseInt(userType);
		switch(usertype)
		{
			case 0:
				user.setRole("User");
				break;
			case 1:
				user.setRole("Approver");
				break;
			case 2:
				user.setRole("Admin");
				break;
		}
		userManagerDao.updateUser(user);
	}
	public boolean changePassword(String userId,String password)
	{
		User user = this.userDetailsFromUser_id(userId);
		try
		{
			String passwordHash = this.md5String(password);
			user.setPassword(passwordHash);
		}
		catch(Exception ex)
		{
			System.out.println("Class:UserManager,Method:createUserService,password:passWord,"+ex);
			return false;
		}
		userManagerDao.updateUser(user);
		return true;
		
	}
	public boolean confirmPassword(String s1,String s2)
	{
		return true;
	}
}
