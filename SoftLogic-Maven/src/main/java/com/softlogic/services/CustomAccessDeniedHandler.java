package com.softlogic.services;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;
import org.springframework.security.web.csrf.InvalidCsrfTokenException;
import org.springframework.security.web.csrf.MissingCsrfTokenException;
import org.springframework.stereotype.Component;

//import com.softlogic.controllers.HelloWorldController;
@Component("customAccessDeniedHandler")
public class CustomAccessDeniedHandler extends AccessDeniedHandlerImpl
{
	static Logger log = Logger.getLogger(CustomAccessDeniedHandler.class.getName());
	@Override
    public void handle(HttpServletRequest request,HttpServletResponse response,AccessDeniedException accessDeniedException) 
	{
		try
		{
			System.out.println("AccessDeniedHandlerImpl:");
			if (accessDeniedException instanceof MissingCsrfTokenException || accessDeniedException instanceof InvalidCsrfTokenException) 
			{
				response.sendRedirect(request.getContextPath()+"/csrfTokenExpiredMessage");                                        
			}
			super.handle(request, response, accessDeniedException);
		}
		catch(Exception ex)
		{
			log.error("AccessDeniedHandlerImpl:" + ex);
		}
	}
}
