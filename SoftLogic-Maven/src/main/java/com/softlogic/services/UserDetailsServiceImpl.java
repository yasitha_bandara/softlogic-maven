package com.softlogic.services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.softlogic.daos.IUserDao;
//import com.softlogic.daos.UserDao;
import com.softlogic.models.User;
import com.softlogic.models.UserStatus;
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService
{
	@Autowired
	private IUserDao userDao;
	
	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException 
	{
		User user = userDao.finedUserByName(username);
		if(user!= null)
		{
			String password = user.getPassword();
			boolean enabled = user.getStatus().equals(UserStatus.ACTIVE);
			boolean accountNonExpired = user.getStatus().equals(UserStatus.ACTIVE);
			boolean credentialsNonExpired = user.getStatus().equals(UserStatus.ACTIVE);
			boolean accountNonLocker = user.getStatus().equals(UserStatus.ACTIVE);
			
			Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>(); 
			
			authorities.add(new SimpleGrantedAuthority(user.getRole()));
			org.springframework.security.core.userdetails.User securityUser = new org.springframework.security.core.userdetails.User(username,password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocker, authorities);
			return securityUser;
		}
		else
		{
			throw new UsernameNotFoundException("User Not Found !!!");
		}
	}

}
