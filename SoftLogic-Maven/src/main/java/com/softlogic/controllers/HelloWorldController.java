package com.softlogic.controllers;
import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.softlogic.models.AC_Customer;
import com.softlogic.models.User;
import com.softlogic.services.*;

@Controller/*annotation marks this class as spring bean which may handle different HTTP
			requests based on mapping specified on class or individual controller methods.*/
@RequestMapping("/")/*annotation is used for mapping web requests onto specific handler classes
					and/or handler methods. In our case, we have applied it on class level too, which says that this
					class is default handler for all HTTP requests of type �/�*/
public class HelloWorldController 
{
	@Autowired
	private UserManager userManager;
	@Autowired
	private DataManager dataManager;

	static Logger log = Logger.getLogger(HelloWorldController.class.getName());
	
	@RequestMapping(method = RequestMethod. GET)/*First method does not have any URL mapping declared, so it will inherit the mapping from
												mapping declared on class level, acting as default handler for GET requests.*/ 
	public ModelAndView first(ModelMap model) 
	{
		model.addAttribute("greeting" , "Hello World from Spring 4 MVC" );
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData("1"));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",1);
		return modelAndView;
	}
	@RequestMapping(value = "/index")
	public String index() 
	{ 
		return "index";
	}
	
	@RequestMapping(value = "/loginpage")
	public String doLogin() 
	{
			return "login";
	}
	@RequestMapping(value = "/showData" , method = RequestMethod. GET)
	public ModelAndView dummy(HttpSession session) 
	{
	    		ModelAndView modelAndView = new ModelAndView("showData");
	    		modelAndView.addObject("showData",dataManager.getPageData("1"));
	    		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
	    		modelAndView.addObject("currentPage",1);
	    		return modelAndView;

	}
	@RequestMapping(value = "/insertPage" , method = RequestMethod. GET)
	public ModelAndView insertPage(HttpSession session,Authentication authentication) 
	{
    	String role="User";
    	ModelAndView modelAndView = new ModelAndView("insertDataByApprover");
        
    	for (GrantedAuthority auth : authentication.getAuthorities()) 
        {
            if (role.equals(auth.getAuthority()))
            {
            	modelAndView = new ModelAndView("insertData");
            	break;
            }
    
        }
    	//ArrayList<Customer> customerList=dataManager.customerDetailsFrom_customer_pk(customer_pk);
    	return modelAndView;
	}
	@RequestMapping(value = "/insertFreeServiceDataPage" , method = RequestMethod. GET)
	public ModelAndView insertFreeServiceDataPage(HttpSession session,Authentication authentication) 
	{
    	String role="User";
    	ModelAndView modelAndView = new ModelAndView("insertFreeServiceDataByApprover");
        
    	for (GrantedAuthority auth : authentication.getAuthorities()) 
        {
            if (role.equals(auth.getAuthority()))
            {
            	modelAndView = new ModelAndView("insertFreeServiceData");
            	break;
            }
    
        }
    	return modelAndView;
		//return "insertFreeServiceData";
	}
	@RequestMapping(value = "/insert" , method = RequestMethod. POST)
	public ModelAndView insert(@RequestParam("month")String month,@RequestParam("installationDate")String installationDate,@RequestParam("jobNo")String jobNo,@RequestParam("customerName")String customerName,@RequestParam("customerAdress")String customerAdress,@RequestParam("city")String city,@RequestParam("contact")String contact,@RequestParam("contactNo")String contactNo,@RequestParam("type")String type,@RequestParam("unit")String unit,@RequestParam("capacity")String capacity,@RequestParam("serialNumberIndoor")String serialNumberIndoor,@RequestParam("serialNumberOutdoor")String serialNumberOutdoor,@RequestParam("no_name")String no_name,@RequestParam("instNo")String instNo,@RequestParam("intalledBy")String intalledBy,@RequestParam("serviceTeam")String serviceTeam,@RequestParam("frequency")String frequency,HttpSession session) 
	{
		dataManager.manageInsertingData(month,installationDate,jobNo,customerName,customerAdress,city,contact,contactNo,type,unit,capacity,serialNumberIndoor,serialNumberOutdoor,no_name,instNo,intalledBy,serviceTeam,frequency);
	    		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData("1"));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",1);
		return modelAndView;
	}
	@RequestMapping(value = "/insertByApprover" , method = RequestMethod. POST)
	public ModelAndView insertByApprover(@RequestParam("month")String month,@RequestParam("installationDate")String installationDate,@RequestParam("jobNo")String jobNo,@RequestParam("customerName")String customerName,@RequestParam("customerAdress")String customerAdress,@RequestParam("city")String city,@RequestParam("contact")String contact,@RequestParam("contactNo")String contactNo,@RequestParam("type")String type,@RequestParam("unit")String unit,@RequestParam("capacity")String capacity,@RequestParam("serialNumberIndoor")String serialNumberIndoor,@RequestParam("serialNumberOutdoor")String serialNumberOutdoor,@RequestParam("no_name")String no_name,@RequestParam("instNo")String instNo,@RequestParam("intalledBy")String intalledBy,@RequestParam("serviceTeam")String serviceTeam,@RequestParam("frequency")String frequency,String comments,HttpSession session) 
	{
		dataManager.manageInsertingDataByApprover(month,installationDate,jobNo,customerName,customerAdress,city,contact,contactNo,type,unit,capacity,serialNumberIndoor,serialNumberOutdoor,no_name,instNo,intalledBy,serviceTeam,frequency,comments);
	    		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData("1"));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",1);
		return modelAndView;
	}
	@RequestMapping(value = "/insertFreeServiceData" , method = RequestMethod. POST)
	public ModelAndView insertFreeServiceData(@RequestParam("month")String month,@RequestParam("installationDate")String installationDate,@RequestParam("jobNo")String jobNo,@RequestParam("customerName")String customerName,@RequestParam("customerAdress")String customerAdress,@RequestParam("city")String city,@RequestParam("contact")String contact,@RequestParam("contactNo")String contactNo,@RequestParam("type")String type,@RequestParam("unit")String unit,@RequestParam("capacity")String capacity,@RequestParam("serialNumberIndoor")String serialNumberIndoor,@RequestParam("serialNumberOutdoor")String serialNumberOutdoor,@RequestParam("no_name")String no_name,@RequestParam("instNo")String instNo,@RequestParam("intalledBy")String intalledBy,@RequestParam("serviceTeam")String serviceTeam,HttpSession session) 
	{
		dataManager.manageInsertingFreeServiceData(month,installationDate,jobNo,customerName,customerAdress,city,contact,contactNo,type,unit,capacity,serialNumberIndoor,serialNumberOutdoor,no_name,instNo,intalledBy,serviceTeam);
	    		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData("1"));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",1);
		return modelAndView;
	}
	@RequestMapping(value = "/insertFreeServiceDataByApprover" , method = RequestMethod. POST)
	public ModelAndView insertFreeServiceDataByApprover(@RequestParam("month")String month,@RequestParam("installationDate")String installationDate,@RequestParam("jobNo")String jobNo,@RequestParam("customerName")String customerName,@RequestParam("customerAdress")String customerAdress,@RequestParam("city")String city,@RequestParam("contact")String contact,@RequestParam("contactNo")String contactNo,@RequestParam("type")String type,@RequestParam("unit")String unit,@RequestParam("capacity")String capacity,@RequestParam("serialNumberIndoor")String serialNumberIndoor,@RequestParam("serialNumberOutdoor")String serialNumberOutdoor,@RequestParam("no_name")String no_name,@RequestParam("instNo")String instNo,@RequestParam("intalledBy")String intalledBy,@RequestParam("serviceTeam")String serviceTeam,String comments,HttpSession session) 
	{
		dataManager.manageInsertingFreeServiceDataByApprover(month,installationDate,jobNo,customerName,customerAdress,city,contact,contactNo,type,unit,capacity,serialNumberIndoor,serialNumberOutdoor,no_name,instNo,intalledBy,serviceTeam,comments);
	    		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData("1"));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",1);
		return modelAndView;
	}
	@RequestMapping(value = "/editpage" , method = RequestMethod. GET)
	public ModelAndView editpage(@RequestParam("customer_pk")String customer_pk,Authentication authentication) 
	{
	        	String role="User";
	        	ModelAndView modelAndView = new ModelAndView("editpage");
	            
	        	for (GrantedAuthority auth : authentication.getAuthorities()) 
	            {
	                if (role.equals(auth.getAuthority()))
	                {
	                	modelAndView = new ModelAndView("editbyUserPage");
	                	break;
	                }
	        
	            }
	        	//ArrayList<Customer> customerList=dataManager.customerDetailsFrom_customer_pk(customer_pk);
	    		
	    		modelAndView.addObject("customerToEdit",dataManager.customerDetailsFromAC_Customer_pk(customer_pk));
	        	return modelAndView;
	}
	@RequestMapping(value = "/editCustomer" , method = RequestMethod. POST)
	public ModelAndView editCustomer(@RequestParam("pk")String pk,@RequestParam("month")String month,@RequestParam("installationDate")String installationDate,@RequestParam("jobNo")String jobNo,@RequestParam("customerName")String customerName,@RequestParam("customerAdress")String customerAdress,@RequestParam("city")String city,@RequestParam("contact")String contact,@RequestParam("contactNo")String contactNo,@RequestParam("type")String type,@RequestParam("unit")String unit,@RequestParam("capacity")String capacity,@RequestParam("serialNumberIndoor")String serialNumberIndoor,@RequestParam("serialNumberOutdoor")String serialNumberOutdoor,@RequestParam("no_name")String no_name,@RequestParam("instNo")String instNo,@RequestParam("intalledBy")String intalledBy,@RequestParam("serviceTeam")String serviceTeam,@RequestParam("firstFree")String firstFree,@RequestParam("secondFree")String secondFree,@RequestParam("thirdFree")String thirdFree,@RequestParam("forthService")String forthService,@RequestParam("fifthService")String fifthService,@RequestParam("sixthService")String sixthService,@RequestParam("firstFreeStatus")String[] firstFreeStatus,@RequestParam("secondFreeStatus")String[] secondFreeStatus,@RequestParam("thirdFreeStatus")String[] thirdFreeStatus,@RequestParam("forthServiceStatus")String[] forthServiceStatus,@RequestParam("fifthServicetatus")String[] fifthServicetatus,@RequestParam("sixthServicetatus")String[] sixthServicetatus,@RequestParam("serviceType")String serviceType,@RequestParam("comments")String comments,HttpSession session) 
	{
		System.out.println("----------------check box values--------------:");
  
		dataManager.editCustomerData(pk,month,installationDate,jobNo,customerName,customerAdress,city,contact,contactNo,type,unit,capacity,serialNumberIndoor,serialNumberOutdoor,no_name,instNo,intalledBy,serviceTeam,firstFree,secondFree,thirdFree,forthService,fifthService,sixthService,firstFreeStatus,secondFreeStatus,thirdFreeStatus,forthServiceStatus,fifthServicetatus,sixthServicetatus,serviceType,comments);
	    		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData("1"));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",1);
		return modelAndView;
	}
	@RequestMapping(value = "/editCustomerbyUser" , method = RequestMethod. POST)
	public ModelAndView editCustomer(@RequestParam("pk")String pk,@RequestParam("month")String month,@RequestParam("installationDate")String installationDate,@RequestParam("jobNo")String jobNo,@RequestParam("customerName")String customerName,@RequestParam("customerAdress")String customerAdress,@RequestParam("city")String city,@RequestParam("contact")String contact,@RequestParam("contactNo")String contactNo,@RequestParam("type")String type,@RequestParam("unit")String unit,@RequestParam("capacity")String capacity,@RequestParam("serialNumberIndoor")String serialNumberIndoor,@RequestParam("serialNumberOutdoor")String serialNumberOutdoor,@RequestParam("no_name")String no_name,@RequestParam("instNo")String instNo,@RequestParam("intalledBy")String intalledBy,@RequestParam("serviceTeam")String serviceTeam,@RequestParam("firstFree")String firstFree,@RequestParam("secondFree")String secondFree,@RequestParam("thirdFree")String thirdFree,@RequestParam("forthService")String forthService,@RequestParam("fifthService")String fifthService,@RequestParam("sixthService")String sixthService,@RequestParam("serviceType")String serviceType,@RequestParam("comments")String comments) 
	{
		System.out.println("----------------check box values--------------:");
  
		dataManager.editCustomerDataByUserLevel(pk,month,installationDate,jobNo,customerName,customerAdress,city,contact,contactNo,type,unit,capacity,serialNumberIndoor,serialNumberOutdoor,no_name,instNo,intalledBy,serviceTeam,firstFree,secondFree,thirdFree,forthService,fifthService,sixthService,serviceType,comments);
	    		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData("1"));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",1);
		return modelAndView;
	}
	
	@RequestMapping(value = "/delete" , method = RequestMethod. GET)
	public ModelAndView deleteCustomer(@RequestParam("customer_pk")String customer_pk,HttpSession session) 
	{
		//log.error("Exiting the program");
		dataManager.removeCustomer(customer_pk);
	    
		ModelAndView modelAndView = new ModelAndView("showData");
	    modelAndView.addObject("showData",dataManager.getPageData("1"));
	    modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
	    modelAndView.addObject("currentPage",1);
	    return modelAndView;

	}
/*	@RequestMapping(value = "/readexcel" , method = RequestMethod. POST)
	public ModelAndView readExcel(@RequestParam("filename")String filename) 
	{
		dataManager.readExcel(filename);
    	ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.takeAllCustomerDetails());
 
		return modelAndView;
	}
*/
	@RequestMapping(value = "/readexcel" , method = RequestMethod. POST)
	public String readExcel(@RequestParam("file")MultipartFile file,@RequestParam("serviceType")String serviceType,@RequestParam("insertExcelFormId")String insertExcelFormId,HttpSession session) 
	{	
    	//System.out.println(serviceType);
		//log.error("came in to read excel function");
		boolean firstSubmit = false;
		boolean stringIdParsable=false;
		long longFormId = 0;
		try
		{
			longFormId = Long.valueOf(insertExcelFormId);
			stringIdParsable = true;
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			stringIdParsable=false;
		}
		ArrayList<Long> FormIdList= (ArrayList<Long>) session.getAttribute("insertExcelFormIds");
		if(FormIdList==null)
		{
			FormIdList = new ArrayList<Long>();
			firstSubmit = false;
		}
		else if(stringIdParsable)
		{
			for(int i=0;i<FormIdList.size();i++)
			{
				if(FormIdList.get(i) == longFormId)
				{
					firstSubmit = true;
					FormIdList.remove(i);
					session.setAttribute("insertExcelFormIds",FormIdList);
				}
			}
		}
			
		Integer redirected;
		if(session.getAttribute("redirected") != null)
		{
			redirected = (Integer) session.getAttribute("redirected");
		}
		else
		{
			redirected=1;
			session.setAttribute("redirected",redirected);
		}
		if(redirected == 0)
		{
			if(firstSubmit == true)
			{
				String[] serviceTypeArray = new String[2];
				if(serviceType.equals("amc"))
				{
					serviceTypeArray[0]="checked";
					serviceTypeArray[1]="";
				}
				if(serviceType.equals("free"))
				{
					serviceTypeArray[0]="";
					serviceTypeArray[1]="checked";
				}
				
				File dir=null;
				if (!file.isEmpty())
				{ 
					System.out.println("!file.isEmpty"); 
					try
					{
		
						 byte[] bytes = file.getBytes();
						 
			                // Creating the directory to store file
			                String rootPath = System.getProperty("catalina.home");
			                dir = new File(rootPath + File.separator + "tmpFiles");
			                if (!dir.exists())dir.mkdirs();
			                
			                // Create the file on server
			                File serverFile = new File(dir.getAbsolutePath() + File.separator + "excelFile");
			                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			                stream.write(bytes);
			                stream.close();
					 }
					 catch(Exception ex)
					 {
						 System.out.println(ex);
					 }
				}
				
		 
				//ArrayList<AC_Customer> lastAC_CustomersArrayList = dataManager.readExcel(dir.getAbsolutePath() + File.separator + "excelFile",file.getOriginalFilename(),serviceType); 
				String[] ExcelViewIdArray = dataManager.readExcel(dir.getAbsolutePath() + File.separator + "excelFile",file.getOriginalFilename(),serviceType); 
				
				session.setAttribute("ExcelViewIdArray", ExcelViewIdArray);
				session.setAttribute("fileName",file.getOriginalFilename());
				session.setAttribute("serviceType",serviceType);
				session.setAttribute("serviceTypeArray",serviceTypeArray);
				
				//ModelAndView modelAndView = new ModelAndView("insertExcel");
				//modelAndView.addObject("showData",lastAC_CustomersArrayList);
				//modelAndView.addObject("fileName",file.getOriginalFilename());
				//modelAndView.addObject("serviceType",serviceType);
				//modelAndView.addObject("serviceTypeArray",serviceTypeArray);
				//return modelAndView;
			}
		}
		else
		{
			String[] serviceTypeArray = new String[2];
			if(serviceType.equals("amc"))
			{
				serviceTypeArray[0]="checked";
				serviceTypeArray[1]="";
			}
			if(serviceType.equals("free"))
			{
				serviceTypeArray[0]="";
				serviceTypeArray[1]="checked";
			}
			
			File dir=null;
			if (!file.isEmpty())
			{ 
				System.out.println("!file.isEmpty"); 
				try
				{
	
					 byte[] bytes = file.getBytes();
					 
		                // Creating the directory to store file
		                String rootPath = System.getProperty("catalina.home");
		                dir = new File(rootPath + File.separator + "tmpFiles");
		                if (!dir.exists())dir.mkdirs();
		                
		                // Create the file on server
		                File serverFile = new File(dir.getAbsolutePath() + File.separator + "excelFile");
		                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
		                stream.write(bytes);
		                stream.close();
				 }
				 catch(Exception ex)
				 {
					 System.out.println(ex);
				 }
			}
			
	 
			//ArrayList<AC_Customer> lastAC_CustomersArrayList = dataManager.readExcel(dir.getAbsolutePath() + File.separator + "excelFile",file.getOriginalFilename(),serviceType); 
			String[] ExcelViewIdArray = dataManager.readExcel(dir.getAbsolutePath() + File.separator + "excelFile",file.getOriginalFilename(),serviceType); 
			
			session.setAttribute("ExcelViewIdArray", ExcelViewIdArray);
			session.setAttribute("fileName",file.getOriginalFilename());
			session.setAttribute("serviceType",serviceType);
			session.setAttribute("serviceTypeArray",serviceTypeArray);
			
			//ModelAndView modelAndView = new ModelAndView("insertExcel");
			//modelAndView.addObject("showData",lastAC_CustomersArrayList);
			//modelAndView.addObject("fileName",file.getOriginalFilename());
			//modelAndView.addObject("serviceType",serviceType);
			//modelAndView.addObject("serviceTypeArray",serviceTypeArray);
			//return modelAndView;
		}
		return "importExcelRedirector";
	}
	/*@RequestMapping(value = "/genarateExcel" , method = RequestMethod. GET)
	public ModelAndView genarateExcel() 
	{
    	ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.takeAllAC_CustomerDetails());
		dataManager.createExcelFile();

 
		return modelAndView;
	}*/
    private static final int BUFFER_SIZE = 4096;
	@RequestMapping(value = "/downloadExcel" , method = RequestMethod. GET)
	public void downloadExcel(HttpServletRequest request,HttpServletResponse response) 
	{		
		//ServletContext context = request.getServletContext();
		
		dataManager.createExcelFile();
		try
		{
	
			// construct the complete absolute path of the file
	        String fullPath = "C:\\Windows\\Temp\\Demo\\customer_details.xls";      
	        File downloadFile = new File(fullPath);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	        
	        // get MIME type of the file
	        /*String mimeType = context.getMimeType(fullPath);//http://www.aibn.com/help/Learn/mimetypes.html,http://www.freeformatter.com/mime-types-list.html
	        if (mimeType == null) 
	        {*/
	        // set to binary type if MIME mapping not found
	        String mimeType = "application/octet-stream";
	        /*}*/
	        
	        // set content attributes for the response
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	        
	        // set headers for the response
	        String headerKey = "Content-Disposition";//https://support.microsoft.com/en-us/kb/260519
	        String headerValue = String.format("attachment; filename=\"%s\"",downloadFile.getName());//http://www.freeformatter.com/mime-types-list.html
	        response.setHeader(headerKey, headerValue);
	        
	        // get output stream of the response
	        OutputStream outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	        
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
		}
		catch(Exception ex)
		{
			 System.out.println(ex);
		}
	}
	@RequestMapping(value = "/pdfGenerate", method = RequestMethod. POST)
	//public ModelAndView pdfGenerate(@RequestParam(value="checkedList") String[] checkedArray,@RequestParam(value="filterInfo") Integer[] filterInfoArray) //@RequestParam List<String> list
	public ModelAndView pdfGenerateByDateRange(@RequestParam(value="checkedList") String[] checkedArray,@RequestParam(value="filterInfo") String[] filterInfo)
	{
		dataManager.pdfGenarateService(checkedArray,filterInfo);
		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.takeAllAC_CustomerDetails());
 
		return modelAndView;
	}
	@RequestMapping(value = "/downloadPDF" , method = RequestMethod. GET)
	public void downloadPDF(HttpServletRequest request,HttpServletResponse response) 
	{	
		try
		{
			// construct the complete absolute path of the file
	        String fullPath = "C:\\Windows\\Temp\\Demo\\Document.pdf";      
	        File downloadFile = new File(fullPath);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	        
	        // get MIME type of the file
	        /*String mimeType = context.getMimeType(fullPath);//http://www.aibn.com/help/Learn/mimetypes.html,http://www.freeformatter.com/mime-types-list.html
	        if (mimeType == null) 
	        {*/
	        // set to binary type if MIME mapping not found
	        String mimeType = "application/octet-stream";
	        /*}*/
	        
	        // set content attributes for the response
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	        
	        // set headers for the response
	        String headerKey = "Content-Disposition";//https://support.microsoft.com/en-us/kb/260519
	        String headerValue = String.format("attachment; filename=\"%s\"",downloadFile.getName());//http://www.freeformatter.com/mime-types-list.html
	        response.setHeader(headerKey, headerValue);
	        
	        // get output stream of the response
	        OutputStream outStream = response.getOutputStream();
	        
	        byte[] buffer = new byte[BUFFER_SIZE];
	        int bytesRead = -1;
	        
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
		}
		catch(Exception ex)
		{
			 System.out.println(ex);
		}
	}
	@RequestMapping(value = "/filter", method = RequestMethod.POST)
	public ModelAndView filter(@RequestParam("from")String fromDate,@RequestParam("to")String toDate,@RequestParam("filtertype")String filtertype,@RequestParam("amcOrFree")String amcOrFree) 
	{
		//System.out.println("customer_pk-"+serviceTurn);
		/*
		String[] serviceSelected = new String[3];
		for(int x=0;x<3;x++)serviceSelected[x]="";
		serviceSelected [Integer.parseInt(serviceTurn)] = "selected";
		
		String[] timeSelected = new String[4];
		for(int x=0;x<4;x++)timeSelected[x]="";
		timeSelected [Integer.parseInt(time)] = "selected";
		*/
		
		String[] selectedFilterType = new String[3];
		for(int x=0;x<3;x++)selectedFilterType[x]="";
		selectedFilterType [Integer.parseInt(filtertype)] = "selected";
		
		String[] selectedServiceType = new String[3];
		for(int x=0;x<3;x++)selectedServiceType[x]="";
		selectedServiceType [Integer.parseInt(amcOrFree)] = "selected";
		
		//System.out.println(fromDate+toDate);
		ModelAndView modelAndView = new ModelAndView("filteredPage");
		ArrayList<AC_Customer> resultList=new ArrayList<AC_Customer>();
		
		if(Integer.parseInt(filtertype) == 0)//0 = all
		{
			if(Integer.parseInt(amcOrFree) == 0)
			{
				resultList = dataManager.filterAllService(fromDate,toDate);
				modelAndView.addObject("showData",resultList);
			}
			else
			{
				resultList = dataManager.filterAllNotAllService(fromDate,toDate,amcOrFree);
				modelAndView.addObject("showData",resultList);
			}
		}
		else if(Integer.parseInt(filtertype) == 1 )//1 = notprinted
		{
			if(Integer.parseInt(amcOrFree) == 0)
			{	
				resultList = dataManager.filterNotPrintedService(fromDate,toDate);
				modelAndView.addObject("showData",resultList);
			}
			else
			{
				resultList = dataManager.filterNotPrintedNotAllService(fromDate,toDate,amcOrFree);
				modelAndView.addObject("showData",resultList);
			}
		}
		else if(Integer.parseInt(filtertype) == 2)//2 = notdone
		{
			if(Integer.parseInt(amcOrFree) == 0)
			{
				resultList = dataManager.filterNotDoneServices(fromDate,toDate);
				modelAndView.addObject("showData",resultList);
			}
			else
			{
				resultList = dataManager.filterNotDoneNotAllServices(fromDate,toDate,amcOrFree);
				modelAndView.addObject("showData",resultList);
			}
		}
        int totalservices = resultList.size();
		modelAndView.addObject("fromDate",fromDate);
		modelAndView.addObject("toDate",toDate);
		modelAndView.addObject("selectedFilterType",selectedFilterType);
		modelAndView.addObject("selectedServiceType",selectedServiceType);
		modelAndView.addObject("totalservices",totalservices);
		return modelAndView;
	}
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView search(@RequestParam("searchcolumn")String searchColumn,@RequestParam("searchkeywords")String searchKeyWords) 
	{
		//System.out.println("customer_pk-"+serviceTurn);
		/*
		String[] serviceSelected = new String[3];
		for(int x=0;x<3;x++)serviceSelected[x]="";
		serviceSelected [Integer.parseInt(serviceTurn)] = "selected";
		
		String[] timeSelected = new String[4];
		for(int x=0;x<4;x++)timeSelected[x]="";
		timeSelected [Integer.parseInt(time)] = "selected";
		*/
		
		String[] selectedColumn = new String[24];
		for(int x=0;x<24;x++)selectedColumn[x]="";
		selectedColumn [Integer.parseInt(searchColumn)] = "selected";
		
		ModelAndView modelAndView = new ModelAndView("search");
		ArrayList<AC_Customer> resultList=new ArrayList<AC_Customer>();
		resultList = dataManager.search(searchColumn,searchKeyWords);
		modelAndView.addObject("showData",resultList);
		modelAndView.addObject("searchKeyWords",searchKeyWords);
		modelAndView.addObject("selectedColumn",selectedColumn);
		int totalservices = resultList.size();
		modelAndView.addObject("totalservices",totalservices);
		//System.out.println("before return controller page");
		return modelAndView;
	}
	@RequestMapping(value = "/insertExcel")
	public ModelAndView insertExcelpage(HttpSession session) 
	{
		String[] serviceTypeArray = new String[2];
		serviceTypeArray[0]="checked";
		serviceTypeArray[1]="";
		
		ArrayList<Long> FormIdList= (ArrayList<Long>) session.getAttribute("insertExcelFormIds");
		if(FormIdList==null)
		{
			FormIdList =new ArrayList<Long>();
		}
		
		Random rand = new Random();
		Long insertExcelFormId = rand.nextLong();
		FormIdList.add(insertExcelFormId);
		session.setAttribute("insertExcelFormIds",FormIdList);
		
		ModelAndView modelAndView = new ModelAndView("insertExcel");
		modelAndView.addObject("serviceTypeArray",serviceTypeArray);
		modelAndView.addObject("insertExcelFormId",insertExcelFormId);
		return modelAndView;

	}
	@RequestMapping(value = "/batchDelete", method = RequestMethod. POST)
	//public ModelAndView pdfGenerate(@RequestParam(value="checkedList") String[] checkedArray,@RequestParam(value="filterInfo") Integer[] filterInfoArray) //@RequestParam List<String> list
	public ModelAndView batchDelete(@RequestParam(value="checkedList") String[] checkedArray)
	{
		//dataManager.pdfGenarateService(checkedArray,filterInfo);
		//System.out.println("batch delete");
		dataManager.batchDeleteService(checkedArray);

		String[] serviceTypeArray = new String[2];
		serviceTypeArray[0]="checked";
		serviceTypeArray[1]="";
		ModelAndView modelAndView = new ModelAndView("insertExcel");
		modelAndView.addObject("serviceTypeArray",serviceTypeArray);
		return modelAndView;
	}
	@RequestMapping(value = "/insertExcelView")
	public ModelAndView insertExcelView(HttpSession session)
	{
		Integer redirected = 1;
		session.setAttribute("redirected",redirected);
		
		ArrayList<Long> FormIdList= (ArrayList<Long>) session.getAttribute("insertExcelFormIds");
		if(FormIdList==null)
		{
			FormIdList =new ArrayList<Long>();
		}
		
		Random rand = new Random();
		Long insertExcelFormId = rand.nextLong();
		FormIdList.add(insertExcelFormId);
		session.setAttribute("insertExcelFormIds",FormIdList);
		
		String[] idArray = (String[]) session.getAttribute("ExcelViewIdArray");
		
		ModelAndView modelAndView = new ModelAndView("insertExcel");
		modelAndView.addObject("showData",dataManager.getCustomerListByArrayOfPKs(idArray));
		modelAndView.addObject("fileName",session.getAttribute("fileName"));
		modelAndView.addObject("serviceType",session.getAttribute("serviceType"));
		modelAndView.addObject("serviceTypeArray",session.getAttribute("serviceTypeArray"));
		modelAndView.addObject("insertExcelFormId",insertExcelFormId);
		return modelAndView;
	}
	@RequestMapping(value = "/pdfGenerateByService", method = RequestMethod. POST)
	//public ModelAndView pdfGenerate(@RequestParam(value="checkedList") String[] checkedArray,@RequestParam(value="filterInfo") Integer[] filterInfoArray) //@RequestParam List<String> list
	public ModelAndView pdfGenerateByServiceNo(@RequestParam(value="checkedList") String[] checkedArray,@RequestParam(value="serviceNo") String[] serviceNo)
	{
		dataManager.pdfGenarateByService(checkedArray,serviceNo);
		
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.newlyEnteredCustomers());
 
		return modelAndView;
	}
	@RequestMapping(value = "/createUserPage")
	public String createUserPage()
	{
		return "createUser";
	}
	@RequestMapping(value = "/createUser")
	public ModelAndView createUser(@RequestParam("username")String userName,@RequestParam("password")String passWord,@RequestParam("userType")String userType)
	{
		boolean status = userManager.createUserService(userName,passWord,userType);
		if(status)
		{
			ModelAndView modelAndView = new ModelAndView("allUsers");
			modelAndView.addObject("allUsers",userManager.getAllCustomers());
			return modelAndView;
		}
		else
		{
			ModelAndView modelAndView = new ModelAndView("createUser");
			modelAndView.addObject("userName",userName);
			modelAndView.addObject("passWord",passWord);
			modelAndView.addObject("userTypetoSelectedArray",userManager.createUserTypetoSelectedArray1(userType));
			modelAndView.addObject("errorMessage","Try with differnt username and password");
			return modelAndView;
		}

	}
	@RequestMapping(value = "/AllUsers")
	public ModelAndView AllUsers()
	{
		ModelAndView modelAndView = new ModelAndView("allUsers");
		modelAndView.addObject("allUsers",userManager.getAllCustomers());
		return modelAndView;
	}
	@RequestMapping(value = "/deleteUser")
	public ModelAndView deleteUser(@RequestParam("user_id")String user_id)
	{
		userManager.removeUser(user_id);
		
		ModelAndView modelAndView = new ModelAndView("allUsers");
		modelAndView.addObject("allUsers",userManager.getAllCustomers());
		return modelAndView;
	}
	@RequestMapping(value = "/editUserPage" , method = RequestMethod. GET)
	public ModelAndView editUserRollpage(@RequestParam("user_id")String user_id) 
	{
	    //System.out.println("customer_pk-"+customer_pk);
	    //ArrayList<Customer> customerList=dataManager.customerDetailsFrom_customer_pk(customer_pk);
	    ModelAndView modelAndView = new ModelAndView("editUser");
	    modelAndView.addObject("user_id",user_id);
	    modelAndView.addObject("userToEdit",userManager.userDetailsFromUser_id(user_id));
	    modelAndView.addObject("userTypetoSelectedArray",userManager.createUserTypetoSelectedArray2(user_id));
	    return modelAndView;
	}
	@RequestMapping(value = "/editUser" , method = RequestMethod. GET)
	public ModelAndView editUserRoll(@RequestParam("user_id")String userId,@RequestParam("username")String username,@RequestParam("userType")String userType) 
	{
		userManager.changeUser(userId,username,userType);
		
		
		ModelAndView modelAndView = new ModelAndView("allUsers");
		modelAndView.addObject("allUsers",userManager.getAllCustomers());
		return modelAndView;
	}
	@RequestMapping(value = "/changePasswordPage" , method = RequestMethod. GET)
	public ModelAndView changePasswordPage(@RequestParam("user_id")String user_id) 
	{
	    //System.out.println("customer_pk-"+customer_pk);
	    //ArrayList<Customer> customerList=dataManager.customerDetailsFrom_customer_pk(customer_pk);
	    ModelAndView modelAndView = new ModelAndView("changePassword");
	    modelAndView.addObject("user_id",user_id);
	    modelAndView.addObject("userToEdit",userManager.userDetailsFromUser_id(user_id));
	    modelAndView.addObject("userTypetoSelectedArray",userManager.createUserTypetoSelectedArray2(user_id));
	    return modelAndView;
	}
	@RequestMapping(value = "/changepassword" , method = RequestMethod. GET)
	public ModelAndView changePassword(@RequestParam("user_id")String userId,@RequestParam("password")String password) 
	{
		userManager.changePassword(userId,password);
		
		ModelAndView modelAndView = new ModelAndView("allUsers");
		modelAndView.addObject("allUsers",userManager.getAllCustomers());
		return modelAndView;
	}
	@RequestMapping(value = "/customerDataPage")
	public ModelAndView customerDataPage(@RequestParam("pageNumber")String pageNumber)
	{
		ModelAndView modelAndView = new ModelAndView("showData");
		modelAndView.addObject("showData",dataManager.getPageData(pageNumber));
		modelAndView.addObject("numberofPages",dataManager.calculateNumberOfPages());
		modelAndView.addObject("currentPage",pageNumber);
		return modelAndView;
	}
	@RequestMapping(value = "/filterbyinstdate", method = RequestMethod.POST)
	public ModelAndView filterByInstDate(@RequestParam("instFrom")String fromDate,@RequestParam("instTo")String toDate,@RequestParam("amcOrFree")String amcOrFree) 
	{
		String[] selectedServiceType = new String[3];
		for(int x=0;x<3;x++)selectedServiceType[x]="";
		selectedServiceType [Integer.parseInt(amcOrFree)] = "selected";
		
		//System.out.println(fromDate+toDate);
		ModelAndView modelAndView = new ModelAndView("instFilteredPage");
		ArrayList<AC_Customer> resultList=new ArrayList<AC_Customer>();
		
		resultList = dataManager.filterByInstDate(fromDate,toDate,amcOrFree);
		modelAndView.addObject("showData",resultList);

        int totalservices = resultList.size();
		modelAndView.addObject("fromDate",fromDate);
		modelAndView.addObject("toDate",toDate);
		modelAndView.addObject("selectedServiceType",selectedServiceType);
		modelAndView.addObject("totalservices",totalservices);
		return modelAndView;
	}
	@RequestMapping(value = "/csrfTokenExpiredMessage")
	public String csrfTokenExpiredMessage() 
	{
		return "csrfTokenExpiredMessage";
	}
}
