package com.softlogic.daos;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import org.springframework.stereotype.Repository;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import com.softlogic.controllers.HelloWorldController;
import com.softlogic.models.AC_Customer;
import com.softlogic.models.PrintModel;

import org.apache.log4j.Logger;

@Repository
public class PDFGenerator implements IPDFGenerator
{
	static Logger log = Logger.getLogger(HelloWorldController.class.getName());
	/*public boolean AllServicePDFGenerator(ArrayList<AC_Customer> acCustomerfirstServiceList,ArrayList<AC_Customer> acCustomerSecondServiceList,ArrayList<AC_Customer> acCustomerThiredServiceList)
	{
		try 
		{
			File file = new File("c:\\Windows\\temp\\Demo\\Document.pdf");
        	file.delete();
		} 
		catch (Exception x) 
		{
			System.out.println(x);
		}
		if(0 == acCustomerfirstServiceList.size())return false;
		System.out.println("class:PDFGenerator,method:firstServicePDFGenerator");
		Document document = new Document(PageSize.A4, 50, 50, 70, 70);
		//document.setMargins(50, 50, 70, 70);
		try 
		{
	        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Windows\\Temp\\Demo\\Document.pdf"));
			document.open();
			//---------------------------Heding-----------------------------//
			try
			{
				Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
				document.add(img);
			}
			catch(Exception ex)
			{
				System.out.println("class:PDFGenerator,method:firstServicePDFGenerator"+ex);
				return false;
			}
			document.add(getHeding("                      "));
			document.add(getHeding("Reminder for first free service"));
			document.add( Chunk.NEWLINE );
			document.add( Chunk.NEWLINE );
			
			//--------------------------address-------------------------------//
			String[] addressArray = acCustomerfirstServiceList.get(0).getCustomerAdress().split(",");
			Paragraph paragraph = new Paragraph();
			
			paragraph.add(acCustomerfirstServiceList.get(0).getCustomerName());
			paragraph.add(",");
			paragraph.add(Chunk.NEWLINE);
			
			for(int j=0;j<addressArray.length;j++)
			{
				paragraph.add(addressArray[j]);
				if(j != (addressArray.length-1))
				{
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					//paragraph.add("\n"); 
				}
			}
			paragraph.add(".");
			document.add(paragraph);
			
			document.add( Chunk.NEWLINE );
			paragraph.clear();
			
			//paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add("Dear Sir,");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We wish to inform you that your yearly 01st service is due on ");
			paragraph.add(acCustomerfirstServiceList.get(0).getFirstFree());
			paragraph.add(" . ");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("Thanking You, \nSoftlogic Service Management");
			document.add(paragraph);
			paragraph.clear();
			if(1 < acCustomerfirstServiceList.size())
			{
				for(int i=1;i<acCustomerfirstServiceList.size();i++)
				{
			        writer.setPageEmpty(false);
			        document.newPage();
					try
					{
						Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
						document.add(img);
					}
					catch(Exception ex)
					{
						System.out.println("class:PDFGenerator,method:firstServicePDFGenerator" + ex);
						return false;
					}
					document.add(getHeding("                      "));
			        document.add(getHeding("Reminder for first free service"));
					document.add( Chunk.NEWLINE );
					document.add( Chunk.NEWLINE );
					
					paragraph.add(acCustomerfirstServiceList.get(i).getCustomerName());
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					
					addressArray = acCustomerfirstServiceList.get(i).getCustomerAdress().split(",");
					for(int j=0;j<addressArray.length;j++)
					{
						paragraph.add(addressArray[j]);
						if(j != (addressArray.length-1))
						{
							paragraph.add(",");
							paragraph.add(Chunk.NEWLINE);
							//paragraph.add("\n");
							//para.add(new Chunk("  A")); 
						}
					}
					paragraph.add(".");
					document.add(paragraph);
					
					document.add( Chunk.NEWLINE );
					paragraph.clear();
					
					//paragraph.add(acCustomerList.get(i).getCustomerName());
					paragraph.add("Dear Sir,");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We wish to inform you that your yearly 01st service is due on ");
					paragraph.add(acCustomerfirstServiceList.get(i).getFirstFree());
					paragraph.add(" . ");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("Thanking You, \nSoftlogic Service Management");
					document.add(paragraph);
					paragraph.clear();
				}
			}
			if(0 < acCustomerSecondServiceList.size())
			{
				for(int i=0;i<acCustomerSecondServiceList.size();i++)
				{
			        writer.setPageEmpty(false);
			        document.newPage();
					try
					{
						Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
						document.add(img);
					}
					catch(Exception ex)
					{
						System.out.println("class:PDFGenerator,method:AllServicePDFGenerator" + ex);
						return false;
					}
					document.add(getHeding("                      "));
			        document.add(getHeding("Reminder for second free service"));
					document.add( Chunk.NEWLINE );
					document.add( Chunk.NEWLINE );
					
					paragraph.add(acCustomerSecondServiceList.get(i).getCustomerName());
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					addressArray = acCustomerSecondServiceList.get(i).getCustomerAdress().split(",");
					for(int j=0;j<addressArray.length;j++)
					{
						paragraph.add(addressArray[j]);
						if(j != (addressArray.length-1))
						{
							paragraph.add(",");
							paragraph.add(Chunk.NEWLINE);
							//paragraph.add("\n");
							//para.add(new Chunk("  A")); 
						}
					}
					paragraph.add(".");
					document.add(paragraph);
					
					document.add( Chunk.NEWLINE );
					paragraph.clear();
					
					paragraph.add("Dear Sir,");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We wish to inform you that your yearly 02nd service is due on ");
					paragraph.add(acCustomerSecondServiceList.get(i).getThirdFree());
					paragraph.add(" . ");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("Thanking You, \nSoftlogic Service Management");
					document.add(paragraph);
					paragraph.clear();
				}
			}
			if(0 < acCustomerThiredServiceList.size())
			{
				for(int i=0;i<acCustomerThiredServiceList.size();i++)
				{
			        writer.setPageEmpty(false);
			        document.newPage();
					try
					{
						Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
						document.add(img);
					}
					catch(Exception ex)
					{
						System.out.println("class:PDFGenerator,method:AllServicePDFGenerator" + ex);
						return false;
					}
					document.add(getHeding("                      "));
			        document.add(getHeding("Reminder for thired free service"));
					document.add( Chunk.NEWLINE );
					document.add( Chunk.NEWLINE );
					
					paragraph.add(acCustomerThiredServiceList.get(i).getCustomerName());
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					
					addressArray = acCustomerThiredServiceList.get(i).getCustomerAdress().split(",");
					for(int j=0;j<addressArray.length;j++)
					{
						paragraph.add(addressArray[j]);
						if(j != (addressArray.length-1))
						{
							paragraph.add(",");
							paragraph.add(Chunk.NEWLINE);
							//paragraph.add("\n");
							//para.add(new Chunk("  A")); 
						}
					}
					paragraph.add(".");
					document.add(paragraph);
					
					document.add( Chunk.NEWLINE );
					paragraph.clear();
					
					//paragraph.add(acCustomerList.get(i).getCustomerName());
					paragraph.add("Dear Sir,");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We wish to inform you that your yearly 03rd service is due on ");
					paragraph.add(acCustomerThiredServiceList.get(i).getFirstFree());
					paragraph.add(" . ");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("Thanking You, \nSoftlogic Service Management");
					document.add(paragraph);
					paragraph.clear();
				}
			}
			document.close();
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}*/
	public boolean firstServicePDFGenerator(ArrayList<AC_Customer> acCustomerList)
	{
		try 
		{
			File file = new File("c:\\Windows\\temp\\Demo\\Document.pdf");
        	file.delete();
		} 
		catch (Exception ex) 
		{
			System.out.println(ex);
			log.error("class:PDFGenetaror,method:firstServicePDFGenerator,situation:delete existing pdf,Exception:"+ ex);
		}
		if(0 == acCustomerList.size())return false;
		System.out.println("class:PDFGenerator,method:firstServicePDFGenerator");
		Document document = new Document(PageSize.A4, 50, 50, 70, 70);
		//document.setMargins(50, 50, 70, 70);
		try 
		{
	        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Windows\\Temp\\Demo\\Document.pdf"));
			document.open();
			//---------------------------Heding-----------------------------//
			try
			{
				Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
				document.add(img);
			}
			catch(Exception ex)
			{
				System.out.println("class:PDFGenerator,method:firstServicePDFGenerator"+ex);
				log.error("class:PDFGenetaror,method:firstServicePDFGenerator,situation:access logo image,Exception:"+ ex);
				return false;
			}
			document.add(getHeding("                      "));
			document.add(getHeding("Reminder for first free service"));
			document.add( Chunk.NEWLINE );
			document.add( Chunk.NEWLINE );
			
			//--------------------------address-------------------------------//
			String[] addressArray = acCustomerList.get(0).getCustomerAdress().split(",");
			Paragraph paragraph = new Paragraph();
			
			paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add(",");
			paragraph.add(Chunk.NEWLINE);
			
			for(int j=0;j<addressArray.length;j++)
			{
				paragraph.add(addressArray[j]);
				if(j != (addressArray.length-1))
				{
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					//paragraph.add("\n"); 
				}
			}
			paragraph.add(".");
			document.add(paragraph);
			
			document.add( Chunk.NEWLINE );
			paragraph.clear();
			
			//paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add("Dear Sir,");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We wish to inform you that your yearly 01st service is due on ");
			paragraph.add(acCustomerList.get(0).getFirstFree());
			paragraph.add(" . ");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("Thanking You, \nSoftlogic Service Management");
			document.add(paragraph);
			paragraph.clear();
			if(1 < acCustomerList.size())
			{
				for(int i=1;i<acCustomerList.size();i++)
				{
			        writer.setPageEmpty(false);
			        document.newPage();
					try
					{
						Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
						document.add(img);
					}
					catch(Exception ex)
					{
						System.out.println("class:PDFGenerator,method:firstServicePDFGenerator" + ex);
						return false;
					}
					document.add(getHeding("                      "));
			        document.add(getHeding("Reminder for first free service"));
					document.add( Chunk.NEWLINE );
					document.add( Chunk.NEWLINE );
					
					paragraph.add(acCustomerList.get(i).getCustomerName());
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					
					addressArray = acCustomerList.get(i).getCustomerAdress().split(",");
					for(int j=0;j<addressArray.length;j++)
					{
						paragraph.add(addressArray[j]);
						if(j != (addressArray.length-1))
						{
							paragraph.add(",");
							paragraph.add(Chunk.NEWLINE);
							//paragraph.add("\n");
							//para.add(new Chunk("  A")); 
						}
					}
					paragraph.add(".");
					document.add(paragraph);
					
					document.add( Chunk.NEWLINE );
					paragraph.clear();
					
					//paragraph.add(acCustomerList.get(i).getCustomerName());
					paragraph.add("Dear Sir,");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We wish to inform you that your yearly 01st service is due on ");
					paragraph.add(acCustomerList.get(i).getFirstFree());
					paragraph.add(" . ");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("Thanking You, \nSoftlogic Service Management");
					document.add(paragraph);
					paragraph.clear();
				}
			}
			document.close();
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean secondServicePDFGenerator(ArrayList<AC_Customer> acCustomerList)
	{
		try 
		{
			File file = new File("c:\\Windows\\temp\\Demo\\Document.pdf");
        	file.delete();
		} 
		catch (Exception x) 
		{
			System.out.println(x);
		}
		if(0 == acCustomerList.size())return false;
		System.out.println("class:PDFGenerator,method:secondServicePDFGenerator");
		Document document = new Document(PageSize.A4, 50, 50, 70, 70);
		//document.setMargins(50, 50, 70, 70);
		try 
		{
	        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Windows\\Temp\\Demo\\Document.pdf"));
			document.open();
			//---------------------------Heding-----------------------------//
			try
			{
				Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
				document.add(img);
			}
			catch(Exception ex)
			{
				System.out.println("class:PDFGenerator,method:secondServicePDFGenerator"+ex);
				return false;
			}
			document.add(getHeding("                      "));
			document.add(getHeding("Reminder for second free service"));
			document.add( Chunk.NEWLINE );
			document.add( Chunk.NEWLINE );
			
			//--------------------------address-------------------------------//
			String[] addressArray = acCustomerList.get(0).getCustomerAdress().split(",");
			Paragraph paragraph = new Paragraph();
			
			paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add(",");
			paragraph.add(Chunk.NEWLINE);
			for(int j=0;j<addressArray.length;j++)
			{
				paragraph.add(addressArray[j]);
				if(j != (addressArray.length-1))
				{
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					//paragraph.add("\n"); 
				}
			}
			paragraph.add(".");
			document.add(paragraph);
			
			document.add( Chunk.NEWLINE );
			paragraph.clear();
			
			//paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add("Dear Sir,");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We wish to inform you that your yearly 02nd service is due on ");
			paragraph.add(acCustomerList.get(0).getSecondFree());
			paragraph.add(" . ");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("Thanking You, \nSoftlogic Service Management");
			document.add(paragraph);
			paragraph.clear();
			if(1 < acCustomerList.size())
			{
				for(int i=1;i<acCustomerList.size();i++)
				{
			        writer.setPageEmpty(false);
			        document.newPage();
					try
					{
						Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
						document.add(img);
					}
					catch(Exception ex)
					{
						System.out.println("class:PDFGenerator,method:secondServicePDFGenerator" + ex);
						return false;
					}
					document.add(getHeding("                      "));
			        document.add(getHeding("Reminder for second free service"));
					document.add( Chunk.NEWLINE );
					document.add( Chunk.NEWLINE );
					
					paragraph.add(acCustomerList.get(i).getCustomerName());
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					addressArray = acCustomerList.get(i).getCustomerAdress().split(",");
					for(int j=0;j<addressArray.length;j++)
					{
						paragraph.add(addressArray[j]);
						if(j != (addressArray.length-1))
						{
							paragraph.add(",");
							paragraph.add(Chunk.NEWLINE);
							//paragraph.add("\n");
							//para.add(new Chunk("  A")); 
						}
					}
					paragraph.add(".");
					document.add(paragraph);
					
					document.add( Chunk.NEWLINE );
					paragraph.clear();
					
					//paragraph.add(acCustomerList.get(i).getCustomerName());
					paragraph.add("Dear Sir,");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We wish to inform you that your yearly 02nd service is due on ");
					paragraph.add(acCustomerList.get(i).getSecondFree());
					paragraph.add(" . ");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("Thanking You, \nSoftlogic Service Management");
					document.add(paragraph);
					paragraph.clear();
				}
			}

			document.close();
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public boolean thiredServicePDFGenerator(ArrayList<AC_Customer> acCustomerList)
	{
		try 
		{
			File file = new File("c:\\Windows\\temp\\Demo\\Document.pdf");
        	file.delete();
		} 
		catch (Exception x) 
		{
			System.out.println(x);
		}
		if(0 == acCustomerList.size())return false;
		System.out.println("class:PDFGenerator,method:thiredServicePDFGenerator");
		Document document = new Document(PageSize.A4, 50, 50, 70, 70);
		//document.setMargins(50, 50, 70, 70);
		try 
		{
	        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Windows\\Temp\\Demo\\Document.pdf"));
			document.open();
			//---------------------------Heding-----------------------------//
			try
			{
				Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
				document.add(img);
			}
			catch(Exception ex)
			{
				System.out.println("class:PDFGenerator,method:thiredServicePDFGenerator"+ex);
				return false;
			}
			document.add(getHeding("                      "));
			document.add(getHeding("Reminder for thired free service"));
			document.add( Chunk.NEWLINE );
			document.add( Chunk.NEWLINE );
			
			//--------------------------address-------------------------------//
			String[] addressArray = acCustomerList.get(0).getCustomerAdress().split(",");
			Paragraph paragraph = new Paragraph();
			
			paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add(",");
			paragraph.add(Chunk.NEWLINE);
			for(int j=0;j<addressArray.length;j++)
			{
				paragraph.add(addressArray[j]);
				if(j != (addressArray.length-1))
				{
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					//paragraph.add("\n"); 
				}
			}
			paragraph.add(".");
			document.add(paragraph);
			
			document.add( Chunk.NEWLINE );
			paragraph.clear();
			
			//paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add("Dear Sir,");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We wish to inform you that your yearly 03rd service is due on ");
			paragraph.add(acCustomerList.get(0).getThirdFree());
			paragraph.add(" . ");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
			document.add(paragraph);
			paragraph.clear();
			
			document.add( Chunk.NEWLINE );
			paragraph.add("Thanking You, \nSoftlogic Service Management");
			document.add(paragraph);
			paragraph.clear();
			if(1 < acCustomerList.size())
			{
				for(int i=1;i<acCustomerList.size();i++)
				{
			        writer.setPageEmpty(false);
			        document.newPage();
					try
					{
						Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
						document.add(img);
					}
					catch(Exception ex)
					{
						System.out.println("class:PDFGenerator,method:thiredServicePDFGenerator" + ex);
						return false;
					}
					document.add(getHeding("                      "));
			        document.add(getHeding("Reminder for thired free service"));
					document.add( Chunk.NEWLINE );
					document.add( Chunk.NEWLINE );
					
					paragraph.add(acCustomerList.get(i).getCustomerName());
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					addressArray = acCustomerList.get(i).getCustomerAdress().split(",");
					for(int j=0;j<addressArray.length;j++)
					{
						paragraph.add(addressArray[j]);
						if(j != (addressArray.length-1))
						{
							paragraph.add(",");
							paragraph.add(Chunk.NEWLINE);
							//paragraph.add("\n");
							//para.add(new Chunk("  A")); 
						}
					}
					paragraph.add(".");
					document.add(paragraph);
					
					document.add( Chunk.NEWLINE );
					paragraph.clear();
					
					paragraph.add("Dear Sir,");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We wish to inform you that your yearly 03rd service is due on ");
					paragraph.add(acCustomerList.get(i).getThirdFree());
					paragraph.add(" . ");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
					document.add(paragraph);
					paragraph.clear();
					
					document.add( Chunk.NEWLINE );
					paragraph.add("Thanking You, \nSoftlogic Service Management");
					document.add(paragraph);
					paragraph.clear();
				}
			}
			document.close();
		} 
		catch(Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	//Document document = new Document();
	public boolean AllServicePDFGenerator(ArrayList<AC_Customer> acCustomerfirstServiceList,ArrayList<AC_Customer> acCustomerSecondServiceList,ArrayList<AC_Customer> acCustomerThiredServiceList,ArrayList<AC_Customer> acCustomerfirstAMCServiceList,ArrayList<AC_Customer> acCustomerSecondAMCServiceList,ArrayList<AC_Customer> acCustomerThiredAMCServiceList,ArrayList<AC_Customer> acCustomerforthServiceList,ArrayList<AC_Customer> acCustomerfifthServiceList,ArrayList<AC_Customer> acCustomersixthServiceList)
	{        	
		try 
		{
			File file = new File("c:\\Windows\\temp\\Demo\\Document.pdf");
        	file.delete();
		} 
		catch (Exception x) 
		{
			System.out.println(x);
		}

		//if(0 == acCustomerfirstServiceList.size())return false;
		//System.out.println("class:PDFGenerator,method:firstServicePDFGenerator");
		Document document = new Document(PageSize.A4, 50, 50, 70, 70);
		//document.setMargins(50, 50, 70, 70);
		try 
		{
	        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Windows\\Temp\\Demo\\Document.pdf"));
			document.open();
			for(int i=0;i < acCustomerfirstServiceList.size() ;i++)
			{
	        	this.printPage(1,"Reminder for first free service","1st",acCustomerfirstServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
	        }
			//System.out.println("acCustomerforthServiceList.size"+acCustomerforthServiceList.size());
			//System.out.println("awa1");
	        for(int i=0;i < acCustomerSecondServiceList.size() ;i++)
	        {
	        	this.printPage(2,"Reminder for second free service","2nd",acCustomerSecondServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
			}
	        for(int i=0;i < acCustomerThiredServiceList.size() ;i++)
	        {
	        	this.printPage(3,"Reminder for third free service","3rd",acCustomerThiredServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
			}
	        //-------------------------AMC---------------------------------
	       // System.out.println("acCustomerforthServiceList.size"+acCustomerforthServiceList.size());
	        for(int i=0;i < acCustomerfirstAMCServiceList.size() ;i++)
			{//System.out.println("awa1.5");
	        	this.printPage(1,"Reminder for first AMC service","1st",acCustomerfirstAMCServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
	        }
	        //System.out.println("awa2");
	        for(int i=0;i < acCustomerSecondAMCServiceList.size() ;i++)
	        {
	        	this.printPage(2,"Reminder for second AMC service","2nd",acCustomerSecondAMCServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
			}
	        for(int i=0;i < acCustomerThiredAMCServiceList.size() ;i++)
	        {
	        	this.printPage(3,"Reminder for third AMC service","3rd",acCustomerThiredAMCServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
			}

	        for(int i=0;i < acCustomerforthServiceList.size() ;i++)
	        {
	        	//System.out.println("awa");
	        	this.printPage(4,"Reminder for forth AMC service","4th",acCustomerforthServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
			}
	        for(int i=0;i < acCustomerfifthServiceList.size() ;i++)
	        {
	        	this.printPage(5,"Reminder for fifth AMC service","5th",acCustomerfifthServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
			}
	        for(int i=0;i < acCustomersixthServiceList.size() ;i++)
	        {
	        	this.printPage(6,"Reminder for sixth AMC service","6th",acCustomersixthServiceList.get(i),document);
				writer.setPageEmpty(false);
				document.newPage();
			}
	        if((acCustomerfirstServiceList.size()==0)&&(acCustomerSecondServiceList.size()==0)&&(acCustomerThiredServiceList.size()==0)&&(acCustomerfirstAMCServiceList.size()==0)&&(acCustomerSecondAMCServiceList.size()==0)&&(acCustomerThiredAMCServiceList.size()==0)&&(acCustomerforthServiceList.size()==0)&&(acCustomerfifthServiceList.size()==0)&&(acCustomersixthServiceList.size()==0))
	        {
	        	document.add(getHeding("                      "));
	        }

			document.close();
		}
		catch(Exception ex)
		{
			System.out.println("AllServicePDFGenerator"+ex);
			return false;
		}
		return true;
	}
	public boolean AllServicePDFGeneratorDBOder(ArrayList<PrintModel> printModelList)
	{
		try 
		{
			File file = new File("c:\\Windows\\temp\\Demo\\Document.pdf");
        	file.delete();
		} 
		catch (Exception x) 
		{
			System.out.println(x);
		}

		//if(0 == acCustomerfirstServiceList.size())return false;
		//System.out.println("class:PDFGenerator,method:firstServicePDFGenerator");
		Document document = new Document(PageSize.A4, 50, 50, 70, 70);
		//document.setMargins(50, 50, 70, 70);
		try 
		{
	        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Windows\\Temp\\Demo\\Document.pdf"));
			document.open();
			for(int i=0;i < printModelList.size() ;i++)
			{
	        	if(printModelList.get(i).getServiceNumber()==1)
	        	{
	        		if(printModelList.get(i).getAc_Customer().getAmc()==1)
	        		{
	        			this.printPage(1,"Reminder for first AMC service","1st",printModelList.get(i).getAc_Customer(),document);
	        		}
	        		else
	        		{
	        			this.printPage(1,"Reminder for first free service","1st",printModelList.get(i).getAc_Customer(),document);
	        		}
	        	}
	        	else if(printModelList.get(i).getServiceNumber()==2)
	        	{
	        		if(printModelList.get(i).getAc_Customer().getAmc()==1)
	        		{
	        			this.printPage(2,"Reminder for second AMC service","2nd",printModelList.get(i).getAc_Customer(),document);
	        		}
	        		else
	        		{
	        			this.printPage(2,"Reminder for second free service","2nd",printModelList.get(i).getAc_Customer(),document);
	        		}
	        	}
	        	else if(printModelList.get(i).getServiceNumber()==3)
	        	{
	        		if(printModelList.get(i).getAc_Customer().getAmc()==1)
	        		{
	        			this.printPage(3,"Reminder for third AMC service","3rd",printModelList.get(i).getAc_Customer(),document);
	        		}
	        		else
	        		{
	        			this.printPage(3,"Reminder for third free service","3rd",printModelList.get(i).getAc_Customer(),document);
	        		}
	        	}
	        	else if(printModelList.get(i).getServiceNumber()==4)
	        	{
	        		if(printModelList.get(i).getAc_Customer().getAmc()==1)
	        		{
	        			this.printPage(4,"Reminder for forth AMC service","4th",printModelList.get(i).getAc_Customer(),document);
	        		}
	        		else
	        		{
	        			this.printPage(4,"Reminder for forth free service","4th",printModelList.get(i).getAc_Customer(),document);
	        		}
	        	}
	        	else if(printModelList.get(i).getServiceNumber()==5)
	        	{
	        		if(printModelList.get(i).getAc_Customer().getAmc()==1)
	        		{
	        			this.printPage(5,"Reminder for fifth AMC service","5th",printModelList.get(i).getAc_Customer(),document);
	        		}
	        		else
	        		{
	        			this.printPage(5,"Reminder for fifth free service","5th",printModelList.get(i).getAc_Customer(),document);
	        		}
	        	}
	        	else if(printModelList.get(i).getServiceNumber()==6)
	        	{
	        		if(printModelList.get(i).getAc_Customer().getAmc()==1)
	        		{
	        			this.printPage(6,"Reminder for sixth AMC service","6th",printModelList.get(i).getAc_Customer(),document);
	        		}
	        		else
	        		{
	        			this.printPage(6,"Reminder for sixth free service","6th",printModelList.get(i).getAc_Customer(),document);
	        		}
	        	}
				writer.setPageEmpty(false);
				document.newPage();
	        }
			//System.out.println("acCustomerforthServiceList.size"+acCustomerforthServiceList.size());
			//System.out.println("awa1");
	        if(printModelList.size()==0)
	        {
	        	document.add(getHeding("                      "));
	        }

			document.close();
		}
		catch(Exception ex)
		{
			System.out.println("AllServicePDFGenerator"+ex);
			return false;
		}
		return true;
	}
	private Paragraph getHeding(String headingString)
	{
		Font font1 = new Font(Font.FontFamily.HELVETICA, 25, Font.BOLD);
		Paragraph HeadinPara = new Paragraph();
	    HeadinPara.setAlignment(Element.ALIGN_CENTER);
	    HeadinPara.setFont(font1);
	    HeadinPara.add(headingString);
	    return HeadinPara;
	}

	private boolean printPage(int serviceNumber,String headerString,String serviceType,AC_Customer acCustomer,Document document)
	{
		//---------------------------Heding-----------------------------//
		try
		{
			Image img = Image.getInstance("C:\\Windows\\Temp\\Demo\\imgres.jpg");
			document.add(img);
		}
		catch(Exception ex)
		{
			System.out.println("class:PDFGenerator,method:printPage"+ex);
			log.error("class:PDFGenerator,method:printPage,situation:accessLogoImage,Exception:"+ex);
			return false;
		}
		try
		{
			document.add(getHeding("                      "));
			document.add(getHeding(headerString));
			document.add( Chunk.NEWLINE );
			document.add( Chunk.NEWLINE );
				
			//--------------------------address-------------------------------//
			String[] addressArray = acCustomer.getCustomerAdress().split(",");
			Paragraph paragraph = new Paragraph();
				
			paragraph.add(acCustomer.getCustomerName());
			paragraph.add(",");
			paragraph.add(Chunk.NEWLINE);
				
			for(int j=0;j<addressArray.length;j++)
			{
				paragraph.add(addressArray[j]);
				if(j != (addressArray.length-1))
				{
					paragraph.add(",");
					paragraph.add(Chunk.NEWLINE);
					//paragraph.add("\n"); 
				}
			}
			paragraph.add(".");
			document.add(paragraph);
				
			document.add( Chunk.NEWLINE );
			paragraph.clear();
				
			//paragraph.add(acCustomerList.get(0).getCustomerName());
			paragraph.add("Dear Sir,");
			document.add(paragraph);
			paragraph.clear();
				
			document.add( Chunk.NEWLINE );
			paragraph.add("We wish to inform you that your yearly "+serviceType+" service is due on ");
			if(serviceNumber==1)
			{
				paragraph.add(acCustomer.getFirstFree());
			}
			else if(serviceNumber==2)
			{
				paragraph.add(acCustomer.getSecondFree());
			}
			else if(serviceNumber==3)
			{
				paragraph.add(acCustomer.getThirdFree());
			}
			else if(serviceNumber==4)
			{
				paragraph.add(acCustomer.getForth_Service());
			}
			else if(serviceNumber==5)
			{
				paragraph.add(acCustomer.getFifth_Service());
			}
			else if(serviceNumber==6)
			{
				paragraph.add(acCustomer.getSixth_Service());
			}
			
			paragraph.add(" . ");
			document.add(paragraph);
			paragraph.clear();
				
			document.add( Chunk.NEWLINE );
			paragraph.add("We would like kindly inform you to call theses number (0766204326/0769376019) & make an appointment with us at your convenience .");
			document.add(paragraph);
			paragraph.clear();
				
			document.add( Chunk.NEWLINE );
			paragraph.add("Thanking You, \nSoftlogic Service Management");
			document.add(paragraph);
			paragraph.clear();
		}
		catch(Exception ex)
		{
			System.out.println("class:PDFGenerator,method:firstServicePDFGenerator"+ex);
			return false;
		}
		return true;
	}

}
