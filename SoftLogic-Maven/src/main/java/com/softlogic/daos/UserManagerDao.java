package com.softlogic.daos;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.softlogic.models.AC_Customer;
import com.softlogic.models.User;

@Repository
public class UserManagerDao implements IUserManagerDao 
{
	@Autowired
	private SessionFactory sessionFactory;
	
	public ArrayList<User> readUserData(String userName,String passWord)
	{
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("userName",userName));
		criteria.add(Restrictions.eq("passWord",passWord));
		ArrayList<User> userList = (ArrayList<User>) criteria.list();
		tx.commit();
		return userList;
	}
	
	public boolean insertUser(User user)
	{
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        session.save(user);
	        tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			if (tx!=null) tx.rollback();
			return false;
		}
		return true;
	}
	public ArrayList<User> selectAllUsers()
	{	    	
		Transaction tx = null;
		ArrayList<User> userList = new ArrayList<User>();
		try
		{
			
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(User.class);
			userList = (ArrayList<User>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			if (tx!=null) tx.rollback();
		}

		System.out.println("User list collected");
		return userList;
	}
	public void DeleteUserRowByPK(User user)
	{
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        session.delete(user);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
	}
	public User selectDetailsFromUser_id(int user_id)
	{
		System.out.println("user_id"+user_id);
		User user= new User();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("id",user_id));
			user = (User) criteria.uniqueResult();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		System.out.println(user.getUserName());
		return user;
	}
	public void updateUser(User user)
	{
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        session.update(user);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
	}

	@Override
	public boolean checkUserName(String userName) 
	{
		User user=new User();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("userName",userName));
			user = (User) criteria.uniqueResult();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		if(user!=null)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
}
