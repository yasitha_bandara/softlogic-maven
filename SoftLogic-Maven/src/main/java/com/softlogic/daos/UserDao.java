package com.softlogic.daos;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.softlogic.models.User;
@Repository
public class UserDao implements IUserDao
{
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public void addUser(User user) 
	{
		sessionFactory.getCurrentSession().save(user);
	}

	@Override
	public void editUser(User user) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(user);
		
	}

	@Override
	public void deleteUser(int user_id) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().delete(findUser(user_id));
	}

	@Override
	public User findUser(int user_id) {
		// TODO Auto-generated method stub
		return (User)sessionFactory.getCurrentSession().get(User.class,user_id);
	}

	@Override
	public User finedUserByName(String userName) 
	{
		User user=null;
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("userName",userName));
			user=(User)criteria.uniqueResult();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			if (tx!=null) tx.rollback();
		}
		return user;
	}

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("from user").list();
	}

}
