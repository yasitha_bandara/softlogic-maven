package com.softlogic.daos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import org.apache.poi.hssf.usermodel.HSSFCell;
//import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.hssf.usermodel.XSSFDateUtil;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.softlogic.models.Customer;
import com.softlogic.models.AC_Customer;

public class FileReadDao implements IFileReaderDao
{
	public ArrayList<Customer> readAllCustomerDetails(String filepath)
	{
		ArrayList<Customer> customerList = new ArrayList<Customer>();
		Customer customer;
		//String filepath="C://Windows//Temp//Demo//"+fileName;
		
		try
		{
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filepath));
			HSSFSheet sheet = workbook.getSheetAt(0);
			System.out.println(sheet.getLastRowNum());
			
			for(int x=1;x<(sheet.getLastRowNum()+1);x++)
			{
				customer = new Customer();
				HSSFRow row = sheet.getRow(x);
				
				if(row.getCell(0)!=null)
				{
					if(row.getCell(0).getCellType()== HSSFCell.CELL_TYPE_STRING)
					{
						String nicNo=row.getCell(0).getStringCellValue();
						System.out.println(nicNo);
						customer.setCustomer_nic(nicNo);
					}
					if(row.getCell(0).getCellType()== HSSFCell.CELL_TYPE_NUMERIC)
					{
						String nicNo = Double.toString(row.getCell(0).getNumericCellValue());
						System.out.println(nicNo);
						customer.setCustomer_nic(nicNo);
					}

				}
				//System.out.println(nicNo);
				if(row.getCell(1)!=null)
				{
					if(row.getCell(0).getCellType()== HSSFCell.CELL_TYPE_STRING)
					{
						String firstName=row.getCell(1).getStringCellValue();
						System.out.println(firstName);
						customer.setCustomer_first_name(firstName);
					}
					if(row.getCell(0).getCellType()== HSSFCell.CELL_TYPE_NUMERIC)
					{
						String firstName = Double.toString(row.getCell(1).getNumericCellValue());
						System.out.println(firstName);
						customer.setCustomer_nic(firstName);
					}
				}
				if(row.getCell(2)!=null)
				{
					String lastName=row.getCell(2).getStringCellValue();
					System.out.println(lastName);
					customer.setCustomer_last_name(lastName);
				}
				if(row.getCell(3)!=null)
				{
					String adddessLine1=row.getCell(3).getStringCellValue();
					System.out.println(adddessLine1);
					customer.setAddress1(adddessLine1);
				}
				if(row.getCell(4)!=null)
				{

					String adddessLine2=row.getCell(4).getStringCellValue();
					System.out.println(adddessLine2);
					customer.setAddress2(adddessLine2);
				}

				if(row.getCell(5)!=null)
				{
					String adddessLine3=row.getCell(5).getStringCellValue();
					System.out.println(adddessLine3);
					customer.setAddress3(adddessLine3);
				}
				if(row.getCell(6)!=null)
				{
					String adddessLine4=row.getCell(6).getStringCellValue();
					System.out.println(adddessLine4);
					customer.setAddress4(adddessLine4);
				}
				if(row.getCell(7)!=null)
				{
					String eMail=row.getCell(7).getStringCellValue();
					System.out.println(eMail);
					customer.setEmail(eMail);
				}
				if(row.getCell(8)!=null)
				{
					String telephone=row.getCell(8).getStringCellValue();
					System.out.println(telephone);
					customer.setTelephone(telephone);
				}
				if(row.getCell(9)!=null)
				{
					String mobilePhone=row.getCell(9).getStringCellValue();
					System.out.println(mobilePhone);
					customer.setMobile_phone(mobilePhone);
				}
				customerList.add(customer);
				//System.out.println(nicNo+","+firstName+","+lastName+","+adddessLine1+","+adddessLine2+","+adddessLine3+","+adddessLine4+","+eMail+","+telephone+","+mobilePhone);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return customerList;
	}
	public ArrayList<AC_Customer> readAllAC_CustomerDetailsXls(String filepath)
	{
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		AC_Customer acCustomer;
		//String filepath="C://Windows//Temp//Demo//"+fileName;
		
		try
		{
			HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filepath));
			HSSFSheet sheet = workbook.getSheetAt(0);
			System.out.println(sheet.getLastRowNum());
			
			for(int x=1;x<(sheet.getLastRowNum()+1);x++)
			{
				acCustomer = new AC_Customer();
				HSSFRow row = sheet.getRow(x);
				
				/*if(row.getCell(0)!=null)
				{
					if(row.getCell(0).getCellType()== HSSFCell.CELL_TYPE_STRING)
					{
						String month=row.getCell(0).getStringCellValue();
						System.out.println(month);
						acCustomer.setMonth(month);
					}
					if(row.getCell(0).getCellType()== HSSFCell.CELL_TYPE_NUMERIC)
					{
						String month = Double.toString(row.getCell(0).getNumericCellValue());
						System.out.println(month);
						acCustomer.setMonth(month);
					}
				}*/
				//System.out.println(nicNo);
				String month= this.validateToMonthStringXls(row, 0);
				if(!(40 < month.length()))
				{
					acCustomer.setMonth(month);
				}
				
				String installationDate=this.validateToIntallation_dateStringXls(row,1);
				acCustomer.setInstallationDate(installationDate);
				
				String jobNo=this.validatefromDateNumericAndStringToStringXls(row,2);
				acCustomer.setJobNo(jobNo);
				
				String customerName = this.validatefromDateNumericAndStringToStringXls(row,3);
				acCustomer.setCustomerName(customerName);

				String customerAdress= this.validatefromDateNumericAndStringToStringXls(row,4);
				acCustomer.setCustomerAdress(customerAdress);

				String city=this.validatefromDateNumericAndStringToStringXls(row,5);
				acCustomer.setCity(city);

				String contact=this.validatefromDateNumericAndStringToStringXls(row,6);
				acCustomer.setContact(contact);
				
				String contactNo=this.validatefromDateNumericAndStringToStringXls(row,7);
				acCustomer.setContactNo(contactNo);

				String type = this.validatefromDateNumericAndStringToStringXls(row,8);
				acCustomer.setType(type);

				String unit= this.validatefromDateNumericAndStringToStringXls(row,9);
				acCustomer.setUnit(unit);
				
				String capacity=this.validatefromDateNumericAndStringToStringXls(row,10);
				acCustomer.setCapacity(capacity);
				
				String serialNumberIndoor=this.validatefromDateNumericAndStringToStringXls(row,11);
				acCustomer.setSerialNumberIndoor(serialNumberIndoor);

				String serialNumberOutdoor=this.validatefromDateNumericAndStringToStringXls(row,12);
				acCustomer.setSerialNumberOutdoor(serialNumberOutdoor);

				String no_name=this.validatefromDateNumericAndStringToStringXls(row,13);
				acCustomer.setNo_name(no_name);
				
				String instNo=this.validatefromDateNumericAndStringToStringXls(row,14);
				acCustomer.setInstNo(instNo);
				
				String intalledBy=this.validatefromDateNumericAndStringToStringXls(row,15);
				acCustomer.setIntalledBy(intalledBy);
				
				String empty=this.validatefromDateNumericAndStringToStringXls(row,16);
				acCustomer.setFrequency(empty);
				
				String serviceTeam=this.validatefromDateNumericAndStringToStringXls(row,17);
				acCustomer.setServiceTeam(serviceTeam);
				
				String firstFree=this.validatefromDateNumericAndStringToStringXls(row,18);
				acCustomer.setFirstFree(firstFree);

				
				String secondFree=this.validatefromDateNumericAndStringToStringXls(row,19);
				acCustomer.setSecondFree(secondFree);
				
				String thirdFree=this.validatefromDateNumericAndStringToStringXls(row,20);
				acCustomer.setThirdFree(thirdFree);
				
				acCustomerList.add(acCustomer);
				//System.out.println(nicNo+","+firstName+","+lastName+","+adddessLine1+","+adddessLine2+","+adddessLine3+","+adddessLine4+","+eMail+","+telephone+","+mobilePhone);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return acCustomerList;
	}
	
	private String validateToString(HSSFRow row,int cellNumber)
	{
		String cellValue=null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== HSSFCell.CELL_TYPE_STRING)
				{
					cellValue=row.getCell(cellNumber).getStringCellValue();
				}
				if(row.getCell(cellNumber).getCellType()== HSSFCell.CELL_TYPE_NUMERIC)
				{
					cellValue = Double.toString(row.getCell(cellNumber).getNumericCellValue());
	
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return cellValue;
	}
	private Date validateToDateXls(HSSFRow row,int cellNumber)
	{
		Date cellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					cellValue =row.getCell(cellNumber).getDateCellValue();
					System.out.println ("////////Row No.: " + row.getRowNum ()+ " " + 
							row.getCell(cellNumber).getDateCellValue());
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return cellValue;
	}
	private String validatefromDateNumericAndStringToStringXls(HSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return stringCellValue;
	}
	private String validateToIntallation_dateStringXls(HSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return stringCellValue;
	}
	private String validateToMonthStringXls(HSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return stringCellValue;
	}
	public ArrayList<AC_Customer> readAllAC_CustomerDetailsXlsx(String filepath)
	{
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		AC_Customer acCustomer;
		//String filepath="C://Windows//Temp//Demo//"+fileName;
		
		try
		{
			XSSFWorkbook workbook = new XSSFWorkbook(new FileInputStream(filepath));
			XSSFSheet sheet = workbook.getSheetAt(0);
			//System.out.println(sheet.getLastRowNum());
			XSSFRow row = null;
			for(int x=1;x<(sheet.getLastRowNum()+1);x++)
			{
				row = sheet.getRow(x);
				if(!blankRow(row))
				{
					acCustomer = new AC_Customer();
					
					String month= this.validateToMonthStringXlsx(row, 0);
					System.out.println("month.length:"+ month.length());
					if(month!=null)
					{
						if(!(45 < month.length()))
						{
							acCustomer.setMonth(month);
						}
					}
					System.out.println("month:"+ month);
					String installationDate=this.validateToIntallation_dateStringXlsx(row,1);
					if(installationDate!=null)
					{	
						if(!(45 < installationDate.length()))
						{
							acCustomer.setInstallationDate(installationDate);
						}
					}	
					System.out.println("setInstallationDate:"+ installationDate);
					System.out.println("acCustomer.getInstallationDate(installationDate);"+ acCustomer.getInstallationDate());
					String jobNo=this.validatefromDateNumericAndStringToStringXlsx(row,2);
					if(jobNo!=null)
					{
						if(!(45 < jobNo.length()))
						{
							acCustomer.setJobNo(jobNo);
						}
					}
					String customerName = this.validatefromDateNumericAndStringToStringXlsx(row,3);
					if(customerName!=null)
					{
						if(!(250 < customerName.length()))
						{
							acCustomer.setCustomerName(customerName);
						}
					}
					String customerAdress= this.validatefromDateNumericAndStringToStringXlsx(row,4);
					if(customerAdress!=null)
					{
						if(!(250 < customerAdress.length()))
						{
							acCustomer.setCustomerAdress(customerAdress);
						}
					}
					String city=this.validatefromDateNumericAndStringToStringXlsx(row,5);
					if(city!=null)
					{
						if(!(45 < city.length()))
						{
							acCustomer.setCity(city);
						}
					}	
					String contact=this.validatefromDateNumericAndStringToStringXlsx(row,6);
					if(contact!=null)
					{
						if(!(45 < contact.length()))
						{
							acCustomer.setContact(contact);
						}
					}
					String contactNo=this.validatefromDateNumericAndStringToStringXlsx(row,7);
					if(contactNo!=null)
					{
						if(!(45 < contactNo.length()))
						{
							acCustomer.setContactNo(contactNo);
						}
					}
					String type = this.validatefromDateNumericAndStringToStringXlsx(row,8);
					if(type!=null)
					{
						if(!(45 < type.length()))
						{
							acCustomer.setType(type);
						}
					}
					String unit= this.validatefromDateNumericAndStringToStringXlsx(row,9);
					if(unit!=null)
					{
						if(!(45 < unit.length()))
						{
							acCustomer.setUnit(unit);
						}
					}
					String capacity=this.validatefromDateNumericAndStringToStringXlsx(row,10);
					if(capacity!=null)
					{
						if(!(45 < capacity.length()))
						{
							acCustomer.setCapacity(capacity);
						}
					}
					String serialNumberIndoor=this.validatefromDateNumericAndStringToStringXlsx(row,11);
					if(serialNumberIndoor!=null)
					{
						if(!(250 < serialNumberIndoor.length()))
						{
							acCustomer.setSerialNumberIndoor(serialNumberIndoor);
						}
					}
					String serialNumberOutdoor=this.validatefromDateNumericAndStringToStringXlsx(row,12);
					if(serialNumberOutdoor!=null)
					{
						if(!(250 < serialNumberOutdoor.length()))
						{
							acCustomer.setSerialNumberOutdoor(serialNumberOutdoor);
						}
					}
					String no_name=this.validatefromDateNumericAndStringToStringXlsx(row,13);
					if(no_name!=null)
					{
						if(!(45 < no_name.length()))
						{
							acCustomer.setNo_name(no_name);
						}
					}
					String instNo=this.validatefromDateNumericAndStringToStringXlsx(row,14);
					if(instNo!=null)
					{
						if(!(45 < instNo.length()))
						{
							acCustomer.setInstNo(instNo);
						}
					}
					String intalledBy=this.validatefromDateNumericAndStringToStringXlsx(row,15);
					if(intalledBy!=null)
					{
						if(!(45 < intalledBy.length()))
						{
							acCustomer.setIntalledBy(intalledBy);
						}
					}
					String empty=this.validatefromDateNumericAndStringToStringXlsx(row,16);
					if(empty!=null)
					{
						if(!(45 < empty.length()))
						{
							acCustomer.setFrequency(empty);
						}
					}
					String serviceTeam=this.validatefromDateNumericAndStringToStringXlsx(row,17);
					if(serviceTeam!=null)
					{
						if(!(45 < serviceTeam.length()))
						{
							acCustomer.setServiceTeam(serviceTeam);
						}
					}
					acCustomerList.add(acCustomer);
					System.out.println("acCustomer size at read file:"+ acCustomerList.size());
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println( "readAllAC_CustomerDetailsXlsx:"+ex);
		}
		return acCustomerList;
	}
	
	private String validateToStringXlsx(XSSFRow row,int cellNumber)
	{
		String cellValue=null;
		try
		{
			if(row.getCell(0)!=null)
			{

				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					cellValue=row.getCell(cellNumber).getStringCellValue();
				}

				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					cellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return cellValue;
	}
		private Date validateToDateXlsx(XSSFRow row,int cellNumber)
	{
		Date cellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					cellValue =row.getCell(cellNumber).getDateCellValue();
					System.out.println ("////////Row No.: " + row.getRowNum ()+ " " + 
							row.getCell(cellNumber).getDateCellValue());
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return cellValue;
	}
	private String validatefromDateNumericAndStringToStringXlsx(XSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return stringCellValue;
	}
	
	private String validateToMonthStringXlsx(XSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("MMM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return stringCellValue;
	}
	private String validateToIntallation_dateStringXlsx(XSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return stringCellValue;
	}
	
	private boolean blankRow(XSSFRow row)
	{
		boolean ret = true;
		
		for(int x=0;x<21;x++)
		{
			//if(row.getCell(x)!=null)
			if(row.getCell(x).getCellType() != XSSFCell.CELL_TYPE_BLANK)
			{
				ret = false;
				break;
			}
		}
		
		return ret;
	
	}
	private String createFirstServiceDateXlsx(XSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		String serviceDate = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				/*if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}*/
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(stringCellValue));
			c.add(Calendar.DATE, 90);  // number of days to add
			sdf = new SimpleDateFormat("dd-MM-yyyy");
			serviceDate = sdf.format(c.getTime());
			
			
			
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return serviceDate;

	}
	private String createSecondServiceDateXlsx(XSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		String serviceDate = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				/*if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}*/
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(stringCellValue));
			c.add(Calendar.DATE,180);  // number of days to add
			sdf = new SimpleDateFormat("dd-MM-yyyy");
			serviceDate = sdf.format(c.getTime());
			
			
			
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return serviceDate;

	}
	private String createThiredServiceDateXlsx(XSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		String serviceDate = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
				}
				/*if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}*/
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Calendar c = Calendar.getInstance();
			c.setTime(sdf.parse(stringCellValue));
			c.add(Calendar.DATE, 270);  // number of days to add
			sdf = new SimpleDateFormat("dd-MM-yyyy");
			serviceDate = sdf.format(c.getTime());
			
			
			
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return serviceDate;

	}
	private String validatefromDateNumericAndStringToDashedDateXlsx(XSSFRow row,int cellNumber)
	{
		String stringCellValue = null;
		Date dateCellValue = null;
		try
		{
			if(row.getCell(cellNumber)!=null)
			{
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_STRING)
				{
					stringCellValue=row.getCell(cellNumber).getStringCellValue();
					DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
					
					dateCellValue = format.parse(stringCellValue);
					format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
				if(row.getCell(cellNumber).getCellType()== XSSFCell.CELL_TYPE_NUMERIC)
				{
					stringCellValue = NumberToTextConverter.toText(row.getCell(cellNumber).getNumericCellValue());
				}
				if (DateUtil.isCellDateFormatted(row.getCell(cellNumber))) 
				{
					dateCellValue =row.getCell(cellNumber).getDateCellValue();
					DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
					stringCellValue = format.format(dateCellValue);
				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex);
		}
		return stringCellValue;
	}
}
