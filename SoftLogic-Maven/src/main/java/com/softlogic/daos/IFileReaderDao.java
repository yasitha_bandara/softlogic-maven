package com.softlogic.daos;

import java.util.ArrayList;

import com.softlogic.models.Customer;
import com.softlogic.models.AC_Customer; 

public interface IFileReaderDao 
{
	public ArrayList<Customer> readAllCustomerDetails(String fileName);
	public ArrayList<AC_Customer> readAllAC_CustomerDetailsXls(String fileName);
	public ArrayList<AC_Customer> readAllAC_CustomerDetailsXlsx(String fileName);
}
