package com.softlogic.daos;

import java.util.ArrayList;

import com.softlogic.models.AC_Customer;
import com.softlogic.models.PrintModel;

public interface IPDFGenerator 
{
	public boolean firstServicePDFGenerator(ArrayList<AC_Customer> acCustomerList);
	public boolean secondServicePDFGenerator(ArrayList<AC_Customer> acCustomerList);
	public boolean thiredServicePDFGenerator(ArrayList<AC_Customer> acCustomerList);
	public boolean AllServicePDFGenerator(ArrayList<AC_Customer> acCustomerfirstServiceList,ArrayList<AC_Customer> acCustomerSecondServiceList,ArrayList<AC_Customer> acCustomerThiredServiceList,ArrayList<AC_Customer> acCustomerfirstAMCServiceList,ArrayList<AC_Customer> acCustomerSecondAMCServiceList,ArrayList<AC_Customer> acCustomerThiredAMCServiceList,ArrayList<AC_Customer> acCustomerforthServiceList,ArrayList<AC_Customer> acCustomerfifthServiceList,ArrayList<AC_Customer> acCustomersixthServiceList);
	public boolean AllServicePDFGeneratorDBOder(ArrayList<PrintModel> printModelList);
}
