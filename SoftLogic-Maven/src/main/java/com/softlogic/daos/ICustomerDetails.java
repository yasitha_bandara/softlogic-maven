package com.softlogic.daos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.softlogic.models.AC_Customer;
import com.softlogic.models.Customer;

public interface ICustomerDetails 
{
	public void excelToMysql();
	public ArrayList<AC_Customer> getAC_CustomerData();
	public void insertMysql(AC_Customer newCustomer);
	public void createExcelFile();
	public int testingTest(int number);
	public AC_Customer selectDetailsFromAC_customer_pk(String acCustomer_pk);
	public void updateAC_Customer(AC_Customer customer);
	public void DeleteCustomerRowByPK(AC_Customer acCustomer);
	public ArrayList<AC_Customer> insertAC_CustomerList(ArrayList<AC_Customer> acCustomerList);
	public void deleteAllAC_Customers();
	public ArrayList<AC_Customer> selectCustomerDetailsByOneCriteria(String column,int value);
	public ArrayList<AC_Customer> selectCustomerDetailsBySixOrCriterias(String column1,int value1,String column2,int value2,String column3,int value3,String column4,int value4,String column5,int value5,String column6,int value6);
	public ArrayList<AC_Customer> selectCustomerDetailsByArrayOfPKs(String[] checkedArray);
	public ArrayList<AC_Customer> selectAC_CustomersBetweenTwoDays(String sdate,String edate);
	public ArrayList<AC_Customer> searchByColumn(String column,String keywrd);
	public ArrayList<AC_Customer> takeLastThousandCustomers(int numOfRows);
	//public boolean deleteCustomerDetailsByListOfCustomer(List<AC_Customer> acCustomerList);
	public long getTotalNumberofCustomerRecoreds();
	public ArrayList<AC_Customer> selectSetOfRows(int firstRow, int numOfRows);
	public ArrayList<AC_Customer> selectAC_CustomersBetweenTwoInstallatoinDates(String fromDate, String toDate);
	public ArrayList<AC_Customer> searchByColumnMiddle(String string,String searchKeyWords);
}
