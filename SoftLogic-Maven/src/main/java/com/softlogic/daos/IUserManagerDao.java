package com.softlogic.daos;
import java.util.ArrayList;

import com.softlogic.models.User;

public interface IUserManagerDao 
{
	public ArrayList<User> readUserData(String userName,String passWord);
	public boolean insertUser(User user);
	public ArrayList<User> selectAllUsers();
	public void DeleteUserRowByPK(User user);
	public User selectDetailsFromUser_id(int user_id);
	public void updateUser(User user);
	public boolean checkUserName(String userName);
}
