package com.softlogic.daos;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import com.softlogic.models.AC_Customer;
import com.softlogic.models.Customer;

@Repository
public class CustomerDetails implements ICustomerDetails
{
	@Autowired
	private SessionFactory sessionFactory;
	
	public void excelToMysql()
	{
		System.out.println("excelToMysql");
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        SQLQuery insertQuery = session.createSQLQuery("LOAD DATA LOCAL INFILE 'C:\\\\Windows\\\\Temp\\\\test.csv' INTO TABLE softlogic.customer_details FIELDS TERMINATED BY ',' ENCLOSED BY '' LINES TERMINATED BY '\r\n' IGNORE 1 LINES (customer_nic,customer_first_name,customer_last_name,address1,address2,address3,address4,email,telephone,mobile_phone);");
			//SQLQuery insertQuery = session.createSQLQuery("");
	        insertQuery.executeUpdate();
	        tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			if (tx!=null) tx.rollback();
		}
        //session.getTransaction().commit();//this also working
	}
	public ArrayList<AC_Customer> getAC_CustomerData()
	{	    	
		Session session = sessionFactory.getCurrentSession();
		Transaction tx = session.beginTransaction();
		Criteria criteria = session.createCriteria(AC_Customer.class);
		//criteria.add(Restrictions.eq("userName",username));
		//criteria.add(Restrictions.eq("passWord",password));
		ArrayList<AC_Customer> acCustomerList = (ArrayList<AC_Customer>) criteria.list();
		tx.commit();
		//session.close();

		
		/*for(int i = 0; i < acCustomerList.size(); i++) 
		{
			AC_Customer ac_customerElement = acCustomerList.get(i);
			System.out.println(ac_customerElement.getCustomer_pk());
			System.out.println(ac_customerElement.getCustomerName());
		}*/

		System.out.println("Customer list collected");
		return acCustomerList;
	}
	public void insertMysql(AC_Customer newCustomer)
	{
		System.out.println("insertMysql");
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        //SQLQuery insertQuery = session.createSQLQuery("INSERT INTO `softlogic`.`customer_details` (`customer_nic`, `customer_first_name`, `customer_last_name`, `address1`, `address2`, `address3`, `address4`, `email`, `telephone`, `mobile_phone`) VALUES ('1233454556v', 'nalinda', 'Damith', 'No 80', 'Aggona waththa', 'Wawala', 'Horana', 'yasitha@gmail.com', '0112908904', '071-122334098');");
	        //insertQuery.executeUpdate();
	        session.save(newCustomer);
	        tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			if (tx!=null) tx.rollback();
		}
	}
	public void createExcelFile()
	{
		System.out.println("createExcelFile");
		try 
		{
			File file = new File("c:\\Windows\\temp\\Demo\\customer_details.xls");
        	file.delete();
		} 
		catch (Exception x) 
		{
			System.out.println(x);
		}
		Session session = null;
		Transaction tx = null;
		try
		{
			session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Query query =session.createSQLQuery("SELECT 'MONTH','INSTALLATION_DATE','JOB_NO','CUSTOMER_NAME','CUSTOMER_ADRESS','CITY','CONTACT','CONTACT_NO','TYPE','UNITS','CAPACITY','SERIAL_NUMER_INDOOR','SERIAL_NUMBER_OUTDOOR','','INST_NO','INSTALLED_BY','','SERVICE_TEAM','1ST_FREE','2ND_FREE','3RD_FREE' UNION ALL SELECT MONTH, INSTALLATION_DATE, JOB_NO, CUSTOMER_NAME, CUSTOMER_ADRESS, CITY, CONTACT, CONTACT_NO, TYPE, UNITS, CAPACITY, SERIAL_NUMER_INDOOR, SERIAL_NUMBER_OUTDOOR, no_name, INST_NO, INSTALLED_BY, empty, SERVICE_TEAM, 1ST_FREE, 2ND_FREE, 3RD_FREE INTO OUTFILE 'C:\\\\Windows\\\\temp\\\\Demo\\\\customer_details.xls' FROM softlogic.ac_customer");
			query.list();
			//query.executeUpdate();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
	}
	public AC_Customer selectDetailsFromAC_customer_pk(String acCustomer_pk)
	{
		AC_Customer acCustomer= new AC_Customer();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(AC_Customer.class);
			criteria.add(Restrictions.eq("customer_pk",acCustomer_pk));
			//criteria.add(Restrictions.eq("passWord",password));
			acCustomer = (AC_Customer) criteria.uniqueResult();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return acCustomer;
	}
	public void updateAC_Customer(AC_Customer acCustomer)
	{
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        session.update(acCustomer);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
	}
	public void DeleteCustomerRowByPK(AC_Customer acCustomer)
	{
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	         session.delete(acCustomer);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
	}
	
	public ArrayList<AC_Customer> insertAC_CustomerList(ArrayList<AC_Customer> acCustomerList)
	{
		System.out.println(acCustomerList.size());
		ArrayList<AC_Customer> lastAC_CustomersArrayList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        //SQLQuery insertQuery = session.createSQLQuery("INSERT INTO `softlogic`.`customer_details` (`customer_nic`, `customer_first_name`, `customer_last_name`, `address1`, `address2`, `address3`, `address4`, `email`, `telephone`, `mobile_phone`) VALUES ('1233454556v', 'nalinda', 'Damith', 'No 80', 'Aggona waththa', 'Wawala', 'Horana', 'yasitha@gmail.com', '0112908904', '071-122334098');");
	        //insertQuery.executeUpdate();
			for (int i = 0; i < acCustomerList.size(); i++) 
			{
				session.save(acCustomerList.get(i));
			}
			//String hqlQuery = "from AC_Customer order by customer_pk DESC LIMIT1";
			Query query = session.createQuery("from AC_Customer order by customer_pk DESC");
			query.setFirstResult(0);
			query.setMaxResults(acCustomerList.size());
			List<AC_Customer> lastAC_CustomersList = query.list();
			lastAC_CustomersArrayList = new ArrayList<AC_Customer>(lastAC_CustomersList);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			if (tx!=null) tx.rollback();
		}
		/*for (int i = 0; i < lastAC_CustomersArrayList.size(); i++) 
		{
			System.out.println(lastAC_CustomersArrayList.get(i).getJobNo());
		}*/
		return lastAC_CustomersArrayList;
	}
	public void deleteAllAC_Customers()
	{
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
	        SQLQuery insertQuery = session.createSQLQuery("delete from ac_customer");
			//SQLQuery insertQuery = session.createSQLQuery("");
	        insertQuery.executeUpdate();
	        tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println(ex);
			if (tx!=null) tx.rollback();
		}
	}
	public ArrayList<AC_Customer> selectCustomerDetailsByOneCriteria(String column,int value)
	{
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(AC_Customer.class);
			criteria.add(Restrictions.eq(column,value));
			//criteria.add(Restrictions.eq("customer_pk",acCustomer_pk));
			//criteria.add(Restrictions.eq("passWord",password));
			
			acCustomerList = (ArrayList<AC_Customer>) criteria.list();;
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:selectCustomerDetailsByOneCriteria,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return acCustomerList;
	}
	public int testingTest(int number)
	{
		return number;
	}
	public ArrayList<AC_Customer> selectCustomerDetailsByArrayOfPKs(String[] checkedArray)
	{
		
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		
		Transaction tx = null;

		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			AC_Customer acCustomer;
			Criteria criteria;
			for(int i=0;i<checkedArray.length;i++)
			{
				criteria = session.createCriteria(AC_Customer.class);
				criteria.add(Restrictions.eq("customer_pk",checkedArray[i]));
				acCustomer = (AC_Customer) criteria.uniqueResult();
				acCustomerList.add(acCustomer);
				criteria = null;
			}
			//acCustomerList = (ArrayList<AC_Customer>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:selectCustomerDetailsByArrayOfPKs,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		for(int i = 0; i < acCustomerList.size(); i++) 
		{
			AC_Customer ac_customerElement = acCustomerList.get(i);
			//System.out.println(ac_customerElement.getCustomer_pk());
			//System.out.println(ac_customerElement.getCustomerName());
		}
		return acCustomerList;
	}
	//@SuppressWarnings("unchecked")
	public ArrayList<AC_Customer> selectCustomerDetailsBySixOrCriterias(String column1,int value1,String column2,int value2,String column3,int value3,String column4,int value4,String column5,int value5,String column6,int value6)
	{
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(AC_Customer.class);
			criteria.add(Restrictions.or(Restrictions.eq(column1,value1),Restrictions.eq(column2,value2),Restrictions.eq(column3,value3),Restrictions.eq(column4,value4),Restrictions.eq(column5,value5),Restrictions.eq(column6,value6)));
			//criteria.add(Restrictions.eq("customer_pk",acCustomer_pk));
			//criteria.add(Restrictions.eq("passWord",password));
			
			acCustomerList = (ArrayList<AC_Customer>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:selectCustomerDetailsByThreeOrCriterias,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return acCustomerList;
	}
	public ArrayList<AC_Customer> selectAC_CustomersBetweenTwoDays(String sdate,String edate)
	{
		
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(AC_Customer.class);

			criteria.add(
							Restrictions.or
							(
								Restrictions.and(Restrictions.ge("firstFree", sdate),Restrictions.lt("firstFree", edate)),
								Restrictions.and(Restrictions.ge("secondFree", sdate),Restrictions.lt("secondFree", edate)),
								Restrictions.and(Restrictions.ge("thirdFree", sdate),Restrictions.lt("thirdFree", edate)),
								Restrictions.and(Restrictions.ge("forth_Service", sdate),Restrictions.lt("forth_Service", edate)),
								Restrictions.and(Restrictions.ge("fifth_Service", sdate),Restrictions.lt("fifth_Service", edate)),
								Restrictions.and(Restrictions.ge("sixth_Service", sdate),Restrictions.lt("sixth_Service", edate))
							)
						);
			
			acCustomerList = (ArrayList<AC_Customer>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:selectAC_CustomersBetweenTwoDays,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return acCustomerList;
	}
	public ArrayList<AC_Customer> searchByColumn(String column,String keywrd)
	{
		System.out.println(column + keywrd);
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(AC_Customer.class);
			criteria.add(Restrictions.ilike(column,keywrd,MatchMode.START));
			acCustomerList = (ArrayList<AC_Customer>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:searchByColumn,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}

		return acCustomerList;
	}
	public ArrayList<AC_Customer> searchByColumnMiddle(String column,String keywrd)
	{
		System.out.println(column + keywrd);
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(AC_Customer.class);
			criteria.add(Restrictions.ilike(column,keywrd,MatchMode.ANYWHERE));
			acCustomerList = (ArrayList<AC_Customer>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:searchByColumn,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}

		return acCustomerList;
	}
	public ArrayList<AC_Customer> takeLastThousandCustomers(int numOfRows)
	{
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Query query = session.createQuery("from AC_Customer order by customer_pk DESC");
			query.setFirstResult(0);
			query.setMaxResults(numOfRows);
			List<AC_Customer> lastAC_CustomersList = query.list();
			acCustomerList = new ArrayList<AC_Customer>(lastAC_CustomersList);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:takeLastThousandCustomers,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return acCustomerList;
	}
	//should develop for batch processing later
	/*public boolean deleteCustomerDetailsByListOfCustomer(List<AC_Customer> acCustomerList)
	{
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			getHibernateTemplate().deleteAll(questionsCollection);
			AC_Customer acCustomer=new AC_Customer();
			//Criteria criteria;
			for(int i=0;i<checkedArray.length;i++)
			{
				//criteria = session.createCriteria(AC_Customer.class);
				//criteria.add(Restrictions.eq("customer_pk",checkedArray[i]));
				acCustomer.setCustomer_pk(checkedArray[0]);
				//acCustomerList.add(acCustomer);
				session.delete(acCustomer);
				//criteria = null;
			}
			//acCustomerList = (ArrayList<AC_Customer>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:selectCustomerDetailsByArrayOfPKs,Exception: "+ ex);
			if (tx!=null) tx.rollback();
			return false;
		}

		return true;
	}*/
	public long getTotalNumberofCustomerRecoreds()
	{
		Transaction tx = null;
		long numberOfRecords = 0;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			
			Criteria criteria = session.createCriteria(AC_Customer.class);
			criteria.setProjection(Projections.rowCount());
			numberOfRecords = (Long) criteria.uniqueResult();
			System.out.println("numberOfPages: "+ numberOfRecords);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:getTotalNumberofCustomerRecoreds,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return numberOfRecords;
	}
	public ArrayList<AC_Customer> selectSetOfRows(int firstRow,int numOfRows)
	{
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Query query = session.createQuery("from AC_Customer order by customer_pk DESC");
			query.setFirstResult(firstRow);
			query.setMaxResults(numOfRows);
			List<AC_Customer> lastAC_CustomersList = query.list();
			acCustomerList = new ArrayList<AC_Customer>(lastAC_CustomersList);
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:selectSetOfRows,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return acCustomerList;
	}
	@Override
	public ArrayList<AC_Customer> selectAC_CustomersBetweenTwoInstallatoinDates(String fromDate, String toDate)
	{
		
		ArrayList<AC_Customer> acCustomerList = new ArrayList<AC_Customer>();
		Transaction tx = null;
		try
		{
			Session session = sessionFactory.getCurrentSession();
			tx = session.beginTransaction();
			Criteria criteria = session.createCriteria(AC_Customer.class);

			criteria.add(Restrictions.and(Restrictions.ge("dateInstDate", fromDate),Restrictions.lt("dateInstDate", toDate)));
			
			acCustomerList = (ArrayList<AC_Customer>) criteria.list();
			tx.commit();
		}
		catch(Exception ex)
		{
			System.out.println("Packege:daos,Class:CustomerDetails,method:selectAC_CustomersBetweenTwoDays,Exception: "+ ex);
			if (tx!=null) tx.rollback();
		}
		return acCustomerList;
	}
}
