package com.softlogic.daos;

import java.util.List;
import com.softlogic.models.User;

public interface IUserDao 
{
	void addUser(User user);
	void editUser(User user);
	void deleteUser(int user_id);
	User findUser(int user_id);
	User finedUserByName(String userName);
	List<User> getAllUsers();
}
