package com.softlogic.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ac_customer")
public class AC_Customer 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "customer_pk")
	private String customer_pk;
	
	@Column(name = "MONTH")
	private String month;
	
	@Column(name = "INSTALLATION_DATE")
	private String installationDate;
	
	@Column(name = "JOB_NO")
	private String jobNo;
	
	@Column(name = "CUSTOMER_NAME")
	private String customerName;
	
	@Column(name = "CUSTOMER_ADRESS")
	private String customerAdress;
	
	@Column(name = "CITY")
	private String city;
	
	@Column(name = "CONTACT")
	private String contact;
	
	@Column(name = "CONTACT_NO")
	private String contactNo;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "UNITS")
	private String unit;
	
	@Column(name = "CAPACITY")
	private String capacity;
	
	@Column(name = "SERIAL_NUMER_INDOOR")
	private String serialNumberIndoor;
	
	@Column(name = "SERIAL_NUMBER_OUTDOOR")
	private String serialNumberOutdoor;
	
	@Column(name = "no_name")
	private String no_name;
	
	@Column(name = "INST_NO")
	private String instNo;
	
	@Column(name = "INSTALLED_BY")
	private String intalledBy;
	
	@Column(name = "empty")
	private String frequency;
	
	@Column(name = "SERVICE_TEAM")
	private String serviceTeam;
	
	@Column(name = "1ST_FREE")
	private String firstFree;
	
	@Column(name = "2ND_FREE")
	private String secondFree;
	
	@Column(name = "3RD_FREE")
	private String thirdFree;
	
	@Column(name = "4th_Service")
	private String forth_Service;
	
	@Column(name = "5th_Service")
	private String fifth_Service;
	
	@Column(name = "6th_Service")
	private String sixth_Service;
	
	@Column(name = "1ST_FREE_Status")
	private int firstFreeStatus;
	
	@Column(name = "2ND_FREE_Status")
	private int secondFreeStatus;
	
	@Column(name = "3ND_FREE_Status")
	private int thirdFreeStatus;
	
	@Column(name = "4th_Service_Status")
	private int forthServiceStatus;
	
	@Column(name = "5th_Service_Status")
	private int fifthServiceStatus;
	
	@Column(name = "6th_Service_Status")
	private int sixthServiceStatus;

	@Column(name = "1ST_FREE_Print_Status")
	private int firstFreePrintStatus;
	
	@Column(name = "2ND_FREE_Print_Status")
	private int secondFreePrintStatus;
	
	@Column(name = "3RD_FREE_Print_Status")
	private int thirdFreePrintStatus;

	@Column(name = "4th_Service_Print_Status")
	private int forthServicePrintStatus;
	
	@Column(name = "5th_Service_Print_Status")
	private int fifthServicePrintStatus;
	
	@Column(name = "6th_Service_Print_Status")
	private int sixthServicePrintStatus;
	
	@Column(name = "AMC")
	private int amc;

	@Column(name = "Date_instDate")
	private String dateInstDate;
	
	@Column(name = "read_error")
	private int readError;

	@Column(name = "comments")
	private String comments;
	
	public String getCustomer_pk() {
		return customer_pk;
	}

	public void setCustomer_pk(String customer_pk) {
		this.customer_pk = customer_pk;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}

	public String getJobNo() {
		return jobNo;
	}

	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAdress() {
		return customerAdress;
	}

	public void setCustomerAdress(String customerAdress) {
		this.customerAdress = customerAdress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getSerialNumberIndoor() {
		return serialNumberIndoor;
	}

	public void setSerialNumberIndoor(String serialNumberIndoor) {
		this.serialNumberIndoor = serialNumberIndoor;
	}

	public String getSerialNumberOutdoor() {
		return serialNumberOutdoor;
	}

	public void setSerialNumberOutdoor(String serialNumberOutdoor) {
		this.serialNumberOutdoor = serialNumberOutdoor;
	}

	public String getNo_name() {
		return no_name;
	}

	public void setNo_name(String no_name) {
		this.no_name = no_name;
	}

	public String getInstNo() {
		return instNo;
	}

	public void setInstNo(String instNo) {
		this.instNo = instNo;
	}

	public String getIntalledBy() {
		return intalledBy;
	}

	public void setIntalledBy(String intalledBy) {
		this.intalledBy = intalledBy;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getServiceTeam() {
		return serviceTeam;
	}

	public void setServiceTeam(String serviceTeam) {
		this.serviceTeam = serviceTeam;
	}

	public String getFirstFree() {
		return firstFree;
	}

	public void setFirstFree(String firstFree) {
		this.firstFree = firstFree;
	}

	public String getSecondFree() {
		return secondFree;
	}

	public void setSecondFree(String secondFree) {
		this.secondFree = secondFree;
	}

	public String getThirdFree() {
		return thirdFree;
	}

	public void setThirdFree(String thirdFree) {
		this.thirdFree = thirdFree;
	}
	public String getForth_Service() {
		return forth_Service;
	}

	public void setForth_Service(String forth_Service) {
		this.forth_Service = forth_Service;
	}

	public String getFifth_Service() {
		return fifth_Service;
	}

	public void setFifth_Service(String fifth_Service) {
		this.fifth_Service = fifth_Service;
	}

	public String getSixth_Service() {
		return sixth_Service;
	}

	public void setSixth_Service(String sixth_Service) {
		this.sixth_Service = sixth_Service;
	}
	public int getFirstFreeStatus() {
		return firstFreeStatus;
	}

	public void setFirstFreeStatus(int firstFreeStatus) {
		this.firstFreeStatus = firstFreeStatus;
	}

	public int getSecondFreeStatus() {
		return secondFreeStatus;
	}

	public void setSecondFreeStatus(int secondFreeStatus) {
		this.secondFreeStatus = secondFreeStatus;
	}

	public int getThirdFreeStatus() {
		return thirdFreeStatus;
	}

	public void setThirdFreeStatus(int thirdFreeStatus) {
		this.thirdFreeStatus = thirdFreeStatus;
	}
	public int getFirstFreePrintStatus() {
		return firstFreePrintStatus;
	}

	public void setFirstFreePrintStatus(int firstFreePrintStatus) {
		this.firstFreePrintStatus = firstFreePrintStatus;
	}

	public int getSecondFreePrintStatus() {
		return secondFreePrintStatus;
	}

	public void setSecondFreePrintStatus(int secondFreePrintStatus) {
		this.secondFreePrintStatus = secondFreePrintStatus;
	}

	public int getThirdFreePrintStatus() {
		return thirdFreePrintStatus;
	}

	public void setThirdFreePrintStatus(int thirdFreePrintStatus) {
		this.thirdFreePrintStatus = thirdFreePrintStatus;
	}
	public int getForthServiceStatus() {
		return forthServiceStatus;
	}

	public void setForthServiceStatus(int forthServiceStatus) {
		this.forthServiceStatus = forthServiceStatus;
	}

	public int getFifthServiceStatus() {
		return fifthServiceStatus;
	}

	public void setFifthServiceStatus(int fifthServiceStatus) {
		this.fifthServiceStatus = fifthServiceStatus;
	}

	public int getSixthServiceStatus() {
		return sixthServiceStatus;
	}

	public void setSixthServiceStatus(int sixthServiceStatus) {
		this.sixthServiceStatus = sixthServiceStatus;
	}
	public int getForthServicePrintStatus() {
		return forthServicePrintStatus;
	}

	public void setForthServicePrintStatus(int forthServicePrintStatus) {
		this.forthServicePrintStatus = forthServicePrintStatus;
	}

	public int getFifthServicePrintStatus() {
		return fifthServicePrintStatus;
	}

	public void setFifthServicePrintStatus(int fifthServicePrintStatus) {
		this.fifthServicePrintStatus = fifthServicePrintStatus;
	}

	public int getSixthServicePrintStatus() {
		return sixthServicePrintStatus;
	}

	public void setSixthServicePrintStatus(int sixthServicePrintStatus) {
		this.sixthServicePrintStatus = sixthServicePrintStatus;
	}
	public int getAmc() {
		return amc;
	}

	public void setAmc(int amc) {
		this.amc = amc;
	}
	
	public int getReadError() {
		return readError;
	}
	
	public void setReadError(int readError) {
		this.readError = readError;
	}
	public String getDateInstDate() {
		return dateInstDate;
	}

	public void setDateInstDate(String dateInstDate) {
		this.dateInstDate = dateInstDate;
	}
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
}

