package com.softlogic.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class VM_AC_Customer 
{

	private String customer_pk;
	

	private String month;
	
	private String installationDate;
	
	private String jobNo;
	
	private String customerName;
	
	private String customerAdress;
	
	private String city;
	
	private String contact;
	
	private String contactNo;
	
	private String type;
	
	private String unit;
	
	private String capacity;
	
	private String serialNumberIndoor;
	
	private String serialNumberOutdoor;
	
	private String no_name;
	
	private String instNo;
	
	private String intalledBy;
	
	private String empty;
	
	private String serviceTeam;
	
	private String firstFree;
	
	private String secondFree;
	
	private String thirdFree;
	
	public String getCustomer_pk() {
		return customer_pk;
	}

	public void setCustomer_pk(String customer_pk) {
		this.customer_pk = customer_pk;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getInstallationDate() {
		return installationDate;
	}

	public void setInstallationDate(String installationDate) {
		this.installationDate = installationDate;
	}

	public String getJobNo() {
		return jobNo;
	}

	public void setJobNo(String jobNo) {
		this.jobNo = jobNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerAdress() {
		return customerAdress;
	}

	public void setCustomerAdress(String customerAdress) {
		this.customerAdress = customerAdress;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCapacity() {
		return capacity;
	}

	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}

	public String getSerialNumberIndoor() {
		return serialNumberIndoor;
	}

	public void setSerialNumberIndoor(String serialNumberIndoor) {
		this.serialNumberIndoor = serialNumberIndoor;
	}

	public String getSerialNumberOutdoor() {
		return serialNumberOutdoor;
	}

	public void setSerialNumberOutdoor(String serialNumberOutdoor) {
		this.serialNumberOutdoor = serialNumberOutdoor;
	}

	public String getNo_name() {
		return no_name;
	}

	public void setNo_name(String no_name) {
		this.no_name = no_name;
	}

	public String getInstNo() {
		return instNo;
	}

	public void setInstNo(String instNo) {
		this.instNo = instNo;
	}

	public String getIntalledBy() {
		return intalledBy;
	}

	public void setIntalledBy(String intalledBy) {
		this.intalledBy = intalledBy;
	}

	public String getEmpty() {
		return empty;
	}

	public void setEmpty(String empty) {
		this.empty = empty;
	}

	public String getServiceTeam() {
		return serviceTeam;
	}

	public void setServiceTeam(String serviceTeam) {
		this.serviceTeam = serviceTeam;
	}

	public String getFirstFree() {
		return firstFree;
	}

	public void setFirstFree(String firstFree) {
		this.firstFree = firstFree;
	}

	public String getSecondFree() {
		return secondFree;
	}

	public void setSecondFree(String secondFree) {
		this.secondFree = secondFree;
	}

	public String getThirdFree() {
		return thirdFree;
	}

	public void setThirdFree(String thirdFree) {
		this.thirdFree = thirdFree;
	}
}
