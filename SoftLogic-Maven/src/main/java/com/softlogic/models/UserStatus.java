package com.softlogic.models;

public enum UserStatus 
{
	ACTIVE,
	INACTIVE;
}
