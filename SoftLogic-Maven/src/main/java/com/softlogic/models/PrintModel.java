package com.softlogic.models;

public class PrintModel 
{
	int serviceNumber;
	AC_Customer ac_Customer;
	
	public int getServiceNumber() {
		return serviceNumber;
	}

	public void setServiceNumber(int serviceNumber) {
		this.serviceNumber = serviceNumber;
	}

	public AC_Customer getAc_Customer() {
		return ac_Customer;
	}

	public void setAc_Customer(AC_Customer ac_Customer) {
		this.ac_Customer = ac_Customer;
	}
}
