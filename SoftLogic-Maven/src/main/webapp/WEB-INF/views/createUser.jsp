<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>Create User</title>
	<link rel="shortcut icon" href="static/img/logoicon.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="static/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="static/DataTables-1.10.8/media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="static/datepicker/datepicker.css"> 
	<link rel="stylesheet" type="text/css" href="static/css/vcl_styles.css">
	<script src="static/js/jquery-1.11.3.min.js"></script>
	<script src="static/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="static/js/bootstrap-filestyle.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">      
      <img alt="Brand" src="static/img/imgres.jpg">
      </a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="showData">Home </a></li>
        <li><a href="insertExcel">Insert Excel</a></li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insert Customer <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="insertPage">AMC Customer</a></li>
            <li><a href="insertFreeServiceDataPage">Free Customer</a></li>
          </ul>
        </li>
        <sec:authorize access="hasAuthority('Admin')">
        	<li class="active dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage Users <span class="caret"></span></a>
          		<ul class="dropdown-menu">
            		<li><a href="AllUsers">All Users</a></li>
            		<li><a href="createUserPage">Create User</a></li>
          		</ul>
        	</li>
		</sec:authorize>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout" onclick="logOut(); return false;">Log out</a></li>

      </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a><sec:authentication property="name"/> <sec:authentication property="authorities"/>  <span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
	</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="vcl_createUserContainer">
    <div class="panel panel-default ExtraPadding30px shadow">
            <h3 class="form-group">Create User</h3>

        <form action="createUser" id="use_create" class="form-horizontal" role="form">
            <div class="form-group">
                <div class="col-sm-3">
                    User Name
                </div>
                <div class="col-sm-9">
                    <input name="username" type="text" class="form-control" id="username" value="${userName}" placeholder="User Name" maxlength="45">
					<span id="usernameVali"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-3">
                    Password
                </div>
                <div class="col-sm-9">
                    <input name="password" type="password" value="${passWord}" class="form-control" id="password" placeholder="Password" maxlength="45">
					<span id="pwVali"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    Confirm Password
                </div>
                <div class="col-sm-9">
                    <input type="password" value="${passWord}" class="form-control" id="password_conf" placeholder="Confirm Password" maxlength="45">
					<span id="pwConfVali"><c:if test="${errorMessage != null}"> <h5 style="color:red;"><c:out value="${errorMessage}"/></h5></c:if></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-3">
                    User Type
                </div>
                <div class="col-sm-9">
                    <select id="filtertype" name="userType" class="form-control">
	                    <option value="0"<c:out value="${userTypetoSelectedArray[0]}"/>>User</option>
	                    <option value="1"<c:out value="${userTypetoSelectedArray[1]}"/>>Approver</option>
	                    <option value="2"<c:out value="${userTypetoSelectedArray[2]}"/>>Admin</option>
                	</select>
					<span id="pwConfVali" class="text-danger"></span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
$(document).ready(function(){
	
	$('#use_create').submit(function (event) {			
		//alert ("er");
		var valiFlag = 0;
		var inp = $("#username");
		var usernName = inp.val();

		if( usernName == "" )
		{
			valiFlag = 1;
			$('#usernameVali').html('<h5 style="color:red;">Please enter the User Name</h5>');
		}
		else
		{
			$('#usernameVali').html("");
		}

		var inp = $("#password");
		var password = inp.val();

		if( password == "" )
		{
			valiFlag = 1;
			$('#pwVali').html('<h5 style="color:red;">Please enter the Password</h5>');
		}
		else
		{
			$('#pwVali').html("");
		}
		
		var inp = $("#password_conf");
		var passwordConf = inp.val();

		if( passwordConf == "" )
		{
			valiFlag = 1;
			$('#pwConfVali').html('<h5 style="color:red;">Please Confirm the Password</h5>');
		}
		else if( passwordConf != password )
		{
			valiFlag = 1;
			$('#pwConfVali').html('<h5 style="color:red;">Confirm Password Does not match</h5>');
		}
		else
		{
			$('#pwConfVali').html("");
		}


		if (valiFlag == 0) 
		{
			return;
		}
		else
		{
			event.preventDefault();	
		}
	});
});
</script>
<script>
	function logOut()
	{
		document.getElementById('logOutBtn').click();
	}
</script>
<form:form action="${pageContext.request.contextPath}/logout" method="POST">
    <input id="logOutBtn" type="submit" value="Logout" />
</form:form>
</body>
</html>