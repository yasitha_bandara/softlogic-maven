<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>Enter AMC Customer</title>
	<link rel="shortcut icon" href="static/img/logoicon.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 
    <link href="static/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="static/datepicker/datepicker.css"> 
    <link rel="stylesheet" type="text/css" media="screen" href="static/datetimepicker/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="static/css/vcl_styles.css">
    
    <script src="static/js/jquery-1.11.3.min.js"></script>
	<script src="static/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">      
      <img alt="Brand" src="static/img/imgres.jpg">
      </a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="showData">Home </a></li>
        <li><a href="insertExcel">Insert Excel</a></li>
		<li class="active dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insert Customer <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="insertPage">AMC Customer</a></li>
            <li><a href="insertFreeServiceDataPage">Free Customer</a></li>
          </ul>
        </li>
        <sec:authorize access="hasAuthority('Admin')">
        	<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage Users <span class="caret"></span></a>
          		<ul class="dropdown-menu">
            		<li><a href="AllUsers">All Users</a></li>
            		<li><a href="createUserPage">Create User</a></li>
          		</ul>
        	</li>
		</sec:authorize>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout" onclick="logOut(); return false;">Log out</a></li>

      </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a><sec:authentication property="name"/> <sec:authentication property="authorities"/>  <span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
	</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="vcl_container">
<h2>Enter AMC Customer Information</h2>
<form action="insertByApprover?${_csrf.parameterName}=${_csrf.token}" method="post">
<input type="hidden" name="pk"value="" size="30" />
<table id="form_table" border="0" cellpadding="2">
<tbody>
<tr>
<td></td>
<td><input id="month" class="form-control" type="hidden" name="month" value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Agreement Date</td>
<td><input id="installationDate" class="form-control" type="text" name="installationDate"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Job No</td>
<td><input class="form-control" type="text" name="jobNo"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Customer Name</td>
<td><input class="form-control" type="text" name="customerName"value="" size="30" maxlength="250"/></td>
<td></td></tr>
<tr>
<td>Customer Adress</td>
<td><input class="form-control" type="text" name="customerAdress"value="" size="30" maxlength="250"/></td>
<td></td></tr>
<tr>
<td>City</td>
<td><input class="form-control" type="text" name="city"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Contact person</td>
<td><input class="form-control" type="text" name="contact"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Contact No</td>
<td><input class="form-control" type="text" name="contactNo"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Type</td>
<td><input class="form-control" type="text" name="type"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Units</td>
<td><input class="form-control" type="text" name="unit"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<td>Capacity</td>
<td><input class="form-control" type="text" name="capacity"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Serial Number Indoor</td>
<td><input class="form-control" type="text" name="serialNumberIndoor"value="" size="30" maxlength="250"/></td>
<td></td></tr>
<tr>
<td>Serial Number Outdoor</td>
<td><input class="form-control" type="text" name="serialNumberOutdoor"value="" size="30" maxlength="250"/></td>
<td></td></tr>
<td>Sales Location</td>
<td><input class="form-control" type="text" name="no_name"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Invoice No</td>
<td><input class="form-control" type="text" name="instNo"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Intalled By</td>
<td><input  class="form-control" type="text" name="intalledBy"value="" size="30" maxlength="45"/></td>
<td></td></tr>
<tr>
<td>Service Team</td>
<td><input  class="form-control" type="text" name="serviceTeam"value="" size="30" maxlength="45"/></td>
<td>
</td>
</tr>
<tr>
<td>Service Frequency</td>
<td>
<select name="frequency">
  <option value="2">2</option>
  <option value="3">3</option>
  <option value="4">4</option>
  <option value="6">6</option>
</select>
</td>
<td>
</td>
</tr>
<tr>
	<td>Comments</td>
	<td><textarea class="form-control" rows="3" name='comments' id='comments' maxlength="250"></textarea></td>
	<td></td>
</tr>
<tr>
<td></td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-primary pull-right"type="submit" value="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" name="submit"/></td>
</tr>
</tbody>
</table>
</form>
</div>
<script src="static/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#month').datepicker({
            format: "dd-M-yyyy",
            autoclose:true
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#installationDate').datepicker({
            format: "dd-M-yyyy",
            autoclose:true
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#firstFree').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#secondFree').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#thirdFree').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#forthService').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#fifthService').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#sixthService').datepicker({
            format: "dd/mm/yyyy"
        });

    });
</script>
<script>
	function logOut()
	{
		document.getElementById('logOutBtn').click();
	}
</script>
<form:form action="${pageContext.request.contextPath}/logout" method="POST">
    <input id="logOutBtn" type="submit" value="Logout" />
</form:form>
</body>
</html>