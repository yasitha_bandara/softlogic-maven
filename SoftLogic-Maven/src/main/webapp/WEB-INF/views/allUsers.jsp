<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>All Users</title>
	<link rel="shortcut icon" href="static/img/logoicon.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="static/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="static/DataTables-1.10.8/media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="static/datepicker/datepicker.css"> 
	<link rel="stylesheet" type="text/css" href="static/css/vcl_styles.css">
	<script src="static/js/jquery-1.11.3.min.js"></script>
	<script src="static/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="static/js/bootstrap-filestyle.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">      
      <img alt="Brand" src="static/img/imgres.jpg">
      </a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class=""><a href="showData">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="insertExcel">Import Excel</a></li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insert Customer <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="insertPage">AMC Customer</a></li>
            <li><a href="insertFreeServiceDataPage">Free Customer</a></li>
          </ul>
        </li>
        <sec:authorize access="hasAuthority('Admin')">
        	<li class="active dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage Users <span class="caret"></span></a>
          		<ul class="dropdown-menu">
            		<li><a href="AllUsers">All Users</a></li>
            		<li><a href="createUserPage">Create User</a></li>
          		</ul>
        	</li>
		</sec:authorize>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout" onclick="logOut(); return false;">Log out</a></li>
      </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a><sec:authentication property="name"/> <sec:authentication property="authorities"/>  <span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
	</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="vcl_webContainer980px">


	</br>
    <div class="panel panel-default">
    	<div class="panel-body">
			<h3 id="header">All Users</h2>
				<div class="col-xs-12">
    				<table id="table_id" class="table table-bordered table-hover display nonSorting">
						<thead>
							<tr>	
								<th>User Name</th>	
								<th>User Type</th>
								<th></th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${allUsers}" var="user">
							<tr>
								<td>${user.userName}</td>
								<td>${user.role}</td>
								<td><a href='editUserPage?user_id=${user.id}'> <button type="button" class="btn btn-primary btn-sm">Edit User</button></a></td>
								<td><a href='changePasswordPage?user_id=${user.id}'> <button type="button" class="btn btn-warning btn-sm">Change Password</button></a></td>
								<td><a href='deleteUser?user_id=${user.id}' onclick="getConfirmation()"> <button type="button" class="btn btn-danger btn-sm">Delete</button></a></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			
		</div>
	</div>
	</form>
	</br>
	<!--<a href="downloadExcel"><button type="button" class="btn btn-success btn-default">Download Excel</button></a>-->
</div>
	
	<!--https://datatables.net/reference/option/dom#-->
	<script type="text/javascript">
		$(document).ready(function() {
		
	  		$('#table_id').DataTable({
	  		  "dom": '<f<"table-responsive"t>ip>',
	  		"columnDefs": [ {
	  			"targets": 2,
	  			"orderable": false
	  			
	  			} ],
	  		"ordering": false
	  		  //"dom": '<lf<"table-responsive"t>ip>'
	  		});   
  		});
	</script>
	<script type="text/javascript">
		/*function onSubmit() 
		{
			var filepath = document.getElementById('file').value;
			//alert(filename);
			filepath = filepath.split('\\');
			document.getElementById("fileName").value = filepath[2];
		}*/
	</script>
	<script type="text/javascript">
	//Did not understand the coad (this script) just copy past from-http://stackoverflow.com/questions/4234589/validation-of-file-extension-before-uploading-file
	
	//var _validFileExtensions = [".xls", ".xlsx"]; 
	var _validFileExtensions = [".xlsx"]; 
		function Validate(oForm) 
		{
			if( document.getElementById("file").files.length != 0 )
			{
		    	var arrInputs = oForm.getElementsByTagName("input");
		    	for (var i = 0; i < arrInputs.length; i++) 
		    	{
		        	var oInput = arrInputs[i];
		        	if (oInput.type == "file") 
		        	{
		            	var sFileName = oInput.value;
		            	if (sFileName.length > 0) 
		            	{
		                	var blnValid = false;
		                	for (var j = 0; j < _validFileExtensions.length; j++) 
		                	{
		                    	var sCurExtension = _validFileExtensions[j];
		                    	if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) 
		                    	{
		                        	blnValid = true;
		                        	break;
		                    	}
		                	}
		                	if (!blnValid) 
		                	{
		                    	alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
		                    	return false;
		                	}
		            	}
		        	}
		    	}
		  
		    	return true;
			}
			else
			{
				 alert("no files selected");
		         return false;
			}
		}

	</script>
	<script type="text/javascript">
	var checkId=[
	             	<c:forEach items="${showData}" var="customer" varStatus="status">
		             	"ckeck${customer.customer_pk}"
		             	<c:if test="${!status.last}">    
		             		,    
		             	</c:if>
	             	</c:forEach>
	            ];
	
	
	
	$('#pdfGenerate').click(function(){

	var checkedList=[];
	//var j=0;
	for ( var i = 0; i < checkId.length; i++)
	{
		if ( $('#'+checkId[i]).length )
		{
			if (document.getElementById(checkId[i]).checked)
			{
				checkedList.push(document.getElementById(checkId[i]).value); 
				//j=j+1;
			}
		}
	}
	var filterInfo=[];
	filterInfo.push(document.getElementById("serviceturn").value);
	filterInfo.push(document.getElementById("time").value);
	
	var url = 'http://localhost:8080/SoftLogic-Maven/pdfGenerate';
	$.ajax({
		  url: url,
		  type: 'POST',
		  data: "checkedList="+checkedList+"&filterInfo="+filterInfo
		  })
		  .done(function(data) {
		    console.log("success");
		    if (data === undefined)
		    {
		    	
		    }
		    else
		    {
		    	document.getElementById('downloadPDF').click();
		    }
		  }); 
	//alert(checkedList[0]);
	});
	$('#clearAll').click(function()
	{
		for (var i = 0; i < checkId.length; i++)
		{
			if ( $('#'+checkId[i]).length )
			{
				document.getElementById(checkId[i]).checked= false;

			}
		}
	});
	$('#selectAll').click(function()
			{
				for (var i = 0; i < checkId.length; i++)
				{
					if ( $('#'+checkId[i]).length )
					{
						document.getElementById(checkId[i]).checked= true;

					}
				}
			});
	</script>
<script src="static/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#from').datepicker({
            format: "yyyy-mm-dd"
        });

    });
    $(document).ready(function () {

        $('#to').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>
<script type="text/javascript">
$('#filterbtn').click(function(){
	
	var DateValiFlag = 0;
	
	var inp = $("#from");
	var fromDate_value = inp.val();
	if(fromDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#fromDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#fromDateVali').html("");
		var arr = fromDate_value.split('-');


		var fromDays = (parseInt((arr[2]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[0]);
		//alert (fromHourse_mins);
		//alert(fromDays);

	}

	var inp = $("#to");
	var toDate_value = inp.val();
	if(toDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#toDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#toDateVali').html("");
		var arr = toDate_value.split('-');


		var toDays = (parseInt((arr[2]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[0]);
		//alert (fromHourse_mins);
		//alert(toDays);

	}
	if(DateValiFlag==0)
	{//alert ("awa yakoo");
		if(toDays <= fromDays)
		{
			//valiFlag = 1;
			DateValiFlag=1;
			$('#fromDateVali').html("<p><code><small>invalid Duration</small></code></p>");
			$('#toDateVali').html("<p><code><small>invalid Duration</small></code></p>");
		}
	}
	if(DateValiFlag==1)
	{
		return false;
	}
	else
	{
		return true;
	}

});
</script>
	<script>
	/*$('#filterbtn').click(function()
	{
	var data = $('filterForm').serialize();
    var url = 'http://localhost:8080/SoftLogic-Maven/filter'; // the script where you handle the form input.
    alert("at the end");
    $.ajax({
           type: 'POST',
           url: url,
           data:data, // serializes the form's elements.
           success: function(data)
           {
               alert(data); // show response from the php script.
           }
         });
	});*/
	
	</script>
<script>
	function logOut()
	{
		document.getElementById('logOutBtn').click();
	}
</script>


<form:form action="${pageContext.request.contextPath}/logout" method="POST">
    <input id="logOutBtn" type="submit" value="Logout" />
</form:form>
<script>
function getConfirmation() 
{
    if (confirm("User will be deleted!") == true) 
    {
		return true;
    } 
    else 
    {
    	event.preventDefault();
    	return false;
    }
}
</script>
</body>
</html>