<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<title>Edit Customer</title>
	<link rel="shortcut icon" href="static/img/logoicon.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    
    <link href="static/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="static/datepicker/datepicker.css"> 
    <link rel="stylesheet" type="text/css" media="screen" href="static/datetimepicker/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="static/css/vcl_styles.css">
    
    <script src="static/js/jquery-1.11.3.min.js"></script>
	<script src="static/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">      
      <img alt="Brand" src="static/img/imgres.jpg">
      </a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="showData">Home </span></a></li>
        <li><a href="insertExcel">Import Excel </a></li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insert Customer <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="insertPage">AMC Customer</a></li>
            <li><a href="insertFreeServiceDataPage">Free Customer</a></li>
          </ul>
        </li>
        <sec:authorize access="hasAuthority('Admin')">
        	<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage Users <span class="caret"></span></a>
          		<ul class="dropdown-menu">
            		<li><a href="AllUsers">All Users</a></li>
            		<li><a href="createUserPage">Create User</a></li>
          		</ul>
        	</li>
		</sec:authorize>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout" onclick="logOut(); return false;">Log out</a></li>

      </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a><sec:authentication property="name"/> <sec:authentication property="authorities"/>  <span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
	</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="vcl_container">
<h1>Change Customer Information</h1>
<form action="editCustomer?${_csrf.parameterName}=${_csrf.token}" method="post">
<input type="hidden" name="pk"value="${customerToEdit.customer_pk}" size="30" />
<table id="form_table" border="0" cellpadding="2">
<tbody>
<tr>
<td>&nbsp;&nbsp;<input type="radio" name="serviceType" value="amc" 								
								<c:choose>
									<c:when test="${customerToEdit.amc == '1'}">    
		             					checked   
		             				</c:when>
  								</c:choose>	
  								> &nbsp;&nbsp;AMC Service<br></td>
<td>&nbsp;&nbsp;<input type="radio" name="serviceType" value="free" 
								<c:choose>
		             				<c:when test="${customerToEdit.amc == '0'}">    
		             					checked    
		             				</c:when>
  								</c:choose>
								> &nbsp;&nbsp;Free Service<br></td>
</tr>
<tr>
<td>   </td>
<td><input id="month" class="form-control" type="hidden" name="month" value="${customerToEdit.month}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Agreement/Installation Date</td>
<td><input id="installationDate" class="form-control" type="text" name="installationDate"value="${customerToEdit.installationDate}" size="30" maxlength="45" maxlength="45"/></td>
</tr>
<tr>
<td>Job No</td>
<td><input class="form-control" type="text" name="jobNo"value="${customerToEdit.jobNo}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Customer Name</td>
<td><input class="form-control" type="text" name="customerName"value="${customerToEdit.customerName}" size="30" maxlength="250"/></td>
</tr>
<tr>
<td>Customer Adress</td>
<td><input class="form-control" type="text" name="customerAdress"value="${customerToEdit.customerAdress}" size="30" maxlength="250"/></td>
</tr>
<tr>
<td>City</td>
<td><input class="form-control" type="text" name="city"value="${customerToEdit.city}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Contact person</td>
<td><input class="form-control" type="text" name="contact"value="${customerToEdit.contact}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Contact No</td>
<td><input class="form-control" type="text" name="contactNo"value="${customerToEdit.contactNo}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Type</td>
<td><input class="form-control" type="text" name="type"value="${customerToEdit.type}" size="30" maxlength="45"/></td>
<tr>
<td>Units</td>
<td><input class="form-control" type="text" name="unit"value="${customerToEdit.unit}" size="30" maxlength="45"/></td>
</tr>
<td>Capacity</td>
<td><input class="form-control" type="text" name="capacity"value="${customerToEdit.capacity}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Serial Number Indoor</td>
<td><input class="form-control" type="text" name="serialNumberIndoor"value="${customerToEdit.serialNumberIndoor}" size="30" maxlength="250"/></td>
</tr>
<tr>
<td>Serial Number Outdoor</td>
<td><input class="form-control" type="text" name="serialNumberOutdoor"value="${customerToEdit.serialNumberOutdoor}" size="30" maxlength="250"/></td>
</tr>
<td>Sales Location</td>
<td><input class="form-control" type="text" name="no_name"value="${customerToEdit.no_name}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Invoice No</td>
<td><input class="form-control" type="text" name="instNo"value="${customerToEdit.instNo}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>Installed By</td>
<td><input class="form-control" type="text" name="intalledBy"value="${customerToEdit.intalledBy}" size="30" maxlength="45"/></td>
</tr>
<tr>
<tr>
<td>Service Team</td>
<td><input class="form-control" type="text" name="serviceTeam"value="${customerToEdit.serviceTeam}" size="30" maxlength="45"/></td>
</tr>
<tr>
<td>1st Service</td>
<td>
	<input id="firstFree" class="form-control" type="text" name="firstFree"value="${customerToEdit.firstFree}" size="30" maxlength="45"/>
	
</td>
<td>
<sec:authorize access="hasAnyAuthority('Admin', 'Approver')">
	<c:if test="${customerToEdit.firstFreeStatus == 1}">
	 <c:set var="checked1" value="checked"/>
	</c:if>
	<c:if test="${customerToEdit.firstFreeStatus == 0}">
	 <c:set var="checked1" value=""/>
	</c:if>
	<input type="hidden" name="firstFreeStatus" value="checked">
	&nbsp;&nbsp;<input type="checkbox" name="firstFreeStatus" <c:out value="${checked1}"/>>&nbsp;&nbsp;
</sec:authorize>	
</td>	
</tr>
<tr>
<td>2nd Service</td>
<td><input id="secondFree" class="form-control" type="text" name="secondFree"value="${customerToEdit.secondFree}" size="30" maxlength="45"/>
</td>
<td>
<sec:authorize access="hasAnyAuthority('Admin', 'Approver')">
	<c:if test="${customerToEdit.secondFreeStatus == 1}">
	 <c:set var="checked2" value="checked"/>
	</c:if>
	<c:if test="${customerToEdit.secondFreeStatus == 0}">
	 <c:set var="checked2" value=""/>
	</c:if>
	<input type="hidden" name="secondFreeStatus" value="checked">
&nbsp;&nbsp;<input type="checkbox" name="secondFreeStatus" <c:out value="${checked2}"/>>&nbsp;&nbsp;
</sec:authorize>
</td>

</tr>
<tr>
<td>3rd Service</td>
<td>
<input id="thirdFree" class="form-control" type="text" name="thirdFree"value="${customerToEdit.thirdFree}" size="30" maxlength="45"/>

</td>
<td>
<sec:authorize access="hasAnyAuthority('Admin', 'Approver')">
	<c:if test="${customerToEdit.thirdFreeStatus == 1}">
	 <c:set var="checked3" value="checked"/>
	</c:if>
	<c:if test="${customerToEdit.thirdFreeStatus == 0}">
	 <c:set var="checked3" value=""/>
	</c:if>
	<input type="hidden" name="thirdFreeStatus" value="checked">
&nbsp;&nbsp;<input type="checkbox" name="thirdFreeStatus" <c:out value="${checked3}"/>>&nbsp;&nbsp;
</sec:authorize>
</td>
</tr>

<tr>
<td>4th Service</td>
<td>
<input id="forthService" class="form-control" type="text" name="forthService"value="${customerToEdit.forth_Service}" size="30" maxlength="45"/>
</td>
<td>
<sec:authorize access="hasAnyAuthority('Admin', 'Approver')">
	<c:if test="${customerToEdit.forthServiceStatus == 1}">
	 <c:set var="checked3" value="checked"/>
	</c:if>
	<c:if test="${customerToEdit.forthServiceStatus == 0}">
	 <c:set var="checked3" value=""/>
	</c:if>

	<input type="hidden" name="forthServiceStatus" value="checked">
&nbsp;&nbsp;<input type="checkbox" name="forthServiceStatus" >&nbsp;&nbsp;
</sec:authorize>
</td>
</tr>
<tr>
<td>5th Service</td>
<td>
<input id="fifthService" class="form-control" type="text" name="fifthService"value="${customerToEdit.fifth_Service}" size="30" maxlength="45"/>


</td>
<td>
<sec:authorize access="hasAnyAuthority('Admin', 'Approver')">	
	<c:if test="${customerToEdit.fifthServiceStatus == 1}">
	 <c:set var="checked3" value="checked"/>
	</c:if>
	<c:if test="${customerToEdit.fifthServiceStatus == 0}">
	 <c:set var="checked3" value=""/>
	</c:if>
<input type="hidden" name="fifthServicetatus" value="checked">
&nbsp;&nbsp;<input type="checkbox" name="fifthServicetatus" >&nbsp;&nbsp;
</sec:authorize>
</td>
</tr>
<tr>
<td>6th Service</td>
<td>
<input id="sixthService" class="form-control" type="text" name="sixthService"value="${customerToEdit.sixth_Service}" size="30" maxlength="45"/>


</td>
<td>
<sec:authorize access="hasAnyAuthority('Admin', 'Approver')">	
	<c:if test="${customerToEdit.sixthServiceStatus == 1}">
	 <c:set var="checked3" value="checked"/>
	</c:if>
	<c:if test="${customerToEdit.sixthServiceStatus == 0}">
	 <c:set var="checked3" value=""/>
	</c:if>
<input type="hidden" name="sixthServicetatus" value="checked">
&nbsp;&nbsp;<input type="checkbox" name="sixthServicetatus" >&nbsp;&nbsp;
</sec:authorize>
</td>

</tr>
<tr>
	<td>Comments</td>
	<td><textarea class="form-control" rows="3" name='comments' id='comments' maxlength="250"></textarea></td>
	<td></td>
</tr>
<tr>
<td></td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-primary pull-right"type="submit" value="&nbsp;&nbsp;&nbsp;Save&nbsp;&nbsp;&nbsp;" name="submit"/></td>
</tr>
</tbody>
</table>

</form>
</div>
<script src="static/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#month').datepicker({
            format: "dd-M-yyyy"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#installationDate').datepicker({
            format: "dd-M-yyyy"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#firstFree').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#secondFree').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#thirdFree').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#forthService').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#fifthService').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#sixthService').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>
<script>
	function logOut()
	{
		document.getElementById('logOutBtn').click();
	}
</script>
<form:form action="${pageContext.request.contextPath}/logout" method="POST">
    <input id="logOutBtn" type="submit" value="Logout" />
</form:form>
</body>
</html>
