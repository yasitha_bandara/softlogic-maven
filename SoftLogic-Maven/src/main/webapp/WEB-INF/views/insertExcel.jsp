<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
		<meta charset="UTF-8">
		<title>Import Excel</title>
		<link rel="shortcut icon" href="static/img/logoicon.jpg">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="static/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="static/DataTables-1.10.8/media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="static/datepicker/datepicker.css"> 
	<link rel="stylesheet" type="text/css" href="static/css/vcl_styles.css">
	<script src="static/js/jquery-1.11.3.min.js"></script>
	<script src="static/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="static/js/bootstrap-filestyle.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">      
      <img alt="Brand" src="static/img/imgres.jpg">
      </a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="showData">Home </a></li>
        <li class="active"><a href="insertExcel">Import Excel <span class="sr-only">(current)</span></a></li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insert Customer <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="insertPage">AMC Customer</a></li>
            <li><a href="insertFreeServiceDataPage">Free Customer</a></li>
          </ul>
        </li>
        <sec:authorize access="hasAuthority('Admin')">
        	<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage Users <span class="caret"></span></a>
          		<ul class="dropdown-menu">
            		<li><a href="AllUsers">All Users</a></li>
            		<li><a href="createUserPage">Create User</a></li>
          		</ul>
        	</li>
		</sec:authorize>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout" onclick="logOut(); return false;">Log out</a></li>

      </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a><sec:authentication property="name"/> <sec:authentication property="authorities"/>  <span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
	</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="vcl_container">
	</br>
	</br>
	<form id="form_table"action="readexcel" method="post"  enctype="multipart/form-data" onsubmit="return Validate(this);">
		<input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
		<input id="fileName" type="hidden" name="fileName" value="" size="30" />
		
		<table>
			<tr>
				<td>
					<input id="file" type="file" name="file" value="" size="45" class="filestyle" />
					<input id="file" type="hidden" name="insertExcelFormId" value="<c:out value="${insertExcelFormId}"/>" size="45" class="filestyle" />
				</td>
			</tr>
			 <tr> 
			 	<td>
			 		&nbsp;&nbsp;<input type="radio" name="serviceType" value="amc" <c:out value="${serviceTypeArray[0]}"/>> &nbsp;&nbsp;AMC Service<br>
  				</td>
  			</tr>
  			<tr> 
			 	<td>
					&nbsp;&nbsp;<input type="radio" name="serviceType" value="free" <c:out value="${serviceTypeArray[1]}"/>> &nbsp;&nbsp;Free Service<br>
  				</td>
  			</tr>
			<tr>
				<td>
					<button  type="submit" class="btn btn-info">Read from Excel</button>
				</td>
			</tr>
		</table>
	</form>
	<button id="pdfGenerate"class="btn btn-danger" >Delete All</button>
	<!--<button class="btn btn-danger" onclick="pdfGenerate(); return false;">Delete All</button>-->
<!--canot display the full path:https://stackoverflow.com/questions/15201071/how-to-get-full-path-of-selected-file-on-change-of-input-type-file-using-jav -->
<!--
<div class="jumbotron">
        <div class="form-group col-lg-12">
        <h4>Filter by service due date</h4>
        </div>
<form id="filterForm" action="filter" method="post" >
            <div class="form-group col-lg-3 control-label filterform-groups">
                
                    <div class="input-group">
                        <span class="input-group-addon">from</span>
                        <input id="from" class="form-control" type="text" name="from" value="" size="30" placeholder="dd-mm-yyyy"/>

                </div>
                <span id="fromDateVali" class="text-danger"></span>
            </div>
            <div id="filterform-group1" class="form-group col-lg-3 control-label filterform-groups">
                
                    <div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input id="to" class="form-control" type="text" name="to" value="" size="30" placeholder="dd-mm-yyyy"/>
                </div>
                <span id="toDateVali" class="text-danger"></span>
            </div>
<button id="filterbtn"type="submit" class="btn btn-default">Filter</button>
</form>
</div>
-->
<form id="checkboxForm">
</br>
<!--canot display the full path:https://stackoverflow.com/questions/15201071/how-to-get-full-path-of-selected-file-on-change-of-input-type-file-using-jav -->
<!--<button type="button" class="btn btn-info" id="pdfGenerate">PDF Generate</button>
	<a href="downloadPDF" id="downloadPDF"></a>
	</br>-->
<!--<a href="genarateExcel"><button type="button" class="btn btn-default">Generate Excel</button></a> -->

	</br>
    <div class="panel panel-default">
    	<div class="panel-body">
			<h3 id="header">${fileName}</h2>
			
    			<div class="col-xs-12 table-responsive table-height">
    			
					<table id="table_id" class="table table-bordered table-hover display nonSorting">
						<thead>
							<tr>
								<!--<th></th>
								<th></th>-->
								<th style="display:none;">			
									
										<button type="button" class="btn btn-default" id="selectAll"><span class="glyphicon glyphicon-check"></button>
										<button type="button" class="btn btn-default" id="clearAll"><span class="glyphicon glyphicon-unchecked"></button>
								
								</th>
								<!--<th>&nbsp;MONTH&nbsp;</th>-->
								<th>
								<c:choose>
									<c:when test="${serviceType == 'amc'}">    
		             					Agreement Date    
		             				</c:when>
		             				<c:when test="${serviceType == 'free'}">    
		             					Installation Date    
		             				</c:when>
		             				<c:otherwise>
    									Installation Date
  									</c:otherwise>
  								</c:choose>	
								</th>				
								<th>Job No</th>
								<th>Customer&nbsp;&nbsp;Name</th>
								<th>Customer Adress</th>
								<th>City</th>
								<th>Contact</th>
								<th>Contact No</th>
								<th>Type</th>
								<th>Units</th>
								<th>apacity</th>
								<th>Serial Number Indoor</th>
								<th>Serial Number Outdoor</th>
								<th>Sales Location</th>
								<th>Invoice No</th>
								<th>Installed By</th>
								<th>Frequency</th>
								<th>Service Team</th>
								<th>1st Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>2nd Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>3rd Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>4th Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>5th Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>6th Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${showData}" var="customer">
							<tr	
								<c:choose>
									<c:when test="${customer.readError == '1'}">    
		             					bgcolor="#d65d5d"   
		             				</c:when>
								</c:choose>
							>
								<!-- <td><a href='editpage?customer_pk=${customer.customer_pk}'> <button type="button" class="btn btn-default">Edit</button></a></td>
								<td><a href='delete?customer_pk=${customer.customer_pk}'> <button type="button" class="btn btn-default">Delete</button></a></td>-->
								<td style="display:none;"> <input type="checkbox" name=${customer.customer_pk} value=${customer.customer_pk} id="ckeck${customer.customer_pk}" checked></td>
								<!--<td>${customer.month}</td>-->
								<!--<td>
								<c:choose>
									<c:when test="${serviceType == 'amc'}">    
		             					${customer.month}    
		             				</c:when>
		             				<c:when test="${serviceType == 'free'}">    
		             					${customer.installationDate}    
		             				</c:when>
  								</c:choose>	
								</td>-->
								<td>${customer.installationDate}</td>
								<td>${customer.jobNo}</td>
								<td>${customer.customerName}</td>
								<td>${customer.customerAdress}</td>
								<td>${customer.city}</td>
								<td>${customer.contact}</td>
								<td>${customer.contactNo}</td>
								<td>${customer.type}</td>
								<td>${customer.unit}</td>
								<td>${customer.capacity}</td>
								<td>${customer.serialNumberIndoor}</td>
								<td>${customer.serialNumberOutdoor}</td>
								<td>${customer.no_name}</td>
								<td>${customer.instNo}</td>
								<td>${customer.intalledBy}</td>
								<td>${customer.frequency}</td>
								<td>${customer.serviceTeam}</td>
								<td>${customer.firstFree}</td>
								<td>${customer.secondFree}</td>
								<td>${customer.thirdFree}</td>
								<td>${customer.forth_Service}</td>
								<td>${customer.fifth_Service}</td>
								<td>${customer.sixth_Service}</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			
		</div>
	</div>
	</form>
	</br>
	<!--<a href="downloadExcel"><button type="button" class="btn btn-success btn-default">Download Excel</button></a>-->
</div>
	
	<!--https://datatables.net/reference/option/dom#-->
	<script type="text/javascript">
		$(document).ready(function() {
		
	  		$('#table_i').DataTable({
	  		  "dom": '<f<"table-responsive"t>i>',
	  		"columnDefs": [ {
	  			"targets": 2,
				"orderable": false
	  			} ],
	  			"paging":   false,
	  			"ordering": false
	  		  //"dom": '<lf<"table-responsive"t>ip>'
	  		});   
  		});
	</script>
		<script type="text/javascript">
		/*function onSubmit() 
		{
			var filepath = document.getElementById('file').value;
			//alert(filename);
			filepath = filepath.split('\\');
			document.getElementById("fileName").value = filepath[2];
		}*/
	</script>
	<script type="text/javascript">
	//Did not understand the coad (this script) just copy past from-http://stackoverflow.com/questions/4234589/validation-of-file-extension-before-uploading-file
	
	//var _validFileExtensions = [".xls", ".xlsx"]; 
	var _validFileExtensions = [".xlsx"]; 
		function Validate(oForm) 
		{
			if( document.getElementById("file").files.length != 0 )
			{
		    	var arrInputs = oForm.getElementsByTagName("input");
		    	for (var i = 0; i < arrInputs.length; i++) 
		    	{
		        	var oInput = arrInputs[i];
		        	if (oInput.type == "file") 
		        	{
		            	var sFileName = oInput.value;
		            	if (sFileName.length > 0) 
		            	{
		                	var blnValid = false;
		                	for (var j = 0; j < _validFileExtensions.length; j++) 
		                	{
		                    	var sCurExtension = _validFileExtensions[j];
		                    	if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) 
		                    	{
		                        	blnValid = true;
		                        	break;
		                    	}
		                	}
		                	if (!blnValid) 
		                	{
		                    	alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
		                    	return false;
		                	}
		            	}
		        	}
		    	}
		  
		    	return true;
			}
			else
			{
				 alert("no files selected");
		         return false;
			}
		}

	</script>
	<script type="text/javascript">
	var checkId=[
	             	<c:forEach items="${showData}" var="customer" varStatus="status">
		             	"${customer.customer_pk}"
		             	<c:if test="${!status.last}">    
		             		,    
		             	</c:if>
	             	</c:forEach>
	            ];
	var server_path = window.location.protocol + window.location.host;
	
	
	$('#pdfGenerate').click(function(){
		//alert(server_path);
	var checkedList=[];
	//var j=0;
	for ( var i = 0; i < checkId.length; i++)
	{
		if ( $('#'+checkId[i]).length )
		{
			if (document.getElementById(checkId[i]).checked)
			{
				checkedList.push(document.getElementById(checkId[i]).value); 
				//j=j+1;
			}
		}
	}
	/*var filterInfo=[];
	filterInfo.push(document.getElementById("serviceturn").value);
	filterInfo.push(document.getElementById("time").value);*/
	
	var url = 'http://localhost:8080/SoftLogic-Maven/batchDelete?${_csrf.parameterName}=${_csrf.token}';
	$.ajax({
		  url: url,
		  type: 'POST',
		  data: "checkedList="+checkId
		  })
		  .done(function(data) {
		    console.log("success");
		    if (data === undefined)
		    {
		    	//alert(data);
		    }
		    else
		    {
		    	//alert(server_path);
		    	window.location.href = '/SoftLogic-Maven/insertExcel';
		    }
		  }); 
	//alert(checkedList[0]);
	});
	$('#clearAll').click(function()
	{
		for (var i = 0; i < checkId.length; i++)
		{
			if ( $('#'+checkId[i]).length )
			{
				document.getElementById(checkId[i]).checked= false;

			}
		}
	});
	$('#selectAll').click(function()
			{
				for (var i = 0; i < checkId.length; i++)
				{
					if ( $('#'+checkId[i]).length )
					{
						document.getElementById(checkId[i]).checked= true;

					}
				}
			});
	</script>
<script src="static/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#from').datepicker({
            format: "dd-mm-yyyy"
        });

    });
    $(document).ready(function () {

        $('#to').datepicker({
            format: "dd-mm-yyyy"
        });

    });
</script>
<script type="text/javascript">
$('#filterbtn').click(function(){
	
	var DateValiFlag = 0;
	
	var inp = $("#from");
	var fromDate_value = inp.val();
	if(fromDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#fromDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#fromDateVali').html("");
		var arr = fromDate_value.split('-');


		var fromDays = (parseInt((arr[2]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[0]);
		//alert (fromHourse_mins);
		//alert(fromDays);

	}

	var inp = $("#to");
	var toDate_value = inp.val();
	if(toDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#toDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#toDateVali').html("");
		var arr = toDate_value.split('-');


		var toDays = (parseInt((arr[2]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[0]);
		//alert (fromHourse_mins);
		//alert(toDays);

	}
	if(DateValiFlag==0)
	{
		if(toDays <= fromDays)
		{
			//valiFlag = 1;
			DateValiFlag==1;
			$('#fromDateVali').html("<p><code><small>invalid Duration</small></code></p>");
			$('#toDateVali').html("<p><code><small>invalid Duration</small></code></p>");
		}
	}
	if(DateValiFlag==1)
	{
		return false;
	}
	else
	{
		return true;
	}

});
</script>
	<script>
	/*$('#filterbtn').click(function()
	{
	var data = $('filterForm').serialize();
    var url = 'http://localhost:8080/SoftLogic-Maven/filter'; // the script where you handle the form input.
    alert("at the end");
    $.ajax({
           type: 'POST',
           url: url,
           data:data, // serializes the form's elements.
           success: function(data)
           {
               alert(data); // show response from the php script.
           }
         });
	});*/
	
	</script>
<script>
	function logOut()
	{
		document.getElementById('logOutBtn').click();
	}
</script>
<form:form action="${pageContext.request.contextPath}/logout" method="POST">
    <input id="logOutBtn" type="submit" value="Logout" />
</form:form>	
</body>
</html>