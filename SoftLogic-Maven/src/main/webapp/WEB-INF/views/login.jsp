<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta charset="UTF-8">
	<META HTTP-EQUIV="REFRESH" CONTENT="900">
	<title>Login</title>
	<link rel="shortcut icon" href="static/img/logoicon.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="static/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="static/DataTables-1.10.8/media/css/dataTables.bootstrap.css">
	    <link rel="stylesheet" href="static/datepicker/datepicker.css"> 
	
	<script src="static/js/jquery-1.11.3.min.js"></script>
	<script src="static/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="static/js/bootstrap-filestyle.min.js"></script>

<link rel="stylesheet" type="text/css" href="static/css/vcl_styles.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">      
      <img alt="Brand" src="static/img/imgres.jpg">
      </a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->

  </div><!-- /.container-fluid -->
</nav>

<div class="vcl_logincontainer">
    <div class="panel panel-default shadow">
    <div class="panel-body">
        <div class="form-group col-lg-10">
            <h3>User Login</h3>
        </div>
		<form id="loginform" name='f'action="login" method='POST' onsubmit="myFunction()">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />            
			<div class="form-group col-lg-12">
                <input id="username" name="username" type="text" class="form-control" placeholder="User Name">
                <span id="usernameVali" class="text-danger"></span>
            </div>
            <div class="form-group col-lg-12">
                <input id="password" name="password" type="password" class="form-control" placeholder="Password">
                <span id="pwVali"><c:if test="${param.error != null}"> <h5 style="color:red;">Invalid username or password</h5></c:if></span>
            </div>
			<div class="row"></div>
			<div class="form-group col-lg-3">
				<Button type="submit" id="btnLogin" class="btn btn-primary" />Login</button><br>
			</div>
		</form>
        <div class="row"></div>
       </div> 
    </div>
</div>
<script>
$('#loginform').submit(function (event) {
		
	var valiFlag = 0;
	var inp = $("#username");
	var usernName = inp.val();

	if (usernName == "") {
		valiFlag = 1;
		$('#usernameVali').html('<h5 style="color:red;">Please enter the User Name</h5>');
	}
	else {
		$('#usernameVali').html("");
	}

	var inp = $("#password");
	var password = inp.val();

	if (password == "") {
		valiFlag = 1;
		$('#pwVali').html('<h5 style="color:red;">Please enter the Password</h5>');
	}
	else {
		$('#pwVali').html("");
	}
	if (valiFlag == 0)
	{
		return;
	}
	else
	{
		event.preventDefault();	
	}

});
</script>
</body>

</html>