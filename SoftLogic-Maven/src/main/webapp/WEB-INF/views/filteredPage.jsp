<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>Filtered Data</title>
	<link rel="shortcut icon" href="static/img/logoicon.jpg">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	
	<link href="static/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="static/DataTables-1.10.8/media/css/dataTables.bootstrap.css">
	    <link rel="stylesheet" href="static/datepicker/datepicker.css"> 
	
	<script src="static/js/jquery-1.11.3.min.js"></script>
	<script src="static/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" charset="utf8" src="static/DataTables-1.10.8/media/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="static/js/bootstrap-filestyle.min.js"></script>

<link rel="stylesheet" type="text/css" href="static/css/vcl_styles.css">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand" href="#">      
      <img alt="Brand" src="static/img/imgres.jpg">
      </a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="showData">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="insertExcel">Import Excel</a></li>
		<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Insert Customer <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="insertPage">AMC Customer</a></li>
            <li><a href="insertFreeServiceDataPage">Free Customer</a></li>
          </ul>
        </li>
        <sec:authorize access="hasAuthority('Admin')">
        	<li class="dropdown">
          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage Users <span class="caret"></span></a>
          		<ul class="dropdown-menu">
            		<li><a href="AllUsers">All Users</a></li>
            		<li><a href="createUserPage">Create User</a></li>
          		</ul>
        	</li>
		</sec:authorize>
      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li><a href="logout" onclick="logOut(); return false;">Log out</a></li>

      </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a><sec:authentication property="name"/> <sec:authentication property="authorities"/>  <span class="glyphicon glyphicon-user" aria-hidden="true"></span></a></li>
	</ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="vcl_container">
<!--	</br>
	</br>
	<form action="readexcel" method="post"  enctype="multipart/form-data" onsubmit="return Validate(this);">
		<input id="fileName" type="hidden" name="fileName" value="" size="30" />
		<table>
			<tr>
				<td>
					<input id="file" type="file" name="file" value="" size="30" class="filestyle" />
				</td>
			</tr>
			<tr>
				<td><input class="form-control" type="submit" value="Read from Excel" name="submit" /></td>
			</tr>
		</table>
	</form>-->
<!--canot display the full path:https://stackoverflow.com/questions/15201071/how-to-get-full-path-of-selected-file-on-change-of-input-type-file-using-jav -->
<!--
<form id="filterForm" action="filter" method="post" >
<select id="serviceturn" name="serviceturn">
  <option value="0" <c:out value="${serviceSelected[0]}"/> >1ST FREE</option>
  <option value="1" <c:out value="${serviceSelected[1]}"/> >2ND FREE</option>
  <option value="2" <c:out value="${serviceSelected[2]}"/> >3RD FREE</option>
</select>
<select id="time" name="time">
  <option value="0"<c:out value="${timeSelected[0]}"/>>1week</option>
  <option value="1"<c:out value="${timeSelected[1]}"/>>2weeks</option>
  <option value="2"<c:out value="${timeSelected[2]}"/>>3weeks</option>
  <option value="3"<c:out value="${timeSelected[3]}"/>>4weeks</option>
</select>
<button id="filterbtn"type="submit" class="btn btn-default">Filter</button>

</form>-->

<a id="clearFilterLink" href="showData" ></a>


</br>
<div class="jumbotron">
        <div class="form-group col-lg-12">
        <h4>Letter Generation - Filter by service due date </h4>
        </div>
<form id="filterForm" action="filter?${_csrf.parameterName}=${_csrf.token}" method="post" >
            <div class="form-group col-sm-3 col-md-3 col-lg-3 control-label filterform-groups">
                
                    <div class="input-group">
                        <span class="input-group-addon">from</span>
                        <input id="from" class="form-control" type="text" name="from" value="<c:out value="${fromDate}"/>" size="30" placeholder="yyyy-mm-dd"/>

                </div>
                <span id="fromDateVali" class="text-danger"></span>
            </div>
            <div id="filterform-group1" class="form-group col-sm-3 col-md-3 col-lg-3 control-label filterform-groups">
                
                    <div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input id="to" class="form-control" type="text" name="to" value="<c:out value="${toDate}"/>" size="30" placeholder="yyyy-mm-dd"/>
                </div>
                <span id="toDateVali" class="text-danger"></span>
            </div>
            <div class="form-group col-sm-3 col-md-3 col-lg-3">
               <div class="input-group">
               <span class="input-group-addon">status</span>
	                <select id="filtertype" name="filtertype" class="form-control">
	                    <option value="0"<c:out value="${selectedFilterType[0]}"/>>All</option>
	                    <option value="1"<c:out value="${selectedFilterType[1]}"/>>Letter Not Generated</option>
	                    <option value="2"<c:out value="${selectedFilterType[2]}"/>>Due Services</option>
	                </select>
	             </div>    
                <span id="CN_vali" class="text-danger"></span>
            </div>
            <div class="form-group col-sm-2 col-md-2 col-lg-2">
               <div class="input-group">
               <span class="input-group-addon">type</span>
	                <select id="amcOrFree" name="amcOrFree" class="form-control">
	                    <option value="0"<c:out value="${selectedServiceType[0]}"/>>All</option>
	                    <option value="1"<c:out value="${selectedServiceType[1]}"/>>AMC</option>
	                    <option value="2"<c:out value="${selectedServiceType[2]}"/>>Free</option>
	                </select>
	             </div>    
                <span id="amcOrFree_vali" class="text-danger"></span>
            </div>
<button id="filterbtn"type="submit" class="btn btn-primary">Filter</button>
</form>
</div>
<div class="jumbotron">
        <div class="form-group col-lg-12">
        <h4>Filter by Installation/Agreement Date</h4>
        </div>
<form id="filterForm" action="filterbyinstdate?${_csrf.parameterName}=${_csrf.token}" method="post" >
            <div class="form-group col-sm-3 col-md-3 col-lg-3 control-label filterform-groups">
                
                    <div class="input-group">
                        <span class="input-group-addon">from</span>
                        <input id="instFrom" class="form-control" type="text" name="instFrom" value="" size="30" placeholder="yyyy-mm-dd"/>

                </div>
                <span id="instFromDateVali" class="text-danger"></span>
            </div>
            <div id="filterform-group1" class="form-group col-sm-3 col-md-3 col-lg-3 control-label filterform-groups">
                
                    <div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input id="instTo" class="form-control" type="text" name="instTo" value="" size="30" placeholder="yyyy-mm-dd"/>
                </div>
                <span id="instToDateVali" class="text-danger"></span>
            </div>
            <div class="form-group col-sm-2 col-md-2 col-lg-2">
               <div class="input-group">
               <span class="input-group-addon">type</span>
	                <select id="amcOrFree" name="amcOrFree" class="form-control">
	                    <option value="0">All</option>
	                    <option value="1">AMC</option>
	                    <option value="2">Free</option>
	                </select>
	             </div>    
                <span id="amcOrFree_vali" class="text-danger"></span>
            </div>
<button id="instdatefilterbtn"type="submit" class="btn btn-primary">Filter</button>
</form>
</div>

<div class="jumbotron">
        <div class="form-group col-lg-12">
        <h4>Search by</h4>
        </div>
<form id="searchForm" action="search?${_csrf.parameterName}=${_csrf.token}" method="post" >


            <div class="form-group col-sm-3 col-md-3 col-lg-3">
                <select id="searchcolumn" onchange="changePlaceholder()" name="searchcolumn" class="form-control">
                    <option value="0"<c:out value="${selectedColumn[0]}"/>>Agreement / Installation Date</option>
                    <option value="1"<c:out value="${selectedColumn[1]}"/>>Job No</option>
                    <option value="2"<c:out value="${selectedColumn[2]}"/>>Customer Name</option>
                    <option value="3"<c:out value="${selectedColumn[3]}"/>>Customer Address</option>
                    <option value="4"<c:out value="${selectedColumn[4]}"/>>city</option>
                    <!--<option value="5"<c:out value="${selectedColumn[5]}"/>>contact</option>-->
                  	<option value="6"<c:out value="${selectedColumn[6]}"/>>contactNo</option>
                    <!--<option value="7"<c:out value="${selectedColumn[7]}"/>>type</option>--> 
                    <!--<option value="8"<c:out value="${selectedColumn[8]}"/>>unit</option>--> 
                    <!--<option value="9"<c:out value="${selectedColumn[9]}"/>>capacity</option>-->                    
                    <option value="10"<c:out value="${selectedColumn[10]}"/>>serialNumberIndoor</option>
                    <option value="11"<c:out value="${selectedColumn[11]}"/>>serialNumberOutdoor</option>  
                    <!--<option value="12"<c:out value="${selectedColumn[12]}"/>>no_name</option>-->                     
                    <option value="13"<c:out value="${selectedColumn[13]}"/>>invoice No</option>
                    <!--<option value="14"<c:out value="${selectedColumn[14]}"/>>intalledBy</option>-->
                    <!--<option value="15"<c:out value="${selectedColumn[15]}"/>>empty</option>-->               
                    <!--<option value="16"<c:out value="${selectedColumn[16]}"/>>serviceTeam</option>-->
                    <!--<option value="17"<c:out value="${selectedColumn[17]}"/>>firstFree</option>-->
                    <!--<option value="18"<c:out value="${selectedColumn[18]}"/>>secondFree</option>-->
                    <!--<option value="19"<c:out value="${selectedColumn[19]}"/>>thirdFree</option>-->
                    <!--<option value="20"<c:out value="${selectedColumn[20]}"/>>forth_Service</option>-->                                        
                    <!--<option value="21"<c:out value="${selectedColumn[21]}"/>>fifth_Service</option>-->
                    <!--<option value="22"<c:out value="${selectedColumn[22]}"/>>sixth_Service</option>-->
                </select>
                <span id="CN_vali" class="text-danger"></span>
            </div>
            <div id="filterform-group1" class="form-group col-sm-3 col-md-3 col-lg-3 control-label filterform-groups">
                

                        <input id="searchkeywords" class="form-control" type="text" name="searchkeywords" value="<c:out value="${searchKeyWords}"/>" size="30" placeholder="dd-MMM-yyyy"/>
               
                <span id="searchkeywordsVali" class="text-danger"></span>
            </div>
            
<button id="searchbtn"type="submit" class="btn btn-primary">Search</button>
<!--<button id="clearFilterbtn" class="btn btn-default"  onclick="clearFilterbtnClick(); return false;" >Clear Filter</button>-->
</form>
<div class="row"></div>
</div>
<!--canot display the full path:https://stackoverflow.com/questions/15201071/how-to-get-full-path-of-selected-file-on-change-of-input-type-file-using-jav 
	<button type="button" class="btn btn-default" >PDF Generate</button>
	<a href="downloadPDF" id="downloadPDF"></a>
	</br>-->
<!--<a href="genarateExcel"><button type="button" class="btn btn-default">Generate Excel</button></a> -->
<a href="downloadPDF" id="downloadPDF"></a>
	</br>
    <div class="panel panel-default">
    	<div class="panel-body">
			<div>
			<h3 class="col-lg-12">Total Services&nbsp;&nbsp;&nbsp;${totalservices}</h3>
			<form  action="filter" method="post" >				
				<div class="form-group col-sm-3 col-md-3 col-lg-3 control-label filterform-groups">
                	<div class="input-group">
                        <span class="input-group-addon">from</span>
                        <input id="fromToGenerate" class="form-control" type="text" name="from" value="<c:out value="${fromDate}"/>" size="30" placeholder="yyyy-mm-dd" readonly/>
					</div>
                	<span id="fromDateVali" class="text-danger"></span>
            	</div>
            	<div id="filterform-group1" class="form-group col-sm-3 col-md-3 col-lg-3 control-label filterform-groups">
                	<div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input id="toToGenerate" class="form-control" type="text" name="to" value="<c:out value="${toDate}"/>" size="30" placeholder="yyyy-mm-dd" readonly/>
                	</div>
                	<span id="toDateVali" class="text-danger"></span>
            	</div>
				<button class="btn btn-info" onclick="pdfGenerate(); return false;">PDF Generate</button>
			</form>
			</div>
		<form>
			<div class="">
    			<div class="col-xs-12 table-responsive table-height">
    			
					<table id="table_id" class="table table-bordered table-hover display">
						<thead>
							<tr>
								<th></th>
							<sec:authorize access="hasAuthority('Admin')">
								<th></th>
							</sec:authorize>
								<th>
									<button type="button" class="btn btn-default" id="selectAll"><span class="glyphicon glyphicon-check"></button>
									<button type="button" class="btn btn-default" id="clearAll"><span class="glyphicon glyphicon-unchecked"></button>
								</th>
								<!--<th>Month</th>-->
								<th>Free/ AMC</th>	
								<th>Agreement/ Installation Date</th>				
								<th>Job No</th>
								<th>Customer&nbsp;&nbsp;Name</th>
								<th>Customer Adress</th>
								<th>City</th>
								<th>Contact</th>
								<th>Contact No</th>
								<th>Type</th>
								<th>Units</th>
								<th>apacity</th>
								<th>Serial Number Indoor</th>
								<th>Serial Number Outdoor</th>
								<th>Sales Location</th>
								<th>Invoice No</th>
								<th>Installed By</th>
								<th>Frequency</th>
								<th>Service Team</th>
								<th>1st Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>2nd Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>3rd Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>4th Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>5th Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>6th Service&nbsp;&nbsp;&nbsp;&nbsp;</th>
								<th>Comments&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${showData}" var="customer">
							<tr>
								<td><a href='editpage?customer_pk=${customer.customer_pk}'> <button type="button" class="btn btn-default btn-sm">Edit</button></a></td>
							<sec:authorize access="hasAuthority('Admin')">	
								<td><a href='delete?customer_pk=${customer.customer_pk}'onclick="getConfirmation()"> <button type="button" class="btn btn-default btn-sm">Delete</button></a></td>
							</sec:authorize>	
								<td> <input type="checkbox" name=${customer.customer_pk} value=${customer.customer_pk} id="ckeck${customer.customer_pk}" checked></td>
								<!--<td>${customer.month}</td>-->
								<td>
								<c:choose>
									<c:when test="${customer.amc == '1'}">    
		             					AMC   
		             				</c:when>
		             				<c:when test="${customer.amc == '0'}">    
		             					Free    
		             				</c:when>
  								</c:choose>	
								</td>
								<td>${customer.installationDate}</td>
								<td>${customer.jobNo}</td>
								<td>${customer.customerName}</td>
								<td>${customer.customerAdress}</td>
								<td>${customer.city}</td>
								<td>${customer.contact}</td>
								<td>${customer.contactNo}</td>
								<td>${customer.type}</td>
								<td>${customer.unit}</td>
								<td>${customer.capacity}</td>
								<td>${customer.serialNumberIndoor}</td>
								<td>${customer.serialNumberOutdoor}</td>
								<td>${customer.no_name}</td>
								<td>${customer.instNo}</td>
								<td>${customer.intalledBy}</td>
								<td>${customer.frequency}</td>
								<td>${customer.serviceTeam}</td>
								<td
									<c:choose>
										<c:when test="${customer.firstFreeStatus == '1'}">    
		             						bgcolor="#b9eca4"   
		             					</c:when>
									</c:choose>
								>${customer.firstFree}</td>
								<td
									<c:choose>
										<c:when test="${customer.secondFreeStatus == '1'}">    
		             						bgcolor="#b9eca4"   
		             					</c:when>
									</c:choose>
								>${customer.secondFree}</td>
								<td
									<c:choose>
										<c:when test="${customer.thirdFreeStatus == '1'}">    
		             						bgcolor="#b9eca4"   
		             					</c:when>
									</c:choose>
								>${customer.thirdFree}</td>
								<td
									<c:choose>
										<c:when test="${customer.forthServiceStatus == '1'}">    
		             						bgcolor="#b9eca4"   
		             					</c:when>
									</c:choose>
								>${customer.forth_Service}</td>
								<td
									<c:choose>
										<c:when test="${customer.fifthServiceStatus == '1'}">    
		             						bgcolor="#b9eca4"   
		             					</c:when>
									</c:choose>
								>${customer.fifth_Service}</td>
								<td
									<c:choose>
										<c:when test="${customer.sixthServiceStatus == '1'}">    
		             						bgcolor="#b9eca4"   
		             					</c:when>
									</c:choose>
								>${customer.sixth_Service}</td>
								<td>${customer.comments}</td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	</form>
	</br>
	<!--<a href="downloadExcel"><button type="button" class="btn btn-success btn-default">Download Excel</button></a>-->
	
</div>
	
	<!--https://datatables.net/reference/option/dom#-->
	<script type="text/javascript">
		$(document).ready(function() {
		
	  		$('#table_i').DataTable({
	  		  "dom": '<f<"table-responsive"t>i>',
	  			"paging":   false
	  		  //"dom": '<lf<"table-responsive"t>ip>'
	  		});   
  		});
		/*this data table is active and if you want to active,give id as table_id(add "d" at the end).
		and,change the tags start from line 200 (can be changed when you read) as follows.
				<form>
				<div class="row">
					<div class="col-xs-12">
			
						<table id="table_id" class="table table-bordered table-hover display">
		*/
	</script>
		<script type="text/javascript">
		/*function onSubmit() 
		{
			var filepath = document.getElementById('file').value;
			//alert(filename);
			filepath = filepath.split('\\');
			document.getElementById("fileName").value = filepath[2];
		}*/
	</script>
	<script type="text/javascript">
	//Did not understand the coad (this script) just copy past from-http://stackoverflow.com/questions/4234589/validation-of-file-extension-before-uploading-file
		var _validFileExtensions = [".xls", ".xlsx"];    
		function Validate(oForm) {
		    var arrInputs = oForm.getElementsByTagName("input");
		    for (var i = 0; i < arrInputs.length; i++) {
		        var oInput = arrInputs[i];
		        if (oInput.type == "file") {
		            var sFileName = oInput.value;
		            if (sFileName.length > 0) {
		                var blnValid = false;
		                for (var j = 0; j < _validFileExtensions.length; j++) {
		                    var sCurExtension = _validFileExtensions[j];
		                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
		                        blnValid = true;
		                        break;
		                    }
		                }
		                
		                if (!blnValid) {
		                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
		                    return false;
		                }
		            }
		        }
		    }
		  
		    return true;
		}
	</script>
	<script type="text/javascript">
	var checkId=[
	             	<c:forEach items="${showData}" var="customer" varStatus="status">
		             	"ckeck${customer.customer_pk}"
		             	<c:if test="${!status.last}">    
		             		,    
		             	</c:if>
	             	</c:forEach>
	            ];
	
	
	
	function pdfGenerate(){

	var checkedList=[];
	//var j=0;
	for ( var i = 0; i < checkId.length; i++)
	{
		if ( $('#'+checkId[i]).length )
		{
			if (document.getElementById(checkId[i]).checked)
			{
				checkedList.push(document.getElementById(checkId[i]).value); 
				//j=j+1;
			}
		}
	}
	var filterInfo=[];
	filterInfo.push(document.getElementById("fromToGenerate").value);
	filterInfo.push(document.getElementById("toToGenerate").value);
	
	//var url = 'http://localhost:8080/SoftLogic-Maven/pdfGenerate?${_csrf.parameterName}=${_csrf.token}';
	var url = window.location.protocol + "pdfGenerate?${_csrf.parameterName}=${_csrf.token}";
	//alert(url);
	$.ajax({
		  url: url,
		  type: 'POST',
		  data: "checkedList="+checkedList+"&filterInfo="+filterInfo
		  })
		  .done(function(data) {
		    console.log("success");
		    if (data === undefined)
		    {
		    
		    }
		    else
		    {
		    	document.getElementById('downloadPDF').click();

		    }
		  }); 
	//alert("Called");
	}
	$('#clearAll').click(function()
	{
		for (var i = 0; i < checkId.length; i++)
		{
			if ( $('#'+checkId[i]).length )
			{
				document.getElementById(checkId[i]).checked= false;

			}
		}
	});
	$('#selectAll').click(function()
			{
				for (var i = 0; i < checkId.length; i++)
				{
					if ( $('#'+checkId[i]).length )
					{
						document.getElementById(checkId[i]).checked= true;

					}
				}
			});
	function clearFilterbtnClick()
	{
		document.getElementById('clearFilterLink').click();
	}
	</script>
	<script src="static/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    // When the document is ready
    $(document).ready(function () {

        $('#from').datepicker({
            format: "yyyy-mm-dd"
        });

    });
    $(document).ready(function () {

        $('#to').datepicker({
            format: "yyyy-mm-dd"
        });

    });
    $(document).ready(function () {

        $('#instFrom').datepicker({
            format: "yyyy-mm-dd"
        });

    });
    $(document).ready(function () {

        $('#instTo').datepicker({
            format: "yyyy-mm-dd"
        });

    });
</script>

<script type="text/javascript">
$('#filterbtn').click(function(){
	
	var DateValiFlag = 0;
	
	var inp = $("#from");
	var fromDate_value = inp.val();
	if(fromDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#fromDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#fromDateVali').html("");
		var arr = fromDate_value.split('-');


		var fromDays = (parseInt((arr[0]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[2]);
		//alert (fromHourse_mins);
		//alert(fromDays);

	}

	var inp = $("#to");
	var toDate_value = inp.val();
	if(toDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#toDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#toDateVali').html("");
		var arr = toDate_value.split('-');


		var toDays = (parseInt((arr[0]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[2]);
		//alert (fromHourse_mins);
		//alert(toDays);

	}
	if(DateValiFlag==0)
	{//alert ("awa yako");
		if(toDays <= fromDays)
		{
			//valiFlag = 1;
			DateValiFlag=1;
			$('#fromDateVali').html("<p><code><small>invalid Duration</small></code></p>");
			$('#toDateVali').html("<p><code><small>invalid Duration</small></code></p>");
		}
	}
	if(DateValiFlag==1)
	{
		return false;
	}
	else
	{
		return true;
	}

});
</script>
<script type="text/javascript">
$('#instdatefilterbtn').click(function(){
	
	var DateValiFlag = 0;
	
	var inp = $("#instFrom");
	var fromDate_value = inp.val();
	if(fromDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#instFromDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#instFromDateVali').html("");
		var arr = fromDate_value.split('-');


		var fromDays = (parseInt((arr[0]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[2]);
		//alert (parseInt(arr[2]-1));
		//alert (parseInt(arr[0]));
		//alert(fromDays);

	}

	var inp = $("#instTo");
	var toDate_value = inp.val();
	if(toDate_value == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#instToDateVali').html("<p><code><small>Date can not be empty</small></code></p>");
	}
	else
	{
		$('#instToDateVali').html("");
		var arr = toDate_value.split('-');


		var toDays = (parseInt((arr[0]-1)*365)+parseInt(arr[1]-1)*30)+parseInt(arr[2]);
		//alert (parseInt(arr[2]-1));
		//alert (parseInt(arr[0]));
		//alert(toDays);

	}
	if(DateValiFlag==0)
	{//alert ("awa yakoo");
		if(toDays <= fromDays)
		{
			//valiFlag = 1;
			DateValiFlag=1;
			$('#instFromDateVali').html("<p><code><small>invalid Duration</small></code></p>");
			$('#instToDateVali').html("<p><code><small>invalid Duration</small></code></p>");
		}
	}
	if(DateValiFlag==1)
	{
		return false;
	}
	else
	{
		return true;
	}

});
</script>
<script type="text/javascript">
$('#searchbtn').click(function(){
	
	var DateValiFlag = 0;
	
	var inp = $("#searchkeywords");
	var searchkeywords = inp.val();
	if(searchkeywords == "")
	{
		//valiFlag = 1;
		DateValiFlag = 1;
		$('#searchkeywordsVali').html("<p><code><small>Keywords can not be empty</small></code></p>");
	}
	else
	{
		DateValiFlag = 0;
		$('#searchkeywordsVali').html("");
	}

	if(DateValiFlag==1)
	{
		event.preventDefault();
		return false;
	}
	else
	{
		return true;
	}

});
</script>
<script>
/*$('select[name="searchcolumn"]').change(function(){
	  
    if ($(this).val() == "0")
    {
        alert("call the do something function on option 0");
    document.getElementsByName("searchkeywords")[0].setAttribute("id", "installed");
    alert(document.getElementsByName("searchkeywords")[0].id);
    }
    
});
    $(document).ready(function () {
	    $('#installed')live.datepicker({
	        format: "yyyy-mm-dd"
   		});
    });
$('select[name="searchcolumn"]').change(function(){
if($( "#searchcolumn option:selected" ).val()== "0")
{

	    $('#searchkeyword').datepicker({
	        format: "yyyy-mm-dd"
   		});
   
}
});*/
</script>
<script type="text/javascript">




</script>

	<script>
	/*$('#filterbtn').click(function()
	{
	var data = $('filterForm').serialize();
    var url = 'http://localhost:8080/SoftLogic-Maven/filter'; // the script where you handle the form input.
    alert("at the end");
    $.ajax({
           type: 'POST',
           url: url,
           data:data, // serializes the form's elements.
           success: function(data)
           {
               alert(data); // show response from the php script.
           }
         });
	});*/
	
	</script>
<script>
	function logOut()
	{
		document.getElementById('logOutBtn').click();
	}
</script>
<form:form action="${pageContext.request.contextPath}/logout" method="POST">
    <input id="logOutBtn" type="submit" value="Logout" />
</form:form>
<script>
function getConfirmation() 
{
    if (confirm("Customer will be deleted!") == true) 
    {
		return true;
    } 
    else 
    {
    	event.preventDefault();
    	return false;
    }
}
</script>
<script>
function changePlaceholder() 
{
    var selectedValue = document.getElementById("searchcolumn").value;
    if(selectedValue == 0)
    {
    	document.getElementById("searchkeywords").placeholder = "dd-MMM-yyyy";
    }
    else
    {
        document.getElementById("searchkeywords").placeholder = "Search";
    }
}
</script>	
</body>
</html>