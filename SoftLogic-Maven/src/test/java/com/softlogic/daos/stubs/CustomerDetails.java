package com.softlogic.daos.stubs;

import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;

import com.softlogic.daos.ICustomerDetails;
import com.softlogic.models.AC_Customer;
import com.softlogic.models.Customer;

@Component
public class CustomerDetails implements ICustomerDetails 
{
	public void excelToMysql()
	{
		
	}
	public ArrayList<Customer> getCustomerData()
	{
		ArrayList<Customer> cus = new ArrayList<Customer>();
		return cus;
	}
	public void insertMysql(Customer newCustomer)
	{
		
	}
	public void createExcelFile()
	{
		
	}
	public Customer selectDetailsFrom_customer_pk(String customer_pk)
	{
		Customer customers= new Customer();

		return customers;
	}
	public void updateCustomer(Customer customer)
	{
		
	}
	public int testingTest(int number)
	{
		return number;
	}
	public void DeleteCustomerRowByPK(Customer customer)
	{
		
	}
	public void InsertCustomerList(ArrayList<Customer> customerList)
	{
		
	}
	@Override
	public ArrayList<AC_Customer> getAC_CustomerData() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void insertMysql(AC_Customer newCustomer) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public AC_Customer selectDetailsFromAC_customer_pk(String acCustomer_pk) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void updateAC_Customer(AC_Customer customer) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void DeleteCustomerRowByPK(AC_Customer acCustomer) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void deleteAllAC_Customers() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public ArrayList<AC_Customer> selectCustomerDetailsByOneCriteria(
			String column, int value) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> selectCustomerDetailsByArrayOfPKs(
			String[] checkedArray) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> insertAC_CustomerList(
			ArrayList<AC_Customer> acCustomerList) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> selectCustomerDetailsBySixOrCriterias(
			String column1, int value1, String column2, int value2,
			String column3, int value3, String column4, int value4,
			String column5, int value5, String column6, int value6) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> selectAC_CustomersBetweenTwoDays(
			String sdate, String edate) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> searchByColumn(String column, String keywrd) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> takeLastThousandCustomers(int numOfRows) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public long getTotalNumberofCustomerRecoreds() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public ArrayList<AC_Customer> selectSetOfRows(int firstRow, int numOfRows) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> selectAC_CustomersBetweenTwoInstallatoinDates(
			String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ArrayList<AC_Customer> searchByColumnMiddle(String string,
			String searchKeyWords) {
		// TODO Auto-generated method stub
		return null;
	}
}
