package com.softlogic.services.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.softlogic.services.DataManager;

@Configuration
@ComponentScan(basePackages = {"com.softlogic.daos.stubs","com.softlogic.services"})


class SpringApplicationContextTest 
{

}
//Spring
@RunWith(SpringJUnit4ClassRunner.class)
//Application Context
@ContextConfiguration(classes = SpringApplicationContextTest.class)
public class DataManagerTest
{

	@Autowired
	private DataManager dataManager;
//

	@Test
	public void testTestingTest() 
	{
		int nubm = dataManager.testingTest(1);
		assertEquals(1,nubm);
	}

	
}
