package com.softlogic.services.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.softlogic.services.UserManager;
@Configuration
@ComponentScan(basePackages = {"com.softlogic.daos.stubs","com.softlogic.services"})



//Spring
@RunWith(SpringJUnit4ClassRunner.class)
//Application Context
@ContextConfiguration(classes = SpringApplicationContextTest.class)
public class UserManagerTest 
{

	@Autowired
	private UserManager userManager;
	//
	@Test
	public void testConfirmPassword() 
	{
		boolean userExistance = userManager.confirmPassword("trueUser","dummyPW");
		assertEquals(true,userExistance);
		
	}

}
